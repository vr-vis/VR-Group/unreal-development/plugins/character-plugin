// This Animgraph node file is a modification from the file "AnimGraphNode_ModifyCurve.h" by Epic Games (in UE 4.26)


#include "ModifyCurvesGraphNode.h"

#define LOCTEXT_NAMESPACE "AnimNodes"

UModifyCurvesGraphNode::UModifyCurvesGraphNode(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

FLinearColor UModifyCurvesGraphNode::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UModifyCurvesGraphNode::GetTooltipText() const
{
	return LOCTEXT("ModifyCurves", "Modify Curves");
}

FText UModifyCurvesGraphNode::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("ModifyCurves", "Modify Curves");
}

FString UModifyCurvesGraphNode::GetNodeCategory() const
{
	return TEXT("CharacterPlugin");
}

#undef LOCTEXT_NAMESPACE

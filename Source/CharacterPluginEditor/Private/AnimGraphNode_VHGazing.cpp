// This Animgraph node file is a modification from the file "AnimGraphNode_ModifyCurve.h" by Epic Games (in UE 4.26)


#include "AnimGraphNode_VHGazing.h"

#define LOCTEXT_NAMESPACE "AnimNodes"

UAnimGraphNode_VHGazing::UAnimGraphNode_VHGazing(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
}

FLinearColor UAnimGraphNode_VHGazing::GetNodeTitleColor() const
{
	return FLinearColor::Green;
}

FText UAnimGraphNode_VHGazing::GetTooltipText() const
{
	return LOCTEXT("VHGazingAnimationNode", "VHGazing Animation Node: applies the data gathered in VHGazing component onto the skeletal mesh.");
}

FText UAnimGraphNode_VHGazing::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	return LOCTEXT("VHGazing", "VHGazing");
}

FString UAnimGraphNode_VHGazing::GetNodeCategory() const
{
	return TEXT("CharacterPlugin");
}

#undef LOCTEXT_NAMESPACE

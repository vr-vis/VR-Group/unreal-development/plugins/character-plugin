// This Animgraph node file is a modification from the file "AnimGraphNode_ModifyCurve.h" by Epic Games (in UE 4.26)

#pragma once

#include "CoreMinimal.h"
#include "AnimGraphNode_Base.h"

#include "AnimNodes/AnimNode_VHGazing.h"

#include "AnimGraphNode_VHGazing.generated.h"

UCLASS()
class CHARACTERPLUGINEDITOR_API UAnimGraphNode_VHGazing : public UAnimGraphNode_Base
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, Category = Settings)
	FAnimNode_VHGazing Node;

public:

	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;

	virtual FString GetNodeCategory() const override;

	UAnimGraphNode_VHGazing(const FObjectInitializer& ObjectInitializer);

protected:
	
};

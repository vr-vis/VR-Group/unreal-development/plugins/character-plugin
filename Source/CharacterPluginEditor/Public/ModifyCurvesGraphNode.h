// This Animgraph node file is a modification from the file "AnimGraphNode_ModifyCurve.h" by Epic Games (in UE 4.26)

#pragma once

#include "CoreMinimal.h"
#include "AnimGraphNode_Base.h"

#include "AnimNodes/ModifyCurvesNode.h"

#include "ModifyCurvesGraphNode.generated.h"

UCLASS()
class CHARACTERPLUGINEDITOR_API UModifyCurvesGraphNode : public UAnimGraphNode_Base
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, Category = Settings)
	FModifyCurvesNode Node;

public:

	virtual FLinearColor GetNodeTitleColor() const override;
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;

	virtual FString GetNodeCategory() const override;

	UModifyCurvesGraphNode(const FObjectInitializer& ObjectInitializer);

protected:
	
};

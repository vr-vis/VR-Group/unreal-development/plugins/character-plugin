using UnrealBuildTool;
using System.IO;

public class CharacterPlugin : ModuleRules
{
    public CharacterPlugin(ReadOnlyTargetRules Target) : base(Target)
    {

        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PrivateIncludePaths.AddRange(new string[] {  });
        PublicIncludePaths.AddRange(new string[] {  });

        PublicDependencyModuleNames.AddRange(new string[] { "CoreUObject", "Engine", "Core", "DisplayCluster", "AIModule", "Projects", "NavigationSystem", "AIModule", "UniversalLogging", "RWTHVRToolkit", "VAPlugin", "AnimGraphRuntime" });

    }
}
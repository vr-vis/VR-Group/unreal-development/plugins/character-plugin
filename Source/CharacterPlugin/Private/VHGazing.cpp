// Fill out your copyright notice in the Description page of Project Settings.


#include "VHGazing.h"

#include "VHAnimInstance.h"
#include "Components/SkeletalMeshComponent.h"
#include "Helper/CharacterPluginLogging.h"
#include "Kismet/KismetMathLibrary.h"
#include "Pawn/RWTHVRPawn.h"
#include "Internationalization/TextKey.h"

#include "Kismet/GameplayStatics.h"

UVHGazing::UVHGazing()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UVHGazing::BeginPlay()
{
	Super::BeginPlay();

	VirtualHumanPtr = Cast<AVirtualHuman>(GetOwner());
	// the above two (Face & Body) are the same for CC3 body types

	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	GazingRandomStream = FRandomStream(TextKeyUtil::HashString(*GetOwner()->GetName()));


	// initial values blinking
	BlinkPhaseTimeElapsed = 0.0f;
	BlinkPhaseDuration = 1.0f; //start blinking after one second
	BlinkState = EBlinkState::Open;

	// start the first microsaccade, which will set a timer for the next call
	MicroSaccade();

	// setup everything for the saccade mode chosen at start
	SetupSaccadeData();
	SetSaccadeMode(SaccadeMode);

	SwitchLookAtUsersEye();//even if it is not used it just flips the value regularly
}

void UVHGazing::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsGazing)
	{
		UpdateTargetRotations();
	}

	UVHAnimInstance* FaceAnimInstance = VirtualHumanPtr->GetFaceAnimInstance();
	UVHAnimInstance* BodyAnimInstance = VirtualHumanPtr->GetBodyAnimInstance();

	FaceAnimInstance->VHGazingData.GazeTargetLocation = TargetLocation;
	FaceAnimInstance->VHGazingData.EyesOnlyGazeTarget.Reset();
	if (EyesOnlyTargetLocation.IsSet()) {
		FaceAnimInstance->VHGazingData.EyesOnlyGazeTarget = EyesOnlyTargetLocation;
	}
	FaceAnimInstance->VHGazingData.bIsGazing = bIsGazing;
	FaceAnimInstance->VHGazingData.GazeMode = GazeMode;

	FaceAnimInstance->VHGazingData.AdditionalSaccadeRotation = MicroSaccadeOffset + SaccadeOffset;
	if (bGazingAtUser && bDoEyeSwitchingWhenLookingAtUser)
	{
		//this offset is added for the saccade offset, since it should always only be applied to the eyes and not the head etc.
		FaceAnimInstance->VHGazingData.AdditionalSaccadeRotation += GazeAtUserEyeOffset();
	}

	FaceAnimInstance->VHGazingData.RightEyeData.MaxPitch = MaxEyePitch;
	FaceAnimInstance->VHGazingData.RightEyeData.MaxYaw = MaxEyeYaw;
	FaceAnimInstance->VHGazingData.RightEyeData.MaxVelocity = InterpolationSpeedEyes;
	FaceAnimInstance->VHGazingData.RightEyeData.StartVelocityFactor = StartSpeedFactorEyes;
	FaceAnimInstance->VHGazingData.RightEyeData.Alignment = 1.0f;

	FaceAnimInstance->VHGazingData.LeftEyeData.MaxPitch = MaxEyePitch;
	FaceAnimInstance->VHGazingData.LeftEyeData.MaxYaw = MaxEyeYaw;
	FaceAnimInstance->VHGazingData.LeftEyeData.MaxVelocity = InterpolationSpeedEyes;
	FaceAnimInstance->VHGazingData.LeftEyeData.StartVelocityFactor = StartSpeedFactorEyes;
	FaceAnimInstance->VHGazingData.LeftEyeData.Alignment = 1.0f;

	FaceAnimInstance->VHGazingData.HeadData.MaxPitch = MaxHeadPitch;
	FaceAnimInstance->VHGazingData.HeadData.MaxYaw = MaxHeadYaw;
	FaceAnimInstance->VHGazingData.HeadData.MaxVelocity = InterpolationSpeedHead;
	FaceAnimInstance->VHGazingData.HeadData.StartVelocityFactor = StartSpeedFactorHead;
	FaceAnimInstance->VHGazingData.HeadData.Alignment = HeadAlignment;

	FaceAnimInstance->VHGazingData.TorsoData.MaxPitch = 0.0f; //we don't want pitch in the torso movement
	FaceAnimInstance->VHGazingData.TorsoData.MaxYaw = MaxTorsoYaw;
	FaceAnimInstance->VHGazingData.TorsoData.MaxVelocity = InterpolationSpeedTorso;
	FaceAnimInstance->VHGazingData.TorsoData.StartVelocityFactor = StartSpeedFactorTorso;
	FaceAnimInstance->VHGazingData.TorsoData.Alignment = TorsoAlignment;
	FaceAnimInstance->VHGazingData.TorsoData.DelayConstant = TorsoRotationDelayConstant;
	FaceAnimInstance->VHGazingData.TorsoData.DelayLinear = TorsoRotationDelayLinear;

	FaceAnimInstance->VHGazingData.bSupportClampedRotationByOtherParts = bSupportClampedRotationByOtherParts;

	//copy over to body anim instance if needed (remember, e.g., for CC3 BodyAnimInstance==FaceAnimInstance)
	if (BodyAnimInstance != FaceAnimInstance) BodyAnimInstance->VHGazingData = FaceAnimInstance->VHGazingData;



	Blink(DeltaTime);

	if ((LastTargetLocation - (EyesOnlyTargetLocation.IsSet() ? EyesOnlyTargetLocation.GetValue() : TargetLocation)).Size() > 10.0f)
	{
		//force a blink if the target location changed by more than 10cm
		ForceBlink();
	}
	LastTargetLocation = (EyesOnlyTargetLocation.IsSet() ? EyesOnlyTargetLocation.GetValue() : TargetLocation);
}

void UVHGazing::UpdateTargetRotations()
{
	if (TargetActor)
	{
		TargetLocation = TargetActor->GetActorLocation() + TargetOffset;
	}
	else if (TargetComponent)
	{
		TargetLocation = TargetComponent->GetComponentLocation() + TargetOffset;
	}

}

void UVHGazing::Blink(float DeltaTime)
{
	if (!bEyeBlink)
		return;

	/*
	 * Implementation is inspired by:
	 * Ruhland, K., Peters, C. E., Andrist, S., Badler, J. B., Badler, N. I., Gleicher, M., Mutlu, B., McDonnell, R. (2015).
	 * A Review of Eye Gaze in Virtual Agents, Social Robotics and HCI: Behaviour Generation, User Interaction and Perception.
	 * Computer Graphics Forum 34 (6), 299-326. https://doi.org/10.1111/CGF.12603
	 *
	 * However, it is not content-related. As stated in the publication above, blinks correlate with thinking and is related to emotion
	 * We, however, incorporated blinks at large saccades ("a blink occurs almost simultaneously with the onset of large eye and eye-head gaze movements")
	 * see ForceBlink in Tick()
	 *
	 * Also based on:
	 * Trutoiu, L. C., Carter, E. J., Matthews, I., Hodgins, J. K. (2011).
	 * Modeling and animating eye blinks.
	 * ACM Transactions on Applied Perception, 8(3), 17. https://doi.org/10.1145/2010325.2010327
	 *
	 * Complete blink duration: 300ms
	 * Fully closed after: 100ms
	 * Using asymmetric ease in/out which is at 9 frames (300ms) only slightly worse than there model
	 */

	BlinkPhaseTimeElapsed += DeltaTime;

	if (BlinkPhaseTimeElapsed >= BlinkPhaseDuration)
	{
		BlinkPhaseTimeElapsed = 0.0f;
		switch (BlinkState)
		{
		case EBlinkState::Open:
			BlinkState = EBlinkState::Closing;
			BlinkPhaseDuration = 0.33 * GazingRandomStream.FRandRange(0.25f, 0.35f);
			break;
		case EBlinkState::Closing:
			BlinkState = EBlinkState::Opening;
			BlinkPhaseDuration = 0.66 * GazingRandomStream.FRandRange(0.25f, 0.35f);
			break;
		case EBlinkState::Opening:
			BlinkState = EBlinkState::Open;
			// average idle blink rate is around 20 blinks/min +- 9 blinks/min, according to https://doi.org/10.1002/mds.870120629
			// theoretically blink rate depends on current activity
			BlinkPhaseDuration = GazingRandomStream.FRandRange(2.1f, 5.4f);
			break;
		default:
			VH_ERROR("[UVHGazing::Blink] Unknown blink state!");
			break;
		}
	}


	UVHAnimInstance* FaceAnimInstance = VirtualHumanPtr->GetFaceAnimInstance();

	if (BlinkState == EBlinkState::Closing)
	{
		FaceAnimInstance->EyeBlink = FMath::InterpEaseInOut(0.0f, 1.0f, BlinkPhaseTimeElapsed / BlinkPhaseDuration, 3.0f);
	}
	else if (BlinkState == EBlinkState::Opening)
	{
		FaceAnimInstance->EyeBlink = FMath::InterpEaseInOut(1.0f, 0.0f, BlinkPhaseTimeElapsed / BlinkPhaseDuration, 3.0f);
	}
	else
	{
		FaceAnimInstance->EyeBlink = 0.0f;
	}

}

void UVHGazing::GazeTo(FVector GazeTarget)
{
	TargetActor = nullptr;
	TargetComponent = nullptr;
	TargetLocation = GazeTarget;
	bIsGazing = true;
	bGazingAtUser = false;
}

void UVHGazing::GazeToActor(AActor* GazeTargetActor, FVector Offset /*= FVector::ZeroVector*/)
{
	TargetActor = GazeTargetActor;
	TargetComponent = nullptr;
	TargetOffset = Offset;
	bIsGazing = true;
	bGazingAtUser = false;
}

void UVHGazing::GazeToComponent(USceneComponent* GazeTargetComponent, FVector Offset)
{
	TargetActor = nullptr;
	TargetComponent = GazeTargetComponent;
	TargetOffset = Offset;
	bIsGazing = true;
	bGazingAtUser = false;
}

void UVHGazing::GazeToUser()
{
	ARWTHVRPawn* VRPawn = Cast<ARWTHVRPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if (VRPawn)
	{
		GazeToComponent(Cast<USceneComponent>(VRPawn->HeadCameraComponent));
		bGazingAtUser = true;
	}
	else
	{
		VH_WARN("[UVHGazing::GazeToUser] The pawn is not a AVirtualRealityPawn, this method does not work!")
	}
}

void UVHGazing::GazeToEyesOnly(FVector Target)
{
	EyesOnlyTargetLocation = Target;
}

void UVHGazing::StopEyesOnlyGazing()
{
	EyesOnlyTargetLocation.Reset();
}

void UVHGazing::StopGazing()
{
	bIsGazing = false;
}

void UVHGazing::ForceBlink()
{
	if (BlinkState == EBlinkState::Open)
	{
		//this way the next call off Blink() will trigger a blink
		BlinkPhaseDuration = 0.0f;
	}
}

void UVHGazing::SetSaccadeMode(ESaccadeMode NewSaccadeMode, bool bStartWithMutualGaze /*= true*/)
{
	SaccadeMode = NewSaccadeMode;

	if (SaccadeMode == ESaccadeMode::Fixating)
	{
		SaccadeOffset = FRotator::ZeroRotator;
		GetWorld()->GetTimerManager().ClearTimer(NextSaccadeHandle);
		return;
	}

	//so last is faked to be "averted", so the next is mutual
	bAvertedSaccade = bStartWithMutualGaze;

	Saccade();
}

ESaccadeMode UVHGazing::GetCurrentSaccadeMode() const
{
	return SaccadeMode;
}

void UVHGazing::MicroSaccade()
{
	// use Table 1 from https://doi.org/10.1167%2F8.14.15 for approx values
	// (table gives SE for average, so SD=SE*sqrt(6))

	float Interval = RandomGaussian(1.43f, 0.48f);//seconds (table gives frequency)
	float Magnitude = RandomGaussian(0.41f, 0.10f); //degree

	if (bDoMicrosaccades)
	{
		MicroSaccadeOffset = RandomSaccadeRotator(Magnitude);
		//VH_DEBUG("Microsaccade with: %f", Magnitude);
	}


	//set timer for next call
	GetWorld()->GetTimerManager().SetTimer(NextMicroSaccadeHandle, this, &UVHGazing::MicroSaccade,
		FMath::Max(Interval, 0.1f),
		false);
}

void UVHGazing::Saccade()
{
	// "flip a coin" whether next is averted or mutual
	// however since the duration are different we have to use the corrected percentage (from time to number of saccades)
	const float RandomPercentage = GazingRandomStream.GetFraction() * 100.0f;
	if (RandomPercentage < SaccadeData[SaccadeMode].PercentageMutualNumber)
	{
		//next gaze it mutual gaze (i.e. to target)
		SaccadeOffset = FRotator::ZeroRotator;
		bAvertedSaccade = false;
	}
	else
	{
		float Magnitude = RandomSaccadeMagnitude();
		SaccadeOffset = RandomSaccadeRotator(Magnitude, SaccadeData[SaccadeMode]);
		bAvertedSaccade = true;
	}

	//now bAvertedSaccade describes the saccade to come and not the past one anymore

	float TimeToNextSaccade = 0.0f;
	if (bAvertedSaccade)
	{
		TimeToNextSaccade = RandomGaussian(SaccadeData[SaccadeMode].AvertedGazeMean,
			SaccadeData[SaccadeMode].AvertedGazeSD);
	}
	else
	{
		TimeToNextSaccade = RandomGaussian(SaccadeData[SaccadeMode].MutualGazeMean,
			SaccadeData[SaccadeMode].MutualGazeSD);
	}

	//the gaussian distribution can be all over the place, so make sure that the numbers are in a reasonable range
	TimeToNextSaccade = FMath::Clamp(TimeToNextSaccade, 0.1f, 60.0f);

	GetWorld()->GetTimerManager().SetTimer(NextSaccadeHandle, this, &UVHGazing::Saccade, TimeToNextSaccade, false);
}

void UVHGazing::SwitchLookAtUsersEye()
{
	bLookAtUsersRightEye = !bLookAtUsersRightEye;

	float TimeToNextSwitch = RandomGaussian(2.0f, 1.0f); //these numbers are guesses
	TimeToNextSwitch = FMath::Clamp(TimeToNextSwitch, 1.0f, 60.0f);

	GetWorld()->GetTimerManager().SetTimer(NextEyeSwitchHandle, this, &UVHGazing::SwitchLookAtUsersEye, TimeToNextSwitch, false);
}

FRotator UVHGazing::GazeAtUserEyeOffset()
{
	AVirtualHuman* VH = Cast<AVirtualHuman>(GetOwner());
	FVector VHEyeLocation = VH->GetFaceAnimInstance()->GetSkelMeshComponent()->GetSocketLocation(VH->GetBoneNames().eye_r);
	FVector DirectGaze = TargetComponent->GetComponentLocation() - VHEyeLocation;
	//mean IPD of humans is 6.3cm
	//Fesharaki H, Rezaei L, Farrahi F, Banihashem T, Jahanbkhshi A. Normal interpupillary distance values in an Iranian population. J Ophthalmic Vis Res. 2012 Jul;7(3):231-4. PMID: 23330061; PMCID: PMC3520592.
	FVector EyeGaze = DirectGaze + TargetComponent->GetRightVector() * (bLookAtUsersRightEye ? 1.0f : -1.0f) * 3.15f;
	return FAnimNode_VHGazing::GetYawPitchRotationBetween(DirectGaze, EyeGaze);
}

FRotator UVHGazing::RandomSaccadeRotator(float Magnitude) const
{
	const FRotator RotationAxisRotator = FRotator(GazingRandomStream.FRandRange(0, 360), 0, 0);
	const FVector RotationAxis = RotationAxisRotator.RotateVector(FVector::UpVector);
	return UKismetMathLibrary::RotatorFromAxisAndAngle(RotationAxis, Magnitude);
}

FRotator UVHGazing::RandomSaccadeRotator(float Magnitude, FSaccadeData SaccadeDate) const
{
	float RandomPercent = GazingRandomStream.GetFraction() * 100.0f;
	int Direction = 0;
	while (RandomPercent > 0.0f && Direction < 8)
	{
		RandomPercent -= SaccadeDate.DirectionProbabilities[Direction++];
	}
	Direction--;
	const FRotator RotationAxisRotator = FRotator(Direction * 45, 0, 0);
	const FVector RotationAxis = RotationAxisRotator.RotateVector(FVector::UpVector);
	return UKismetMathLibrary::RotatorFromAxisAndAngle(RotationAxis, Magnitude);
}

float UVHGazing::RandomGaussian(float Mean, float SD) const
{
	// using Box-Muller transform
	const float U1 = GazingRandomStream.GetFraction();
	const float U2 = GazingRandomStream.GetFraction();
	const float Z1 = FMath::Sqrt(-2.0f * FMath::Loge(U1)) * FMath::Cos(2.0f * PI * U2);
	//Z1 is now following a standard normal distribution
	return Z1 * SD + Mean;
}

float UVHGazing::RandomSaccadeMagnitude() const
{
	// formula from: https://dl.acm.org/doi/10.1145/566570.566629
	float P = GazingRandomStream.GetFraction() * 15.0f;
	float A = -6.9f * FMath::Loge(P / 15.7f);
	//for security clamp it between reasonable numbers (the orig paper proposes higher values)
	return FMath::Clamp(A, 1.0f, 12.0f);
}

void UVHGazing::SetupSaccadeData()
{
	// data from: https://dl.acm.org/doi/10.1145/566570.566629
	FSaccadeData SpeakingData;
	SpeakingData.AvertedGazeMean = 0.93f;
	SpeakingData.AvertedGazeSD = 0.23f;
	SpeakingData.MutualGazeMean = 7.92f;
	SpeakingData.MutualGazeSD = 1.57f;
	SpeakingData.PercentageMutual = 41.0f;
	SpeakingData.DirectionProbabilities = { 15.54f, 6.46f, 17.69f, 7.44f, 16.80f, 7.89f, 20.38f, 7.79f };
	SaccadeData.Add(ESaccadeMode::Speaking, SpeakingData);

	FSaccadeData ListeningData;
	ListeningData.AvertedGazeMean = 0.43f;
	ListeningData.AvertedGazeSD = 0.8f;
	ListeningData.MutualGazeMean = 3.13f;
	ListeningData.MutualGazeSD = 3.1f;
	ListeningData.PercentageMutual = 75.0f;
	ListeningData.DirectionProbabilities = { 15.54f, 6.46f, 17.69f, 7.44f, 16.80f, 7.89f, 20.38f, 7.79f };
	SaccadeData.Add(ESaccadeMode::Listening, ListeningData);


	// Thinking mode data taken from smartbody
	// https://sourceforge.net/p/smartbody/code/HEAD/tree/trunk/core/smartbody/SmartBody/src/controllers/me_ct_saccade.cpp
	FSaccadeData ThinkingData;
	ThinkingData.AvertedGazeMean = 6.0f;
	ThinkingData.AvertedGazeSD = 1.56f;
	ThinkingData.MutualGazeMean = 6.0f;
	ThinkingData.MutualGazeSD = 1.56f;
	ThinkingData.PercentageMutual = 20.0f;
	ThinkingData.DirectionProbabilities = { 5.46f, 10.54f, 24.69f, 6.44f, 6.89f, 12.80f, 26.38f, 6.79f };
	SaccadeData.Add(ESaccadeMode::Thinking, ThinkingData);

	for (auto& SaccadeDate : SaccadeData)
	{
		float D_averted = SaccadeDate.Value.AvertedGazeMean;
		float D_mutual = SaccadeDate.Value.MutualGazeMean;
		float P_mutual = SaccadeDate.Value.PercentageMutual;
		float P_averted = 100.0f - P_mutual;
		float n = D_averted / P_averted * P_mutual / D_mutual;
		SaccadeDate.Value.PercentageMutualNumber = n / (n + 1) * 100.0f;
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "VHMovement.h"

#include <ctime>

#include "VirtualHuman.h"
#include "VirtualHumanAIController.h"
#include "Waypoint.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "Kismet/KismetMathLibrary.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"

#include "IUniversalLogging.h"
#include "Engine/World.h"
#include "Helper/CharacterPluginLogging.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "VHAnimInstance.h"


// Sets default values for this component's properties
UVHMovement::UVHMovement()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


UCharacterMovementComponent* UVHMovement::GetCharacterMovementComponent() const
{
	if (!AVirtualHumanPawn)
		return nullptr;
	return Cast<UCharacterMovementComponent>(AVirtualHumanPawn->GetMovementComponent());
}

// Called when the game starts
void UVHMovement::BeginPlay()
{
	Super::BeginPlay();

	AVirtualHumanPawn = Cast<APawn>(GetOwner());
	VirtualHumanAIController = Cast<AVirtualHumanAIController>(AVirtualHumanPawn->GetController());
	AVirtualHumanPtr = Cast<AVirtualHuman>(GetOwner());
	CurrentWaypointIterator = 0;
	IntermediatePointIterator = 0;
	bChildMovement = false;
	bLineMovement = false;

	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	MovementRandomStream = FRandomStream(TextKeyUtil::HashString(*GetOwner()->GetName()));

	UCapsuleComponent* capsuleComp = AVirtualHumanPtr->GetCapsuleComponent();
	IKTraceDistance = capsuleComp->GetScaledCapsuleHalfHeight();
	IKOffsetRigthFoot = 0.0f;
	IKOffsetLeftFoot = 0.0f;
	IKInterpSpeed = 10.0f;
	IKMaxOffset = 55.0f;
	HipDisplacementFactor = 1.5f;
	RightFootSocket = AVirtualHumanPtr->GetBoneNames().foot_socket_r;
	LeftFootSocket = AVirtualHumanPtr->GetBoneNames().foot_socket_l;
	AnimationInstance = AVirtualHumanPtr->GetBodyAnimInstance();


	//set child Parameters
	int i = 1;
	for (TPair<AVirtualHuman*, float >& child : ChildVH)
	{
		if (!child.Key)
		{
			VH_ERROR("Virtual Human child is not valid\n");
			return;
		}
		bChild = false;
		UVHMovement* childMovement = child.Key->FindComponentByClass<UVHMovement>();
		childMovement->WaypointGroup = GetWaypointGroup();
		childMovement->bLoop = IsLoop();
		childMovement->parentVH = Cast<AVirtualHuman>(AVirtualHumanPawn);
		childMovement->GroupFormation = GroupFormation;
		childMovement->WaypointOrder = WaypointOrder;
		childMovement->bChild = true;
		childMovement->bDrawDebugSpheres = bDrawDebugSpheres;
		childMovement->ChildVH.Empty();
		if (child.Value == 0) //no offset has been specified
		{
			childMovement->OffsetFromWaypoint = 100 * i;
		}
		else
		{
			childMovement->OffsetFromWaypoint = child.Value;
		}
		i++;
	}
}


// Called every frame
void UVHMovement::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// Calculate IKOffset for Foot alignment

	FVector MeshLocation = AVirtualHumanPtr->GetBodyMesh()->GetComponentLocation();

	// RightFoot

	FHitResult Hit = IKFootTrace(RightFootSocket, IKTraceDistance);

	// the location we want the foot of the VA to be
	FVector HitLocationRight = Hit.Location;

	FVector offsetRight = HitLocationRight - MeshLocation;
	float IKOffsetRight = offsetRight.Z - IKHipOffset;

	// LeftFoot

	Hit = IKFootTrace(LeftFootSocket, IKTraceDistance);

	// the location we want the foot of the VA to be
	FVector HitLocationLeft = Hit.Location;

	FVector offsetLeft = HitLocationLeft - MeshLocation;
	float IKOffsetLeft = offsetLeft.Z - IKHipOffset;

	// adjust the height of the VA, by lowering the hip bone

	float footHeightDistance = abs(HitLocationRight.Z - HitLocationLeft.Z);
	float hipOffset = 0;

	// lower the hip for a more natural posture, when walking on slopes or stairs
	if (footHeightDistance < 100.0f)
	{
		hipOffset = footHeightDistance * -0.5f * HipDisplacementFactor;
	}

	// adjust foot rotation
	FVector normal = Hit.ImpactNormal;
	FVector up = MeshLocation.UpVector;

	float footRotationAngleRAD = acosf(FVector::DotProduct(normal, up));

	// check if VA is walking UP or DOWN and change sign of footRotationAngle accordingly
	FVector velocity = AVirtualHumanPtr->GetVelocity();
	float normalToVelocity = FVector::DotProduct(normal, velocity);

	if (velocity.Size() > 0) // only change when walking
	{
		// if angle between normal and velocity is bigger than 90 degree value is negative.
		// that means VA is walking UP
		if (normalToVelocity < 0)
		{
			bWalkingUp = true;
		}
		else
		{
			bWalkingUp = false;
		}
	}

	// the rotation introduces a height error
	// the Socket is at the front of the foot
	// the bone we rotate around is at the back of the foot
	const BoneNames& bones = AVirtualHumanPtr->GetBoneNames();
	FVector footVector = AVirtualHumanPtr->GetBodyMesh()->GetBoneLocation(bones.foot_r) - AVirtualHumanPtr->GetBodyMesh()->GetSocketLocation(RightFootSocket);
	float footLength = footVector.Size(); // the length of the shoe
	float rotationErrorHeightAdjustment = tanf(footRotationAngleRAD) * footLength;
	rotationErrorHeightAdjustment = FMath::Clamp(rotationErrorHeightAdjustment, 0.0f, footLength); // safety clamp

	// we always use FInterpTo function to get smooth animations for all variables that we need in the Animation Blueprint

	if (bWalkingUp)
	{
		IKFootRotationAngle = FMath::FInterpTo(IKFootRotationAngle, -FMath::RadiansToDegrees(footRotationAngleRAD), DeltaTime, IKInterpSpeed);

		IKOffsetRight -= rotationErrorHeightAdjustment;
		IKOffsetLeft -= rotationErrorHeightAdjustment;
	}
	else
	{
		IKFootRotationAngle = FMath::FInterpTo(IKFootRotationAngle, FMath::RadiansToDegrees(footRotationAngleRAD), DeltaTime, IKInterpSpeed);
		IKOffsetRight += rotationErrorHeightAdjustment;
		IKOffsetLeft += rotationErrorHeightAdjustment;
	}

	// if one of the offsets is still negative we have to lower the hip even further to reach the floor

	if (IKOffsetLeft < 0.0f && IKOffsetLeft > -(IKMaxOffset / 2.0f))
	{
		hipOffset += IKOffsetLeft;
	}
	else if (IKOffsetRight < 0.0f && IKOffsetRight > -(IKMaxOffset / 2.0f))
	{
		hipOffset += IKOffsetRight;
	}

	hipOffset = FMath::Clamp(hipOffset, -IKMaxOffset, 0.0f);
	IKHipOffset = FMath::FInterpTo(IKHipOffset, hipOffset, DeltaTime, IKInterpSpeed);

	IKOffsetRight = FMath::Clamp(IKOffsetRight, 0.0f, IKMaxOffset);
	IKOffsetRigthFoot = FMath::FInterpTo(IKOffsetRigthFoot, IKOffsetRight, DeltaTime, IKInterpSpeed);


	IKOffsetLeft = FMath::Clamp(IKOffsetLeft, 0.0f, IKMaxOffset);
	IKOffsetLeftFoot = FMath::FInterpTo(IKOffsetLeftFoot, IKOffsetLeft, DeltaTime, IKInterpSpeed);


	// write variables to Animation Instance
	if (AnimationInstance)
	{
		AnimationInstance->IKOffsetRight = -IKOffsetRigthFoot;
		AnimationInstance->IKOffsetLeft = IKOffsetLeftFoot;
		AnimationInstance->IKHipOffset = IKHipOffset;
		AnimationInstance->IKFootRotation = IKFootRotationAngle;
	}
}

void UVHMovement::MoveToWaypoint()
{
	if (!AVirtualHumanPawn) {
		VH_ERROR("[UVHMovement::MoveToWaypoint()] Owner is not derived from APawn\n");
		return;
	}

	if(!VirtualHumanAIController)
	{
		//this might not be initialized on BeginPlay
		VirtualHumanAIController = Cast<AVirtualHumanAIController>(AVirtualHumanPawn->GetController());
	}
	if (!VirtualHumanAIController) {
		VH_ERROR("[UVHMovement::MoveToWaypoint()] Virtual Human does not implement AIController\n");
		return;
	}

	// if we are at the last waypoint and it defines destroying agent, do so
	if (CurrentWaypointIterator == Waypoints.Num() && Waypoints[CurrentWaypointIterator - 1]->GetLastWaypointCommand() == EOnWaypointReach::Destroy)
	{
		VirtualHumanAIController->DestroyAgent();
	}


	if (CurrentWaypointIterator < Waypoints.Num())
	{
		ACurrentWaypoint = Waypoints[CurrentWaypointIterator];
		if (!ACurrentWaypoint) {
			VH_ERROR("Waypoint with Index %i is not valid\n", CurrentWaypointIterator);
			return;
		}
			
		
		UNavigationSystemV1* navSys = UNavigationSystemV1::GetCurrent(GetWorld());
		if (!navSys) {
			VH_ERROR("Navigation System is not valid\n");
			return;
		}
			

		auto path = navSys->FindPathToActorSynchronously(GetWorld(), AVirtualHumanPawn->GetActorLocation(), ACurrentWaypoint);
		FAIMoveRequest request = FAIMoveRequest();
		request.SetGoalActor(ACurrentWaypoint);
		request.SetUsePathfinding(true);
		request.SetAllowPartialPath(true);
		Cast<UCharacterMovementComponent>(AVirtualHumanPawn->GetMovementComponent())->MaxWalkSpeed = ACurrentWaypoint->GetWalkingSpeed();

		CurrentRequestID = VirtualHumanAIController->RequestMove(request, path->GetPath());
						
		for (TPair<AVirtualHuman*, float >& child : ChildVH)
		{
			if (!child.Key) {
				VH_ERROR("Virtual Human does not have a valid child\n");
				return;
			}
				
			UVHMovement* childMovement = child.Key->FindComponentByClass<UVHMovement>();
			if (!childMovement->bLineMovement)
			{
				childMovement->CurrentWaypointIterator = CurrentWaypointIterator;
				childMovement->bChildMovement = false; // stop AIController from executing ChildMoveToWaypoint()
				childMovement->IntermediatePointIterator = 0;
				childMovement->ChildMoveToWaypoint();
				childMovement->bChildMovement = true; // allows AIController to execute ChildMoveToWaypoint()
			}
		}
		CurrentWaypointIterator++;
	}
	// if loop is enabled
	if (CurrentWaypointIterator == Waypoints.Num() && IsLoop() && Waypoints.Num() > 1)
	{
		CurrentWaypointIterator = 0;
	}
}


/*
 *	Moves VH to desired location. Supply with FVector as destination.
 */
void UVHMovement::MoveToWaypoint(FVector TargetLocation, float Speed)
{
	if (!AVirtualHumanPawn) {
		VH_ERROR("Parent Pawn is not Virtual Human\n");
		return;
	}

	if (!VirtualHumanAIController) {
		VH_ERROR("Virtual Human does not implement AIController\n");
		return;
	}

	Cast<UCharacterMovementComponent>(AVirtualHumanPawn->GetMovementComponent())->MaxWalkSpeed = Speed;
	CurrentRequestID = VirtualHumanAIController->MoveToLocation(TargetLocation, false);
}

void UVHMovement::ChildMoveToWaypoint()
{
	if (!AVirtualHumanPawn) {
		VH_ERROR("Parent Pawn is not Virtual Human\n");
		return; 
	}

	if (!VirtualHumanAIController)
	{
		//this might not be initialized on BeginPlay
		VirtualHumanAIController = Cast<AVirtualHumanAIController>(AVirtualHumanPawn->GetController());
	}
	if (!VirtualHumanAIController) {
		VH_ERROR("Virtual Human does not implement AIController\n");
		return;
	}
		

	if (CurrentWaypointIterator < Waypoints.Num())
	{
		ACurrentWaypoint = Waypoints[CurrentWaypointIterator]; //ICurrentWaypoint is only incremented by the parent
		if (!ACurrentWaypoint) {
			VH_ERROR("Waypoint with Index %i is not valid\n", CurrentWaypointIterator);
			return;
		}


		UNavigationSystemV1* navSys = UNavigationSystemV1::GetCurrent(GetWorld());
		if (!navSys) {
			VH_ERROR("Navigation System is not valid\n");
			return;
		}

		TArray<FVector> intermediatePoints = ChildPoints.FindRef(ACurrentWaypoint);

		if (IntermediatePointIterator < intermediatePoints.Num()) {

			auto path = navSys->FindPathToLocationSynchronously(GetWorld(), AVirtualHumanPawn->GetActorLocation(), intermediatePoints[IntermediatePointIterator]);

			FAIMoveRequest request = FAIMoveRequest();
			request.SetUsePathfinding(true);
			request.SetAllowPartialPath(true);
			request.SetAcceptanceRadius(10.0f);
			float speed;
			// first point is either speed up point or we are on the inside line and there exists only one point
			if (IntermediatePointIterator == 0)
			{ 
				
				if (intermediatePoints.Num() > 1) //then we walk towards the speed up point
				{	
					// calculate the speed towards the lastPointOfCurrentPath (therefore + 1)
					speed = CalculateChildSpeed(ACurrentWaypoint, intermediatePoints[IntermediatePointIterator + 1]); 
					speed = FMath::Clamp(speed, 0.0f, FMath::Min(ACurrentWaypoint->GetWalkingSpeed() * 1.5f, MAX_WALKING_SPEED));
					Cast<UCharacterMovementComponent>(AVirtualHumanPawn->GetMovementComponent())->MaxWalkSpeed = speed;
				}
				else // we are on the inside and need to slow down
				{
					speed = CalculateChildSpeed(ACurrentWaypoint, intermediatePoints[IntermediatePointIterator]); // calculate the speed towards the inner point
					speed = FMath::Clamp(speed, 0.0f, FMath::Min(ACurrentWaypoint->GetWalkingSpeed() * 1.5f, MAX_WALKING_SPEED));
					Cast<UCharacterMovementComponent>(AVirtualHumanPawn->GetMovementComponent())->MaxWalkSpeed = speed;
				}
			}

			if(IntermediatePointIterator == 1) // we are at the speedUpPoint and therefore need to increase our speed to be at the firstPointOfNextPath when parent reaches the waypoint
			{
				speed = CalculateChildSpeed(ACurrentWaypoint, intermediatePoints);
				speed = FMath::Clamp(speed, 0.0f, FMath::Min(ACurrentWaypoint->GetWalkingSpeed() * 1.3f, MAX_WALKING_SPEED));
				Cast<UCharacterMovementComponent>(AVirtualHumanPawn->GetMovementComponent())->MaxWalkSpeed = speed;
			}
			CurrentRequestID = VirtualHumanAIController->RequestMove(request, path->GetPath());
			IntermediatePointIterator++;
		}
		if (IntermediatePointIterator == intermediatePoints.Num()) {
			// so AIControlle does not call childMoveToWaypoint again
			// will be set to true by parent if he has reached the current waypoint
			bChildMovement = false;
		}


	}
}

TArray<AWaypoint*> UVHMovement::ShuffleArray(TArray<AWaypoint*> myArray)
{
	if (myArray.Num() > 0)
	{
		int32 LastIndex = myArray.Num() - 1;
		for (int32 i = 0; i <= LastIndex; ++i)
		{
			int32 Index = MovementRandomStream.RandRange(0, LastIndex - 1);
			if (i != Index)
			{
				myArray.Swap(Index, i);
			}
		}
	}
	return myArray;
}

/*
 *	This is to demonstrate how navigations via Waypoint works.
 *	Use WaypointBP to determine intermediary route points.
 *	You can determine the order of Waypoints in each instance in "WaypointOrder" field.
 *	Function polls all waypoints in the level and puts them into queue
 */
void UVHMovement::WaypointMovement()
{
	// If we have not collected and sorted the Waypoints in the scene do so
	if (Waypoints.Num() < 1)
	{
		AActor* AVHPawn = AVirtualHumanPawn;
		UWorld* World = GetWorld();

		StoreAllWaypoints();

		if (Waypoints.Num() < 1) {
			VH_ERROR("No Waypoints for WaypointGroup %i\n", WaypointGroup);
			return;
		}
		// sort Waypoint array accordingly
		switch (WaypointOrder)
		{
		case Closest:

			// first we find the closest waypoint to the VAs position, then we sort the array according to the distance between the waypoints

			// first we sort by distance to VA
			// we compare the distance of the path on the nav mesh, not the euclidean distance
			Waypoints.Sort([AVHPawn, World](AWaypoint& a, AWaypoint& b)
				{
					// only works if NavigationSystem is included in build.cs
					UNavigationPath* NpathToA = UNavigationSystemV1::FindPathToLocationSynchronously(
						World, AVHPawn->GetActorLocation(), a.GetActorLocation());

					UNavigationPath* NpathToB = UNavigationSystemV1::FindPathToLocationSynchronously(
						World, AVHPawn->GetActorLocation(), b.GetActorLocation());

					float distanceToA = NpathToA->GetPathLength(); //GetPathLength() is potentially slow says unreal
					float distanceToB = NpathToB->GetPathLength();

					return distanceToA < distanceToB;
				});


			// then sort by distance from one WP to the next one
			for (int i = 1; i < Waypoints.Num(); ++i)
			{
				for (int j = 2; j < Waypoints.Num(); ++j)
				{
					if (j < i)
					{
						continue;
					}
					UNavigationPath* NpathToi = UNavigationSystemV1::FindPathToLocationSynchronously(
						GetWorld(), Waypoints[i - 1]->GetActorLocation(), Waypoints[i]->GetActorLocation());

					UNavigationPath* NpathToJ = UNavigationSystemV1::FindPathToLocationSynchronously(
						GetWorld(), Waypoints[i - 1]->GetActorLocation(), Waypoints[j]->GetActorLocation());

					float distanceToI = NpathToi->GetPathLength(); //GetPathLength() is potentially slow says unreal
					float distanceToJ = NpathToJ->GetPathLength();

					if (distanceToI > distanceToJ)
					{
						Waypoints.Swap(i, j);
					}
					/*
					 * if GetPathLength() above is too slow we could use the euclidian distance
					if(Waypoints[i-1]->GetDistanceTo(Waypoints[i]) > Waypoints[i-1]->GetDistanceTo(Waypoints[j]))
					{
						Waypoints.Swap(i, j);
					}
					*/
				}
			}
			break;

		case Manual:

			Waypoints.Sort([](AWaypoint& a, AWaypoint& b)
				{
					return a.GetWaypointOrder() < b.GetWaypointOrder();
				});
			break;

		case Random:
			Waypoints = ShuffleArray(Waypoints);
			break;

		default:
			VH_ERROR("Waypoint Handling is not specified\n");
			break; // cannot be reached
		}

		// if we are the parent, we set the Waypoints for our childs
		if (!bChild)
		{
			int i = 1;
			for (TPair<AVirtualHuman*, float >& child : ChildVH)
			{
				if (!child.Key)
				{
					VH_ERROR("Virtual Human child is not valid\n");
					return;
				}
				UVHMovement* childMovement = child.Key->FindComponentByClass<UVHMovement>();
				childMovement->Waypoints = Waypoints;
				i++;
			}
		}
		// if we are the child, we set the Waypoints for the parent and for other childs
		if (bChild)
		{

			UVHMovement* parentMovement = parentVH->FindComponentByClass<UVHMovement>();
			if (!parentVH) 
			{
				VH_ERROR("child has no valid parent\n");
				return;
			}

			parentMovement->Waypoints = Waypoints;

			int i = 1;
			for (TPair<AVirtualHuman*, float >& child : parentMovement->ChildVH)
			{
				if (!child.Key)
				{
					VH_ERROR("Virtual Human child is not valid\n");
					return;
				}
				UVHMovement* childMovement = child.Key->FindComponentByClass<UVHMovement>();
				childMovement->Waypoints = Waypoints;
				i++;
			}
		}

	}


	if (Waypoints.Num() < 1)
	{
		// in case we are still empty, there is no a failure
		VH_ERROR("No Waypoints for WaypointGroup %i\n", WaypointGroup);
		return;
	}
	

	if(bChild)
	{
		CreateChildPoints();
	}
	else
	{
		MoveToWaypoint();
	}
}

/**
*	Internally store all the waypoints of our group
*	Return val: false of no waypoints are available, else true
**/
bool UVHMovement::StoreAllWaypoints()
{
	// ensure we do not have old ones:
	Waypoints.Empty();

	TArray<AActor*> WaypointsInScene;
	// Get all Waypoints in the Scene
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), WaypointsInScene);

	// Check for all Waypoints in the Scene to which movementGroup and therefore, to which VA they belong
	for (AActor* AWaypointInScene : WaypointsInScene)
	{
		AWaypoint* waypoint = Cast<AWaypoint>(AWaypointInScene);
		{
			if (waypoint->GetWaypointGroup() == WaypointGroup)
			{
				Waypoints.Add(waypoint);
			}
		}
	}
	
	return (Waypoints.Num() > 0);
}

void UVHMovement::CreateChildPoints()
{
	FVector startingPosition;
	switch (GroupFormation)
	{
	case Abreast:
		if (Waypoints.Num() > 1)
		{
			// around first waypoint from starting position
			startingPosition = CreateIntermediatePoints(NULL, Waypoints[0], Waypoints[1])[0];

			//  around 2nd to last -1 
			for (int i = 1; i < Waypoints.Num() - 1; i++) 
			{
				ChildPoints.Add(Waypoints[i],(CreateIntermediatePoints(Waypoints[i - 1], Waypoints[i], Waypoints[i + 1])));
			}

			// around last waypoint
			ChildPoints.Add(Waypoints.Last(), CreateIntermediatePoints(Waypoints.Last(1), Waypoints.Last(), Waypoints[0]));

			// around first waypoint from last waypoint
			if (IsLoop())
			{
				ChildPoints.Add(Waypoints[0], CreateIntermediatePoints(Waypoints.Last(), Waypoints[0], Waypoints[1]));
			}
			MoveToWaypoint(startingPosition, CalculateChildSpeed(Waypoints[0], startingPosition));
		}
		else 
		{
			// only one waypoint
			startingPosition = CreateIntermediatePoints(NULL, Waypoints[0], Waypoints[0])[0];
			MoveToWaypoint(startingPosition, CalculateChildSpeed(Waypoints[0], startingPosition));
		}
		break;

	case InLine:
		bLineMovement = true;
		AWaypoint* first;
		AWaypoint* next;
		FVector direction;
		// we just need an aditional starting point that the child moves to in the time the parent walks to the first waypoint
		if (Waypoints.Num() > 1)
		{

			first = Waypoints[0];
			next = Waypoints[1];
			direction = first->GetActorLocation() - next->GetActorLocation();
		}
		else
		{
			first = Waypoints[0];
			direction = parentVH->GetActorLocation() - first->GetActorLocation();
		}

		direction.Normalize();
		FVector offsetToChildStartingPoint = OffsetFromWaypoint * direction;
		startingPosition = first->GetActorLocation() + offsetToChildStartingPoint;

		float childSpeed = CalculateChildSpeed(first, startingPosition);

		// we skip walking to the first waypoint if:
		// the child walks in front of the parent	or	we only have one waypoint, then the child just walks to the starting position
		if (OffsetFromWaypoint < 0					||	Waypoints.Num() == 1)
		{
			CurrentWaypointIterator++;
		}

		MoveToWaypoint(startingPosition, childSpeed); //after this is done VirtualHumanAIController will automatically call MoveToWaypoint()

		if (bDrawDebugSpheres)
		{
			DrawDebugSphere(GetWorld(), startingPosition, 50.0f, 30.0f, FColor::Green, true);
		}
		break;
	}
}

TArray<FVector>  UVHMovement::CreateIntermediatePoints(AWaypoint* lastWP, AWaypoint* currentWP, AWaypoint* nextWP)
{
	TArray<FVector> newPoints;
	FVector last;

	if (lastWP == NULL) // special case when walking from starting position to first waypoint
	{
		last = parentVH->GetActorLocation();
	}
	else 
	{
		last = lastWP->GetActorLocation();
	}
	
	FVector current = currentWP->GetActorLocation();
	FVector next = nextWP->GetActorLocation();

	FVector dirToLastWaypoint = last - current;
	FVector dirToNextWaypoint = next - current;
	dirToLastWaypoint.Normalize();
	dirToNextWaypoint.Normalize();
	float innerAngle = FMath::Acos(FVector::DotProduct(dirToLastWaypoint, dirToNextWaypoint));
	innerAngle = FMath::RadiansToDegrees(innerAngle);

	// figure out sign of angle
	FVector crossProduct = FVector::CrossProduct(dirToLastWaypoint, dirToNextWaypoint);
	float sign = 1.0f;
	if (crossProduct.Z > 0) 
	{
		sign = -1.0f;
	}
	
	// angle in movement direction (with sign to figure out if we rotate right (+) or left (-))
	float angleInMoveDir = sign * abs(innerAngle - 180);
	FVector dirToCurrentWaypoint = -dirToLastWaypoint;
	FVector directionToChildPoint = dirToCurrentWaypoint.RotateAngleAxis(-90, FVector(0, 0, 1));

	directionToChildPoint.Normalize();
	FVector childLocationOffset = directionToChildPoint * FVector(OffsetFromWaypoint, OffsetFromWaypoint, 0);

	FVector LastPointOfCurrentPath = current + childLocationOffset;

	// create speedUpPoint if we are on the outside path:
	// we make a right turn and offset is to the left					or		we make a left turn and offset is to the right
	if ((angleInMoveDir > 0 && lastWP != NULL && OffsetFromWaypoint > 0) || (angleInMoveDir < 0 && lastWP != NULL && OffsetFromWaypoint < 0)) {
		//calculate a point towards the end of the path to the next waypoint at which the child begins to walk faster (if he has the outside path)
		FVector speedUpPoint = LastPointOfCurrentPath - 0.40 * (current - last);

		newPoints.Add(speedUpPoint);

		if (bDrawDebugSpheres) 
		{
			DrawDebugSphere(GetWorld(), speedUpPoint, 50.0f, 30.0f, FColor::Magenta, true);
		}
	}

	newPoints.Add(LastPointOfCurrentPath);

	if (bDrawDebugSpheres)
	{
		DrawDebugSphere(GetWorld(), LastPointOfCurrentPath, 50.0f, 30.0f, FColor::Red, true);
	}

	// if the angle is very big we want the child to just turn around instead of walking a curve
	// this requires a side change (if the child walks to the left of the parent, in walking direction, it will now be on the right side)
	if (angleInMoveDir > 140) {
		OffsetFromWaypoint = -1 * OffsetFromWaypoint; // side change
		angleInMoveDir -= 180; // for the remaining angle we still calculate intermediate points

	}
	if (angleInMoveDir < -140) {
		OffsetFromWaypoint = -1 * OffsetFromWaypoint;
		angleInMoveDir += 180;
	}

	// create intermediate points if we are on the outside path:
	// we make a right turn and offset is to the left						or		we make a left turn and offset is to the right
	if ( (angleInMoveDir > 0 && lastWP != NULL && OffsetFromWaypoint > 0) || (angleInMoveDir < 0 && lastWP != NULL && OffsetFromWaypoint < 0)) {

		// intermediate point (especially usefull if angles between waypoints is very big)
		// could be neglected for perfromace reasons
		FVector intermediatePointOffset = childLocationOffset.RotateAngleAxis(angleInMoveDir / 2, FVector(0, 0, 1));
		FVector intermediatePoint = current + intermediatePointOffset;

		newPoints.Add(intermediatePoint);

		FVector firstPointOfNextPathOffset = childLocationOffset.RotateAngleAxis(angleInMoveDir, FVector(0, 0, 1));
		FVector firstPointOfNextPath = current + firstPointOfNextPathOffset;


		newPoints.Add(firstPointOfNextPath);

		if (bDrawDebugSpheres)
		{
			DrawDebugSphere(GetWorld(), intermediatePoint, 50.0f, 30.0f, FColor::Orange, true);
			DrawDebugSphere(GetWorld(), firstPointOfNextPath, 50.0f, 30.0f, FColor::Green, true);
		}
	}

	return newPoints;
}


float UVHMovement::CalculateChildSpeed(AWaypoint* nextWaypoint, TArray<FVector> points)
{

	if (points.Num() < 2) {
		VH_ERROR("Not enough points to calculate child speed, so we set it to zero.\n");
		return 0.f;
	}
	UNavigationPath* parentPath = UNavigationSystemV1::FindPathToLocationSynchronously(
		GetWorld(), parentVH->GetActorLocation(), nextWaypoint->GetActorLocation());

	UNavigationPath* childPath = UNavigationSystemV1::FindPathToLocationSynchronously(
		GetWorld(), AVirtualHumanPawn->GetActorLocation(), points[1]);
	float childDistance = childPath->GetPathLength();

	for (int i=1; i<points.Num() -1 ; i++) 
	{
		childPath = UNavigationSystemV1::FindPathToLocationSynchronously(
			GetWorld(), points[i], points[i+1]);
		childDistance += childPath->GetPathLength();
	}

	float parentSpeed = nextWaypoint->GetWalkingSpeed();

	//distance to travel
	float parenttDistance = parentPath->GetPathLength();

	//time
	float walkingTime = (parenttDistance / parentSpeed);

	//child spped
	float childSpeed = (childDistance / walkingTime);
	return childSpeed;
}

float UVHMovement::CalculateChildSpeed(AWaypoint* nextWaypoint, FVector childPoint)
{
	UNavigationPath* parentPath = UNavigationSystemV1::FindPathToLocationSynchronously(
		GetWorld(), parentVH->GetActorLocation(), nextWaypoint->GetActorLocation());

	UNavigationPath* childPath = UNavigationSystemV1::FindPathToLocationSynchronously(
		GetWorld(), AVirtualHumanPawn->GetActorLocation(), childPoint);
	float childDistance = childPath->GetPathLength();

	//distance to travel
	float parentSpeed = nextWaypoint->GetWalkingSpeed();

	float parenttDistance = parentPath->GetPathLength();

	//time
	float walkingTime = (parenttDistance / parentSpeed);

	//child spped
	float childSpeed = (childDistance / walkingTime);

	return childSpeed;
}

FHitResult UVHMovement::IKFootTrace(FName Socket, float TraceDistance)
{
	USkeletalMeshComponent* mesh = AVirtualHumanPtr->GetBodyMesh();
	FVector SocketLocation = mesh->GetSocketLocation(Socket);
	FVector ActorLocation = AVirtualHumanPtr->GetActorLocation();

	// line trace start location
	FVector LineTraceStart = SocketLocation;
	LineTraceStart.Z = ActorLocation.Z;

	// line trace end location
	FVector LineTraceEnd = SocketLocation;
	LineTraceEnd.Z = SocketLocation.Z - IKTraceDistance;

	FHitResult Hit;
	FCollisionQueryParams TraceParams;
	TraceParams.bTraceComplex = true;
	TraceParams.AddIgnoredActor(AVirtualHumanPtr);
	bool hitFound = GetWorld()->LineTraceSingleByChannel(Hit, LineTraceStart, LineTraceEnd, ECC_Visibility, TraceParams);
	// DrawDebugLine(GetWorld(), LineTraceStart, LineTraceEnd, FColor::Cyan, false, 0.1f);
	return Hit;
}


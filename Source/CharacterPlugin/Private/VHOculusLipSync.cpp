// Fill out your copyright notice in the Description page of Project Settings.


#include "VHOculusLipSync.h"

#include "HAL/PlatformFileManager.h"
//#include "PlatformFile.h"
#include "GenericPlatform/GenericPlatformFile.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"
#include "GameFramework/Actor.h"
#include "Engine.h"
#include "Animation/AnimSequence.h"
#include "Misc/AssertionMacros.h"
#include "Helper/CharacterPluginLogging.h"


bool UVHOculusLipSync::InternalLoadFile(FString LocalFilename) {

	if(Mapping == nullptr)
	{
		VH_ERROR("[UVHOculusLipSync::InternalLoadFile] Mapping is not specified!\n");
		return false;
	}

	TArray<FName> BlendshapeNames = Mapping->GetCurveFNames();
	TArray<TArray<float>> BlendshapeValues;
	for(FName PoseName : Mapping->GetPoseFNames())
	{
		BlendshapeValues.Add(Mapping->GetCurveValues(Mapping->GetPoseIndexByName(PoseName)));
	}
	
  //Load visemes.txt
  FString FileAsString;
  FString Filename = FPaths::Combine(FPaths::ProjectContentDir(), LocalFilename);
  IPlatformFile &File = FPlatformFileManager::Get().GetPlatformFile();
  if (File.FileExists(*Filename)) {

    FFileHelper::LoadFileToString(FileAsString, *Filename);
  }
  TArray<FString> Lines;
  FileAsString.ParseIntoArrayLines(Lines, true);

	AnimationData.BlendshapeNames.Empty();
	AnimationData.TimeSteps.Empty();
	for(FName Name : BlendshapeNames)
	{
		AnimationData.BlendshapeNames.Add(Name.ToString());
	}
	
	int FrameNumber = 0;

  for (FString Line : Lines) {

    if (Line.StartsWith("#Framerate")) {
      TArray<FString> Entries;
      Line.ParseIntoArrayWS(Entries);
      if (Entries.Num() == 2)
        Framerate = FCString::Atof(*Entries[1]);
    }

    if (Line.StartsWith("#"))
      continue;

		FFrameData FrameData;
		FrameData.Timestamp = FrameNumber / Framerate;

    TArray<FString> Entries;
    Line.ParseIntoArrayWS(Entries);
    TArray<float> VisemeActivations;
    for (FString Entry : Entries) {
			VisemeActivations.Add(FCString::Atof(*Entry));
    }

		TArray<float> Activations;
		Activations.Init(0.0f, BlendshapeNames.Num());

		//gather data of all potentially active visemes in this frame
		for (int i = 0; i < BlendshapeNames.Num(); ++i) {
			float BlendshapeActivation = 0.0f;
			for (int j = 0; j < VisemeActivations.Num(); ++j) {
				BlendshapeActivation += VisemeActivations[j] * BlendshapeValues[j][i];
			}
			FrameData.BlendshapeActivations.Add(BlendshapeActivation);
		}
		AnimationData.TimeSteps.Add(FrameData);
		++FrameNumber;
  }
  return true;
}

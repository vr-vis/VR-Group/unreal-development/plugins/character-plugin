#include "CharacterPlugin.h"

#define LOCTEXT_NAMESPACE "FCharacterPluginModule"

#include "Helper/CharacterPluginLogging.h"

void FCharacterPluginModule::StartupModule ()
{
  CharacterPluginLogging::InitializeLoggingStreams();
}
void FCharacterPluginModule::ShutdownModule()
{

}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FCharacterPluginModule, CharacterPlugin)
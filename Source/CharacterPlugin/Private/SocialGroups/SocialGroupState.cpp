// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/SocialGroupState.h"

#include "Helper/CharacterPluginLogging.h"
#include "Kismet/GameplayStatics.h"

// Add default functionality here for any ISocialGroupState functions that are not pure virtual.

void ISocialGroupState::Tick(ASocialGroup* SG)
{
}

void ISocialGroupState::ToIdle(ASocialGroup* SG)
{
}

void ISocialGroupState::ToGazing(ASocialGroup* SG)
{
}

void ISocialGroupState::ToInGroup(ASocialGroup* SG)
{
}

void ISocialGroupState::ToLeaving(ASocialGroup* SG)
{
}

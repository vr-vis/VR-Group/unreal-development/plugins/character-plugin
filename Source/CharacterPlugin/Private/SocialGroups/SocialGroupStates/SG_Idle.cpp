// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/SocialGroupStates/SG_Idle.h"

#include "Helper/CharacterPluginLogging.h"
#include "SocialGroups/SocialGroupStates/SG_Gazing.h"

void USG_Idle::ToGazing(ASocialGroup* SG)
{
	// only gaze to the user if he is not in the group yet
	if (!SG->bPlayerIsInGroup || !SG->bUser)
	{
		SG->SortByDistanceToActor(SG->TrackedPawnLocation);
		for (int i = 0; i < 2; i++)
		{
			SG->GetSocialGroupsComponent(SG->AgentsInGroup[i])->StartGazeToUserTimer();
		}
		//UniLog.Log("Gazing", "VHDebug");
		// change to gazing
		SG->ChangeState(SG->Gazing);
	}
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/SocialGroupStates/SG_InGroup.h"

#include "IUniversalLogging.h"
#include "SocialGroups/SocialGroupStates/SG_Leaving.h"

void USG_InGroup::ToLeaving(ASocialGroup* SG)
{
	// make player opposite of user play a waving animation
	SG->SortByAngleToPlayer();
	SG->AgentsInGroup.Last()->PlayAnimMontage(SG->WavingMontages.Last());
	SG->bPlayerIsInGroup = false;
	// changing group radius, will make agents adjust themselves automatically (see GetCircleForce() in VHSocialGroups)
	SG->GroupRadius = SG->GetGroupRadius(SG->AgentsInGroup.Num());
	//UniLog.Log("Leaving", "VHDebug");
	SG->ChangeState(SG->Leaving);
}

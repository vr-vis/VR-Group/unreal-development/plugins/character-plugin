// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/SocialGroupStates/SG_Gazing.h"

#include "Helper/CharacterPluginLogging.h"
#include "SocialGroups/SocialGroupStates/SG_Idle.h"
#include "SocialGroups/SocialGroupStates/SG_InGroup.h"

void USG_Gazing::Tick(ASocialGroup* SG)
{
	FVector DistanceToTrackedPawn = SG->TrackedPawnLocation - SG->GetActorLocation();
	// sort Agents according to distance to user
	SG->SortByDistanceToActor(SG->TrackedPawnLocation);
	// the two closest agents gaze towards the user
	for (int i = 0; i < 2; i++)
	{
		auto VH = SG->AgentsInGroup[i];
		// to stop the gazing after 3 seconds and allow other gazing
		if (SG->GetSocialGroupsComponent(VH)->bGazeToUser) //flipped by the GazeToUserTimer
		{
			UVHGazing* GazingComponent = SG->AgentsInGroup[i]->FindComponentByClass<UVHGazing>();
			// in case we track the user, we gaze to the users head
			if(SG->bUser)
			{
				GazingComponent->GazeToUser();
			} else
			{
				// we gaze to an agent
				GazingComponent->GazeToActor(SG->AgentPawn, FVector(0, 0, SG->AgentPawn->BaseEyeHeight));
			}
		}
	}
}


void USG_Gazing::ToInGroup(ASocialGroup* SG)
{
	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	SGGazingRandomStream = FRandomStream(TextKeyUtil::HashString(SG->GetName()));
	// delete timer for all
	for (int i = 0; i < SG->AgentsInGroup.Num(); i++)
	{
		SG->GetSocialGroupsComponent(SG->AgentsInGroup[i])->ClearGazeToUserTimer();
	}
	SG->bPlayerIsInGroup = true;

	SG->GroupRadius = SG->GetGroupRadius(SG->AgentsInGroup.Num());

	// look at user for 1 to 2.5 seconds
	LookAtUserOnJoin(SG);
	// stop dialogue for a couple of seconds if the user joins the group
	if (SG->bStartDialogue)
	{
		SG->PauseDialogue(3.0f);
	}
	// start random gazing pattern after initial delay
	SG->StartRandomGazingTimer(SGGazingRandomStream.FRandRange(2.0f, 4.0f));

	// make the agent opposite to the user play a waving animation
	SG->SortByAngleToPlayer();
	SG->AgentsInGroup[0]->PlayAnimMontage(SG->WavingMontages[SGGazingRandomStream.RandRange(0,SG->WavingMontages.Num()-1)]);
	//UniLog.Log("InGroup", "VHDebug");
	SG->ChangeState(SG->InGroup);
}

void USG_Gazing::ToIdle(ASocialGroup* SG)
{
	if (!SG->bPlayerIsInGroup || !SG->bUser)
	{
		// delete timer for all
		for (int i = 0; i < SG->AgentsInGroup.Num(); i++)
		{
			SG->GetSocialGroupsComponent(SG->AgentsInGroup[i])->ClearGazeToUserTimer();
		}
		//UniLog.Log("Idle", "VHDebug");
		SG->ChangeState(SG->Idle);
	}
}

void USG_Gazing::LookAtUserOnJoin(ASocialGroup* SG)
{
	for (int i = 0; i < SG->AgentsInGroup.Num(); i++)
	{
		const auto VH = SG->AgentsInGroup[i];
		auto SGComp = SG->GetSocialGroupsComponent(VH);
		SGComp->EndGazing();
		UVHGazing* GazingComponent = SG->AgentsInGroup[i]->FindComponentByClass<UVHGazing>();
		// in case we track the user, we gaze to the users head, which is a scene component as the users is a VRPawn
		if (SG->bUser)
		{
			GazingComponent->GazeToComponent(SG->VRPawn->HeadCameraComponent);
		}
		else
		{
			// we gaze to an agent
			GazingComponent->GazeToActor(SG->AgentPawn, FVector(0, 0, SG->AgentPawn->BaseEyeHeight));
		}
	}
}

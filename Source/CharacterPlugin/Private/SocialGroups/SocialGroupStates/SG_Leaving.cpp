// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/SocialGroupStates/SG_Leaving.h"

#include "IUniversalLogging.h"
#include "SocialGroups/SocialGroupStates/SG_Idle.h"

void USG_Leaving::ToIdle(ASocialGroup* SG)
{
	//UniLog.Log("Idle", "VHDebug");
	SG->ChangeState(SG->Idle);
}

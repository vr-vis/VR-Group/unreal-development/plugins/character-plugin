// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/SocialGroup.h"
#include "VirtualHuman.h"
#include "SocialGroups/VHSocialGroups.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "Engine.h"
#include "Runtime/Engine/Classes/Engine/World.h"

#include "Kismet/KismetMathLibrary.h"
#include "VHGazing.h"
#include "VHSpeech.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Engine/AssetManager.h"
#include "Helper/CharacterPluginLogging.h"
#include "Pawn/RWTHVRPawn.h"
#include "SocialGroups/SocialGroupStates/SG_Gazing.h"
#include "SocialGroups/SocialGroupStates/SG_Idle.h"
#include "SocialGroups/SocialGroupStates/SG_InGroup.h"
#include "SocialGroups/SocialGroupStates/SG_Leaving.h"
#include "SoundSource/VAAudiofileSourceComponent.h"

// Sets default values
ASocialGroup::ASocialGroup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Default Parameters
	GroupRadius = 0.0f;
	DialogueIterator = 0;
	NumberOfAgentsInConversation = 2;
	bLoopDialogue = true;
	bSpawnAsGroup = true;
	bStartDialogue = false;
	TrackedAgentIt = 0;
	//starting state of the social group
	bPlayerIsInGroup = false;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	CollisionSphere->InitSphereRadius(100); // will be resized later depending on the group radius
	CollisionSphere->SetupAttachment(RootComponent);
	CollisionSphere->SetGenerateOverlapEvents(true);
	CollisionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	bUser = true;

}

// Called when the game starts or when spawned
void ASocialGroup::BeginPlay()
{
	
	Super::BeginPlay();
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ASocialGroup::OnSphereBeginOverlap);
	CollisionSphere->OnComponentEndOverlap.AddDynamic(this, &ASocialGroup::OnSphereEndOverlap);

	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the SocialGroup, so random behavior is different for every SG
	SocialGroupRandomStream = FRandomStream(TextKeyUtil::HashString(GetName()));

	VRPawn = Cast<ARWTHVRPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	if(!VRPawn)
	{
		VH_ERROR("[ASocialGroup::BeginPlay] The pawn is not a AVirtualRealityPawn, this method does not work!")
	}
	
	if(!bSpawnAsGroup)
	{
		GroupSize = AgentsInGroup.Num();
	}
	
	if (GroupSize < 2)
	{
		// A Social Group should at least contain two agents
		VH_WARN("[ASocialGroup::BeginPlay] Social Groups have to contain at least 2 agents\n");
		return;
	}
	if (GroupSize > 10)
	{
		VH_WARN("[ASocialGroup::BeginPlay] Group size is larger than 10! Social Groups have only been tested to work with at most 10 agents. Proceed at own risk.\n")
	}
	
	//spawn specified amount of agents at start of the game, instead of adding agents manually
	if (bSpawnAsGroup)
	{
		GroupRadius = GetGroupRadius(GroupSize);
		AgentsInGroup = SpawnInGroupFormation();
		StartRandomGazingTimer(2.0f);
	} else // AgentsInGroup will be filled via the editor by the user
	{
		GroupRadius = GetGroupRadius(AgentsInGroup.Num());
		StartRandomGazingTimer(2.0f);
	}
	// initialize all parameters
	for(int i = 0; i < AgentsInGroup.Num(); i++)
	{
		GetSocialGroupsComponent(AgentsInGroup[i])->InitializeParameters(this);
		auto MovementComp = AgentsInGroup[i]->FindComponentByClass<UCharacterMovementComponent>();
		if (MovementComp)
		{
			// always stay oriented towards group center
			// needs to be disabled, when agent leaves group
			MovementComp->bOrientRotationToMovement = false;
		}
	}


	// set Animation Montages used during group behavior
	WavingMontages = LoadAnimationMontages();
	if(WavingMontages.Num() < 1)
	{
		VH_ERROR("[ASocialGroup::BeginPlay] Could not load waving animation.\n")
		return;
	}

	//preloading here takes some time if too many agents are involved,
	//since all audio files are loaded for all agents since we don'T know who will speak it
	//so rather call it manually if you need that, better just set bStartDialogue (then the right things are preloaded)
	//PreloadDialogue();
	
	CollisionSphere->SetSphereRadius(GroupRadius + 1.2 * GazeDistance);
	if(bStartDialogue)
	{
		StartDialogue(5.0f);
	}

	//init states
	Idle = NewObject<USG_Idle>();
	Gazing = NewObject<USG_Gazing>();
	Leaving = NewObject<USG_Leaving>();
	InGroup = NewObject<USG_InGroup>();
	
	// add starting state of user (index 0)
	GroupStates.Add(Idle);
}

// Called every frame
void ASocialGroup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	// if iterator is 0 we track the user, if the iterator is 1 or bigger we track all agents, which are inside the collision sphere
	if(TrackedAgentIt == 0)
	{
		// if the TrackedAgentIt is 0 we track the player
		AgentPawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
		bUser = true;
		if (VRPawn)
		{
			TrackedPawnLocation = VRPawn->HeadCameraComponent->GetComponentLocation();
		}
		else
		{
			// take location of whatever pawn the user is using
			TrackedPawnLocation = UGameplayStatics::GetPlayerPawn(GetWorld(), 0)->GetActorLocation();
		}
	}
	else if(TrackedAgentIt >= 1)
	{
		AgentPawn = Cast<APawn>(CloseAgents[TrackedAgentIt-1]);
		if(!AgentPawn)
		{
			VH_ERROR("[ASocialGroup::Tick] Cast to pawn failed\n")
			return;
		}
		bUser = false;
		TrackedPawnLocation = AgentPawn->GetActorLocation();
	}
	
	GroupStates[TrackedAgentIt]->Tick(this);
	const FVector ToTrackedPawn = TrackedPawnLocation - GetActorLocation();
	
	//*********************************************************************
	// state transitions
	//*********************************************************************
		if (ToTrackedPawn.Size() < GroupRadius + GazeDistance)
		{
			GroupStates[TrackedAgentIt]->ToGazing(this);
		}
		
		if (IsUserOrientedToGroup(ToGroupThreshold) && ToTrackedPawn.Size() < GroupRadius + JoinDistance)
		{
			if(!bPlayerIsInGroup && bUser)
			{
				GroupStates[TrackedAgentIt]->ToInGroup(this);
			}
		}
		
		if (ToTrackedPawn.Size() > GroupRadius + GazeDistance + GroupRadius / 3)
		{
			GroupStates[TrackedAgentIt]->ToIdle(this);
		}
		
		if (!IsUserOrientedToGroup(ToGroupThreshold) && ToTrackedPawn.Size() > GroupRadius + StillStandingVariance)
		{
			GroupStates[TrackedAgentIt]->ToLeaving(this);
		}
	

	// when join agent function is called, this will be true
	if(bAgentIsJoining)
	{
		const FVector DistanceToGroup = GetActorLocation() - JoiningAgent->GetActorLocation();
		if (DistanceToGroup.Size() < GroupRadius + JoinDistance)
		{
			AgentsInGroup.Add(JoiningAgent);
			JoiningAgent->GetCharacterMovement()->StopActiveMovement();
			JoiningAgent->GetCharacterMovement()->bOrientRotationToMovement = false;
			GroupRadius = GetGroupRadius(AgentsInGroup.Num());
			GetSocialGroupsComponent(JoiningAgent)->InitializeParameters(this);
			if(bStartDialogue)
			{
				RestartDialogue(3.0f);
			}
			bAgentIsJoining = false;
		}
	}

	//iterate over all agents, that are in the collision sphere around the group
	TrackedAgentIt++;
	if (TrackedAgentIt >= CloseAgents.Num() +1) //+1 as TrackedAgentIt=0 is the player
	{
		TrackedAgentIt = 0;
	}

}

void ASocialGroup::ChangeState(TScriptInterface<ISocialGroupState> NextState)
{
	GroupStates[TrackedAgentIt] = NextState;
}

// returns true if user rotation towards the group center is below the threshold given in degree
bool ASocialGroup::IsUserOrientedToGroup(float Threshold)
{
	FVector UserForward = VRPawn->HeadCameraComponent->GetForwardVector();
	FVector UserToGroupCenter = TrackedPawnLocation - GetActorLocation();
	FVector2D UserForwardXY = FVector2D(UserForward.X, UserForward.Y);
	FVector2D UserToGroupCenterXY = FVector2D(UserToGroupCenter.X, UserToGroupCenter.Y);

	UserForwardXY.Normalize();
	UserToGroupCenterXY.Normalize();
	
	float Angle = FMath::Abs(FVector2D::DotProduct(UserForwardXY, UserToGroupCenterXY));
	Angle = FMath::Acos(Angle);
	Angle = FMath::RadiansToDegrees(Angle);
	return Angle < Threshold;
}

void ASocialGroup::DestroyAgent()
{
	if (AgentsInGroup.Num() > 0)
	{
		AgentsInGroup.Pop()->Destroy();
	}
}

float ASocialGroup::GetGroupRadius(int NumberOfAgents) const
{
	if (bPlayerIsInGroup)
	{
		NumberOfAgents ++;
	}

	// arc measure: b = alpha/360 * 2 * pi * r => r = b * 360 / (alpha * 2 * pi)
	// alpha = NumberOFAgents / 360
	// => r = b * NumberOfAgents / ( 2 * pi )
	// arc measure b is equal to the distance between each VA
	return ((ConversationDistance) * NumberOfAgents) / (2 * PI);
}

// calculate the positions on the circle for the given number of agents.
// positions depend also on the SocialDistance.
TArray<FVector> ASocialGroup::GetCirclePositions(int NumberOfAgents)
{
	GroupRadius = GetGroupRadius(NumberOfAgents);
	if(bPlayerIsInGroup)
	{
		NumberOfAgents++;
	}
	float Alpha = 360.0 / NumberOfAgents;
	TArray<FVector> Positions;
	
	for (int i = 0; i < NumberOfAgents; i++)
	{
		const float RandomDeviation = SocialGroupRandomStream.FRandRange(-5.0f, 5.0f);
		const float x = (GetActorLocation().X + GroupRadius * cos(FMath::DegreesToRadians(Alpha * (i + 1)))) + RandomDeviation;
		const float y = (GetActorLocation().Y + GroupRadius * sin(FMath::DegreesToRadians(Alpha * (i + 1)))) + RandomDeviation;
		FVector Pos = FVector(x, y, GetActorLocation().Z);

		//DrawDebugSphere(GetWorld(), Pos, 50.0f, 30.0f, FColor::Green, true);
		Positions.Add(Pos);
	}

	return Positions;
}

void ASocialGroup::SortByDistanceToActor(FVector ActorLocation)
{
	FVector ActorPos = ActorLocation;
	// sort the array according to the distance to the ActorLocation
	AgentsInGroup.Sort([ActorPos](AVirtualHuman& vh1, AVirtualHuman& vh2)
	{
			FVector d1 = ActorPos - vh1.GetActorLocation();
			FVector d2 = ActorPos - vh2.GetActorLocation();

			return d1.Size() < d2.Size();
	});
}

// returns the speaker of the group, or nulptr if there is no speaker
AActor* ASocialGroup::GetSpeaker()
{
	for(int i = 0; i < AgentsInGroup.Num();	i++)
	{
		auto VH = AgentsInGroup[i];
		auto SGComp = GetSocialGroupsComponent(VH);
		if (SGComp->GetIsSpeaker())
		{

			return AgentsInGroup[i];
		}
	}
	return nullptr;
}

TArray<AVirtualHuman*> ASocialGroup::SpawnInGroupFormation()
{
	TArray<AVirtualHuman*> Agents;
	if (VHTypes.Num()<=0)
	{
		VH_ERROR("[ASocialGroup::SpawnInGroupFormation] Could not spawn agents. Please specify a subclass of virtual human, that should be spawned.\n");
		return Agents;
	}

	// remove all agents
	while (AgentsInGroup.Num() > 0)
	{
		DestroyAgent();
	}
	
	TArray<FVector> CirclePositions = GetCirclePositions(GroupSize);
	for (int i = 0; i < GroupSize; i++)
	{
		TSubclassOf<AVirtualHuman> VHType = VHTypes[i % (VHTypes.Num())];
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		FVector SpawnLocation = CirclePositions[i];
		//adjust z value of spawn location. Otherwise VAs are stuck in ground.
		SpawnLocation.Z = SpawnLocation.Z + 80.0f;
		FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(CirclePositions[i], GetActorLocation());
		AVirtualHuman* VH = GetWorld()->SpawnActor<AVirtualHuman>(VHType, SpawnLocation, FRotator(0,Rotation.Yaw,0), SpawnParams);

		// check if the agent has all the necessary components
		
		auto SGComp = GetSocialGroupsComponent(VH);
		if(!SGComp)
		{
			continue;
		}
		if(!SGComp->InitializeParameters(this))
		{
			continue;
		}
		
		// Rotate agent towards the center

		auto GC = VH->FindComponentByClass<UVHGazing>();
		GC->StopGazing();
		
		auto MovementComp = VH->FindComponentByClass<UCharacterMovementComponent>();
		if (MovementComp)
		{
			MovementComp->bOrientRotationToMovement = false;
		}
		if (!VH)
		{
			VH_ERROR("[ASocialGroup::SpawnInGroupFormation] The Spawned Virtual Human is not valid.\n")
			return Agents;
		}
		Agents.Add(VH);
	}
	return Agents;
}

bool ASocialGroup::StartDialogue(float InitialDelay)
{
	// if bStartDialogue was not true the AudioComponents of the social group need to be initialized since that only happened for the social groups where the dialogue was started before.
	if (bStartDialogue == false) {
		bStartDialogue = true;
		for (int i = 0; i < AgentsInGroup.Num(); i++)
		{
			auto VH = AgentsInGroup[i];
			auto SGComp = GetSocialGroupsComponent(VH);
			SGComp->InitializeParameters(this);
		}
	}
	SetupDialogue();
	GetWorld()->GetTimerManager().SetTimer(SpeakerTimerHandle, this, &ASocialGroup::SetSpeaker, InitialDelay,false);
	return true;

}

void ASocialGroup::NextDialogue()
{
	DialogueIterator++;
	GetWorld()->GetTimerManager().SetTimer(SpeakerTimerHandle, this, &ASocialGroup::SetSpeaker, 1.5, false);
}


bool ASocialGroup::SetupDialogue()
{
	// reset array
	AgentsInConversation.Empty();
	if (NumberOfAgentsInConversation > Dialogue.Num())
	{
		VH_WARN("There should be %d agents in dialog, but only %d different dialog options exist.\n NumberOfAgentsInConversation has been set to %d\n",
			NumberOfAgentsInConversation, Dialogue.Num(), Dialogue.Num());
		NumberOfAgentsInConversation = Dialogue.Num();
	}

	if(AgentsInGroup.Num() < NumberOfAgentsInConversation)
	{
		VH_WARN("There are only %d agents in the group. Cannot set up a conversation with %d agents\n", AgentsInGroup.Num(), NumberOfAgentsInConversation);
		return false;
	}
	
	TArray<AVirtualHuman*> Agents = ShuffleArray(AgentsInGroup);
	int DiaIt = 0;
	// assign NumberOfAgentsInConversation dialogues to agents
	for(int AgentIt = 0; AgentIt < Agents.Num() -1;)
	{
		if(DiaIt < NumberOfAgentsInConversation)
		{
			if (Agents[AgentIt]->GetGender() == Dialogue[DiaIt].Gender)
			{
				FAgentDialogueUtterance NewDialog;
				NewDialog.VH = Agents[AgentIt];
				NewDialog.Utterance = Dialogue[DiaIt];
				AgentsInConversation.Add(NewDialog);
				Agents.RemoveAt(AgentIt);
				DiaIt++;
				AgentIt = 0;
			}
			else
			{
				AgentIt++;
			}
		} else
		{
			// end for loop
			AgentIt = Agents.Num() - 1;
		}
	}

	if(AgentsInConversation.Num() < NumberOfAgentsInConversation)
	{
		VH_WARN("Could not match all dialogs to agents corretly. Check if the gender of the agents matches the gender of the dialog");
		// match unmatched dialogue to other agents
		while(AgentsInConversation.Num() < NumberOfAgentsInConversation)
		{
			FAgentDialogueUtterance NewDialog;
			NewDialog.VH = Agents[0];
			NewDialog.Utterance = Dialogue[DiaIt];
			AgentsInConversation.Add(NewDialog);
			Agents.RemoveAt(0);
			DiaIt++;
		}
	}

	// it might be the case, that there are multiple dialogues for the same agent
	// now we need to assign the other Dialogues, to the agents, that are in dialogue
	for(int z = NumberOfAgentsInConversation; z < Dialogue.Num(); z++)
	{
		FAgentDialogueUtterance NewDialogue;
		NewDialogue.VH = AgentsInConversation[z % NumberOfAgentsInConversation].VH;
		NewDialogue.Utterance = Dialogue[z];
		AgentsInConversation.Add(NewDialogue);
	}

	// PreLoad animation and audio to save performance
	for(auto Conv : AgentsInConversation)
	{
		GetSpeechComponent(Conv.VH)->PreloadUtterance(Conv.Utterance);
	}
	return true;
}

void ASocialGroup::PreloadDialogue()
{
	//try to preload what possible, for changing groups etc. this has to be done at runtime again in SetupDialogue and will cause lags

	//but since we don't know who is speaking what we preload everything for everyone
	//this can be very intense, so use with care
	for(AVirtualHuman* Agent : AgentsInGroup)
	{
		for(const FDialogueUtterance &Utterance : Dialogue)
		{
			GetSpeechComponent(Agent)->PreloadUtterance(Utterance);
		}
	}
}

void ASocialGroup::StopDialogue()
{
	for (AVirtualHuman* VH : AgentsInGroup)
	{
		auto SGComp = GetSocialGroupsComponent(VH);
		if (SGComp->GetIsSpeaker())
		{
			GetSpeechComponent(VH)->StopDialogue();
		}
	}
	ClearSpeakerTimer();
}

void ASocialGroup::PauseDialogue(float Duration)
{
	for (AVirtualHuman* VH : AgentsInGroup)
	{
		auto SGComp = GetSocialGroupsComponent(VH);
		if (SGComp->GetIsSpeaker())
		{
			GetSpeechComponent(VH)->PauseDialogue();
		}
	}
	
	// Restart dialogue after given Duration
	GetWorld()->GetTimerManager().SetTimer(SpeakerTimerHandle, this, &ASocialGroup::SetSpeaker, Duration, false);
}


void ASocialGroup::ClearSpeakerTimer()
{
	GetWorld()->GetTimerManager().ClearTimer(SpeakerTimerHandle);
}

void ASocialGroup::SetSpeaker()
{
	if(DialogueIterator >= Dialogue.Num())
	{
		if(bLoopDialogue)
		{
			DialogueIterator = 0;
		} else
		{
			return;
		}
	}
	const FDialogueUtterance CurrentDialogue = AgentsInConversation[DialogueIterator].Utterance;
	AVirtualHuman* VH = AgentsInConversation[DialogueIterator].VH;

	// it might be the case, that an agent, that was part of the conversation, has left the group at some point
	if(!AgentsInGroup.Contains(VH))
	{
		RestartDialogue(2.0f);
		return;
	}
	UVHSocialGroups* SGComp = GetSocialGroupsComponent(VH);
	SGComp->SetIsSpeaker(true);
	if(Dialogue.IsValidIndex(DialogueIterator))
	{
		GetSpeechComponent(VH)->PlayDialogue(CurrentDialogue, 1.5f);
	} else
	{
		VH_WARN("[ASocialGroup::SetSpeaker] No entry found for Dialogues Index: %d\n", DialogueIterator);
	}
}

void ASocialGroup::StartRandomGazingTimer(float FirstDelay)
{

	GetWorld()->GetTimerManager().SetTimer(RandomGazingTimer, this, &ASocialGroup::StartRandomGazing, FirstDelay, false);
}

void ASocialGroup::StartRandomGazing()
{
	for (int i = 0; i < AgentsInGroup.Num(); i++)
	{
		const auto VH = AgentsInGroup[i];
		GetSocialGroupsComponent(VH)->RandomGazing();
	}
}

UVHSocialGroups* ASocialGroup::GetSocialGroupsComponent(AVirtualHuman* VH) const
{
	const auto SG = VH->FindComponentByClass<UVHSocialGroups>();
	if(!SG)
	{
		VH_WARN("[ASocialGroup::GetSocialGroupsComponent] Virtual Human has no valid SocialGroup component\n");
	}
	return SG;
}

UVHGazing* ASocialGroup::GetGazingComponent(AVirtualHuman* VH) const
{
	const auto GC = VH->FindComponentByClass<UVHGazing>();
	if (!GC)
	{
		VH_WARN("[ASocialGroup::GetGazingComponent] Virtual Human has no valid Gazing component\n");
	}
	return GC;
}

UVHSpeech* ASocialGroup::GetSpeechComponent(AVirtualHuman* VH) const
{
	const auto SC = VH->FindComponentByClass<UVHSpeech>();
	if (!SC)
	{
		VH_WARN("[ASocialGroup::GetSpeechComponent] Virtual Human has no valid Speech component\n");
	}
	return SC;
}

// sorts AgentsInGroup array so that the agents opposite to the player is at index 0
void ASocialGroup::SortByAngleToPlayer()
{
	FVector PlayerPos = TrackedPawnLocation;
	FVector PlayerForward = VRPawn->GetActorForwardVector();
	// sort Agents in Group according to angle to the user
	AgentsInGroup.Sort([PlayerForward](AVirtualHuman& vh1, AVirtualHuman& vh2)
		{
			// Check if this agent is in front of the player
			FVector PlayerToVA1 = vh1.GetActorLocation() - PlayerForward;
			float Dot1 = FVector::DotProduct(PlayerForward, PlayerToVA1);

			FVector PlayerToVA2 = vh2.GetActorLocation() - PlayerForward;
			float Dot2 = FVector::DotProduct(PlayerForward, PlayerToVA2);

			return Dot1 > Dot2;
		});
}

TArray<AVirtualHuman*> ASocialGroup::ShuffleArray(TArray<AVirtualHuman*> Agents)
{
	if (Agents.Num() > 0)
	{
		const int LastIndex = Agents.Num() - 1;
		for (int i = 0; i <= LastIndex; ++i)
		{
			const int Index = FMath::RandRange(0, LastIndex);
			if (i != Index)
			{
				Agents.Swap(Index, i);
			}
		}
	}
	return Agents;
}

void ASocialGroup::LeaveGroup(AVirtualHuman* VH)
{
	if(!AgentsInGroup.Contains(VH))
	{
		VH_WARN("[ASocialGroup::LeaveGroup] Agent was not part of the group\n");
		return;
	}
	AgentsInGroup.Remove(VH);
	GroupRadius = GetGroupRadius(AgentsInGroup.Num());
	auto MovementComp = VH->FindComponentByClass<UCharacterMovementComponent>();
	MovementComp->bOrientRotationToMovement = true;
	// stop head rotation
	auto SGComp = GetSocialGroupsComponent(VH);
	SGComp->EndGazing();
	SGComp->SetSocialGroup(nullptr);
	if(bStartDialogue)
	{
		RestartDialogue(2.0f);
	}
}

void ASocialGroup::JoinGroup(AVirtualHuman* VH)
{
	if (AgentsInGroup.Contains(VH))
	{
		VH_WARN("[ASocialGroup::JoinGroup] Agent is already in the group\n")
		return;
	}
	JoiningAgent = VH;
	bAgentIsJoining = true;
	GetSocialGroupsComponent(VH)->VHMoveToLocation(GetActorLocation(),100);
	// in OnTick we check if the agent is close to the group and then integrate him into the group
}


void ASocialGroup::OnSphereBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AVirtualHuman* CloseAgent = Cast<AVirtualHuman>(OtherActor);
	if(!CloseAgent)
	{
		return;
	}
	if(!AgentsInGroup.Contains(CloseAgent))
	{
		CloseAgents.AddUnique(CloseAgent);
		GroupStates.Add(Idle);// starting state
	}
}

void ASocialGroup::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AVirtualHuman* CloseAgent = Cast<AVirtualHuman>(OtherActor);
	if(!CloseAgent)
	{
		return;
	}
	if (AgentsInGroup.Contains(CloseAgent))
	{
		int Index = CloseAgents.Find(CloseAgent);
		CloseAgents.Remove(CloseAgent);
		// +1 because 0 is the group state of the player
		GroupStates.RemoveAt(Index + 1);
	}
}

void ASocialGroup::RestartDialogue(float InitialDelay)
{
	StopDialogue();
	StartDialogue(InitialDelay);
}

TArray<UAnimMontage*> ASocialGroup::LoadAnimationMontages() const
{
	TArray<UAnimMontage*> Montages;
	// load animations for hand gestures
	auto VH = AgentsInGroup[0];
	FString Path = "";
	if (VH->GetBodyType() == EBodyType::CC3)
	{
		Path = "/CharacterPlugin/Animations/Waving/CC3";
	}
	else if (VH->GetBodyType() == EBodyType::MetaHuman)
	{
		Path = "/CharacterPlugin/Animations/Waving/MH";
	}
	
	if (!FPaths::ValidatePath(Path))
	{
		VH_WARN("[ASocialGroup::LoadAnimationMontages] Path: %s is not valid\n", *Path)
	}

	TArray<UObject*> Assets;
	EngineUtils::FindOrLoadAssetsByPath(Path, Assets, EngineUtils::ATL_Regular);

	for (UObject* Asset : Assets)
	{
		UAnimMontage* Anim = Cast<UAnimMontage>(Asset);
		if (Anim)
		{
			Montages.Add(Anim);
		}
		else
		{
			VH_WARN("[ASocialGroup::LoadAnimationMontages] Could not find a valid UAnimInstance at %s\n", *Path)
		}
	}
	return Montages;
}


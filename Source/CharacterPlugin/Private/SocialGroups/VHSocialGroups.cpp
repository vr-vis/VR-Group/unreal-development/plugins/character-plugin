// Fill out your copyright notice in the Description page of Project Settings.


#include "SocialGroups/VHSocialGroups.h"

#include "VirtualHuman.h"
#include "VirtualHumanAIController.h"
#include "Helper/CharacterPluginLogging.h"
#include "SocialGroups/SocialGroup.h"
#include "VHAnimInstance.h"
#include "VHGazing.h"

#include "Engine.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "SoundSource/VAAudiofileSourceComponent.h"

// Sets default values for this component's properties
UVHSocialGroups::UVHSocialGroups()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bAudioCanFinish = false;
	bIsSpeaker = false;
	bGazeToUser = true;
	CurrentYaw = 0.0f;
}

// Called when the game starts
void UVHSocialGroups::BeginPlay()
{
	Super::BeginPlay();
	if(!Init())
	{
		return;
	}
	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	SocialGroupsRandomStream = FRandomStream(TextKeyUtil::HashString(*GetOwner()->GetName()));

	GazeState = GazeAtSpeaker;
	
}
// will be called by the social group once VH has been spawned
bool UVHSocialGroups::InitializeParameters(ASocialGroup* SetSocialGroup)
{
	SocialGroup = SetSocialGroup;
	if(!SocialGroup)
	{
		VH_ERROR("[VHSocialGroups::InitializeParameters] Social Group is not valid\n");
		return false;
	}
	GazingComp = GetOwner()->FindComponentByClass<UVHGazing>();
	if (!GazingComp)
	{
		VH_ERROR("[VHSocialGroups::InitializeParameters]: Virtual Human has no Gazing Component\n")
		return false;
	}
	if(SocialGroup->bStartDialogue)
	{
		AudioComp = GetOwner()->FindComponentByClass<UVAAudiofileSourceComponent>();
		if(!AudioComp)
		{
			VH_ERROR("[VHSocialGroups::InitializeParameters]: Virtual Human has no Audiofile Source Component. If the Social Group should not have Dialogue, please disable the \"Start Dialogue\" flag.\n")
			return false;
		}
		UVHSpeech* SpeechComp = AVirtualHumanPtr->FindComponentByClass<UVHSpeech>();
		if (!SpeechComp)
		{
			VH_ERROR("[VHSocialGroups::InitializeParameters]: Virtual Human has no VHSpeech Component. If the Social Group should not have Dialogue, please disable the \"Start Dialogue\" flag.\n")
			return false;
		}
		UVHFaceAnimation* FaceAnimComp = AVirtualHumanPtr->FindComponentByClass<UVHFaceAnimation>();
		if (!FaceAnimComp)
		{
			VH_ERROR("[VHSocialGroups::InitializeParameters]: Virtual Human has no LiveLink Component. If the Social Group should not have Dialogue, please disable the \"Start Dialogue\" flag.\n")
			return false;
		}
	}

	GetCharacterMovementComponent()->MaxWalkSpeed = 600;
	return true;
}


// Called every frame
void UVHSocialGroups::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if(bIsSpeaker)
	{
		// if audio is playing we CAN finish
		if (AudioComp->GetAudiofileSignalSource()->GetPlayActionEnum() == EPlayAction::Play)
		{
			bAudioCanFinish = true;
		// if audio is not playing anymore, but was playing before
		} else if(bAudioCanFinish)
		{
			OnAudioFinished();
			bAudioCanFinish = false;
		}
	}

	if(SocialGroup)
	{
		// calculate force to standing position in the group
		FVector Movement = GetProximityForce() + GetCircleForce();
		
		float Angle = FMath::Abs(FVector::DotProduct(GetProximityForce(), GetCircleForce()));
		// to increase stability, do not move if forces are very small
		if(Movement.Size() > 2.0f)
		{
			Movement.Normalize();
			VHPawn->AddMovementInput(Movement, 0.3f);
		}

		if (VHPawn->GetMovementComponent()->Velocity.Size() < 1)
		{
			// orient towards group center
			FRotator Rotation = UKismetMathLibrary::FindLookAtRotation(VHPawn->GetActorLocation(), SocialGroup->GetActorLocation());
			// only rotate if rotation is big enough/necessary
			if (FMath::Abs(Rotation.Yaw) > 1)
			{
				float Yaw = Rotation.Yaw;
				CurrentYaw = FMath::FInterpTo(CurrentYaw, Yaw, DeltaTime, 10.0f);
				AVirtualHumanPtr->GetCapsuleComponent()->SetWorldRotation(FRotator(0, CurrentYaw, 0));
			}
		}
	}
}

// force away from other agents
FVector UVHSocialGroups::GetProximityForce()
{
	FVector Force = FVector::ZeroVector;
	
	for(int i = 0; i < SocialGroup->AgentsInGroup.Num(); i++)
	{
		FVector CurrentAgent = VHPawn->GetActorLocation();
		FVector OtherAgent = SocialGroup->AgentsInGroup[i]->GetActorLocation();
		FVector Distance = OtherAgent - CurrentAgent;

		if (Distance.Size() < SocialGroup->ConversationDistance && Distance.Size() > 0)
		{
			float ToDesiredDistance = SocialGroup->ConversationDistance - Distance.Size();
			Distance.Normalize();
			Force += Distance * ToDesiredDistance;
		}
		
	}
	if(SocialGroup->bPlayerIsInGroup)
	{
		FVector CurrentAgent = VHPawn->GetActorLocation();
		FVector OtherAgent = SocialGroup->VRPawn->HeadCameraComponent->GetComponentLocation();
		FVector Distance = OtherAgent - CurrentAgent;

		if (Distance.Size() < SocialGroup->ConversationDistance && Distance.Size()>0)
		{
			float ToDesiredDistance = SocialGroup->ConversationDistance - Distance.Size();
			Distance.Normalize();
			Force += Distance * ToDesiredDistance;
		}
	}

	return -Force;
}

// force towards the group center
FVector UVHSocialGroups::GetCircleForce()
{
	FVector2D GroupCenter = FVector2D(SocialGroup->GetActorLocation().X, SocialGroup->GetActorLocation().Y);
	FVector2D Agent = FVector2D(VHPawn->GetActorLocation().X, VHPawn->GetActorLocation().Y);
	
	FVector2D Direction = GroupCenter - Agent;
	float DesiredLength = Direction.Size() - SocialGroup->GroupRadius;
	Direction.Normalize();
	return FVector(Direction,0) * DesiredLength;
}

// if the audio has finished, start the next dialogue
void UVHSocialGroups::OnAudioFinished()
{
	bIsSpeaker = false;
	
	// do not call next dialogue if the audio is only paused
	if(AudioComp->GetAudiofileSignalSource()->GetPlayActionEnum() != EPlayAction::Pause)
	{
		SocialGroup->NextDialogue();
	}
}

UCharacterMovementComponent* UVHSocialGroups::GetCharacterMovementComponent()
{
	VHPawn = Cast<APawn>(GetOwner());
	if (!VHPawn)
	{
		VH_ERROR("[VHSocialGroups]: Owner can not be cast to Pawn\n");
		return nullptr;
	}
		
	return Cast<UCharacterMovementComponent>(VHPawn->GetMovementComponent());
}

void UVHSocialGroups::VHMoveToLocation(FVector Location, float Speed)
{
	GetCharacterMovementComponent()->MaxWalkSpeed = Speed;
	AIController = Cast<AVirtualHumanAIController>(VHPawn->GetController());
	CurrentRequestID = AIController->MoveToLocation(Location, 2.0f,false);
}

void UVHSocialGroups::SetGazingTimer()
{
	float TimerInterval = 0.0f;

	switch (GazeState)
	{
	case GazeAtSpeaker:
		TimerInterval = SocialGroupsRandomStream.FRandRange(3.0f, 10.0f);
		break;
	case GazeAtRandom:
		TimerInterval = SocialGroupsRandomStream.FRandRange(3.5f, 4.5f);
		break;
	default:
		break;
	}

	GetWorld()->GetTimerManager().SetTimer(GazingTimerHandle, this, &UVHSocialGroups::RandomGazing, TimerInterval, false);
}

void UVHSocialGroups::EndGazing()
{

	GazingComp->StopGazing();
	GetWorld()->GetTimerManager().ClearTimer(GazingTimerHandle);
}

void UVHSocialGroups::RandomGazing()
{
	int RandomIndex = 0;
	AActor* Speaker = nullptr;
	const FVector Offset = FVector(0, 0, AVirtualHumanPtr->BaseEyeHeight);
	switch (GazeState)
	{
	case GazeAtSpeaker:
		Speaker = SocialGroup->GetSpeaker();
		
		// make sure speaker is valid, and we are not the speaker
		if(IsValid(Speaker) && !bIsSpeaker)
		{
			GazingComp->GazeToActor(Speaker,Offset);
		} else
		{
			// if we are speaking, or if there is currently no speaker, gaze at random agent
			RandomIndex = SocialGroupsRandomStream.RandRange(0, SocialGroup->AgentsInGroup.Num() - 1);

			while(SocialGroup->AgentsInGroup[RandomIndex] == AVirtualHumanPtr)
			{
				RandomIndex = SocialGroupsRandomStream.RandRange(0, SocialGroup->AgentsInGroup.Num() - 1);
			}
			GazingComp->GazeToActor(SocialGroup->AgentsInGroup[RandomIndex],Offset);
		}
		GazeState = GazeAtRandom;
		break;
	case GazeAtRandom:

		if(SocialGroup->bPlayerIsInGroup)
		{
			RandomIndex = SocialGroupsRandomStream.RandRange(0, SocialGroup->AgentsInGroup.Num());
		} else
		{
			RandomIndex = SocialGroupsRandomStream.RandRange(0, SocialGroup->AgentsInGroup.Num()-1);
		}

		if(RandomIndex == SocialGroup->AgentsInGroup.Num())
		{
			GazingComp->GazeToUser();
		} else
		{
			while (SocialGroup->AgentsInGroup[RandomIndex] == AVirtualHumanPtr)
			{
				RandomIndex = SocialGroupsRandomStream.RandRange(0, SocialGroup->AgentsInGroup.Num() - 1);
			}
			GazingComp->GazeToActor(SocialGroup->AgentsInGroup[RandomIndex], Offset);
		}
		GazeState = GazeAtSpeaker;
		break;
	default:
		GazingComp->StopGazing();
		break;
	}

	// loop back to set the gazing timer
	SetGazingTimer();
}

void UVHSocialGroups::LeaveGroup()
{
	if(!SocialGroup)
	{
		VH_WARN("[VHSocialGroups::LeaveGroup] SocialGroup is not valid when calling LeaveGroup()\n");
		return;
	}
	SocialGroup->LeaveGroup(AVirtualHumanPtr);
}

void UVHSocialGroups::JoinGroup(ASocialGroup* SG)
{
	if(!SG)
	{
		VH_ERROR("[VHSocialGroups::JoinGroup] SocialGroup is not valid when calling JoinGroup\n");
		return;
	}
	SG->JoinGroup(AVirtualHumanPtr);
}

void UVHSocialGroups::StartGazeToUserTimer()
{
	float TimerInterval = 0.0f;
	if (bGazeToUser)
	{
		TimerInterval = 3.0f;
	}
	else
	{
		TimerInterval = SocialGroupsRandomStream.FRandRange(3.0f, 5.0f);
	}
	GetWorld()->GetTimerManager().SetTimer(GazeToUserTimer, this, &UVHSocialGroups::GazeToUser, TimerInterval, false);
}
void UVHSocialGroups::ClearGazeToUserTimer()
{
	bGazeToUser = false;
	GetWorld()->GetTimerManager().ClearTimer(GazeToUserTimer);
}

void UVHSocialGroups::GazeToUser()
{
	bGazeToUser = !bGazeToUser;
	if (!bGazeToUser)
	{
		RandomGazing();
	}
	StartGazeToUserTimer();
}

bool UVHSocialGroups::Init()
{
	AVirtualHumanPtr = Cast<AVirtualHuman>(GetOwner());
	if(!AVirtualHumanPtr)
	{
		VH_ERROR("[VHSocialGroups::Init] Cannot cast Owner to Virtual Human\n")
		return false;
	}
	// because this pawn is spawned by the social group
	// we need to make sure that the correct AI Controller is spawned and possessed as well
	// set the AI Controller we want to have
	VHPawn = Cast<APawn>(GetOwner());
	if (!VHPawn)
	{
		VH_ERROR("[VHSocialGroups::Init] Cannot cast Owner to Pawn\n")
		return false;
	}
	VHPawn->AIControllerClass = AVirtualHumanAIController::StaticClass();
	// spawn and possesses the AI Controller we just set
	VHPawn->SpawnDefaultController();
	AIController = Cast<AVirtualHumanAIController>(VHPawn->GetController());
	if (!AIController)
	{
		VH_ERROR("[VHSocialGroups::Init] AI Controller is not valid\n")
		return false;
	}
	AnimationInstance = Cast<UVHAnimInstance>(AVirtualHumanPtr->GetBodyAnimInstance());
	if (!AnimationInstance)
	{
		VH_ERROR("[VHSocialGroups::Init] Animation Instance is not valid\n")
		return false;
	}

	return true;
}
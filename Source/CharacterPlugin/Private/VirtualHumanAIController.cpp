// Fill out your copyright notice in the Description page of Project Settings.


#include "VirtualHumanAIController.h"
#include "VHMovement.h"
#include "VirtualHuman.h"
#include "Crowds/VelocityPostProcessorComponent.h"
#include "Crowds/VHsManager.h"
#include "Helper/CharacterPluginLogging.h"
#include "Navigation/PathFollowingComponent.h"

#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AVirtualHumanAIController::BeginPlay()
{
	Super::BeginPlay();
	GetPathFollowingComponent()->PostProcessMove.BindUObject(this, &AVirtualHumanAIController::PostProcessMovement);
}

void AVirtualHumanAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	LastDeltaTime = DeltaSeconds;
}

void AVirtualHumanAIController::PostProcessMovement(UPathFollowingComponent* Comp, FVector& Velocity)
{
	TArray<AVHsManager*> MyManagers = GetAssociatedVHsManagers();
	if (MyManagers.Num() > 0)
	{
		for (AVHsManager *manager : MyManagers)
		{
			if (manager != nullptr && manager->DebugDraw)
			{
				auto points = Comp->GetPath()->GetPathPoints();
				if (points.Num() > 0)
				{
					auto World = GetWorld();
					auto NextWaypoint = points[Comp->GetNextPathIndex()].Location;

					DrawDebugLine(World, GetPawn()->GetActorLocation(), NextWaypoint, FColor::Orange, false,
						LastDeltaTime, 0.f, 5.f);

					DrawDebugPoint(World, NextWaypoint, 15, FColor::Orange, false, LastDeltaTime);

					for (int i = Comp->GetNextPathIndex(); i < points.Num() - 1; i++)
					{
						DrawDebugLine(World, points[i], points[i + 1], FColor::Orange, false,
							LastDeltaTime, 0.f, 5.f);

						DrawDebugPoint(World, points[i + 1], 15.f, FColor::Orange, false,
							LastDeltaTime);
					}
				}
			}
		}
	}
	auto VelocityPostProcessor = Cast<UVelocityPostProcessorComponent>(GetPawn()->GetComponentByClass(UVelocityPostProcessorComponent::StaticClass()));
	if (!VelocityPostProcessor)
	{
		return;
	}
	VelocityPostProcessor->PostProcessVelocity(LastDeltaTime, Velocity);

	
}

void AVirtualHumanAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	UVHMovement* Movement = Cast<UVHMovement>(GetPawn()->FindComponentByClass<UVHMovement>());

	if(Movement)
	{
		if(Movement->CurrentRequestID != RequestID)
		{
			return;
		}
		if (Movement->ShouldChildMove() && Movement->IsChild() && !Movement->IsLineMovement()) {
			Movement->ChildMoveToWaypoint();
		}
		else if(!Movement->IsChild() || Movement->IsLineMovement())
		{
			Movement->MoveToWaypoint();
		}

	}
}

void AVirtualHumanAIController::DestroyAgent()
{
	AVirtualHuman* Agent = Cast<AVirtualHuman>(GetPawn());

	TArray<AVHsManager*> MyManagers = GetAssociatedVHsManagers();
	for (AVHsManager* m : MyManagers)
	{
		m->Remove(Agent);
	}
	Agent->Destroy();
}


TArray<AVHsManager*> AVirtualHumanAIController::GetAssociatedVHsManagers()
{

	TArray<AVHsManager*> MyManagers;
	MyManagers.Empty();

	// Get all managers in the scene
	TArray<AActor*> AllManagers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVHsManager::StaticClass(), AllManagers);
	
	// Try to find out if we belong to a manger
	if (AllManagers.Num() > 0)
	{
		AVirtualHuman* Agent = Cast<AVirtualHuman>(GetPawn());
		//VH_DEBUG("Name of current agent is %s", *OwnerAgent->GetName());
		for (AActor *a : AllManagers)
		{
			AVHsManager* VHsManager = Cast<AVHsManager>(a);
			if (VHsManager->HasAgent(Agent))
			{
				MyManagers.Add(VHsManager);
			}
		}
	}
	return MyManagers;
}

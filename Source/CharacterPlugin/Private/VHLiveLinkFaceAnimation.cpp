// Fill out your copyright notice in the Description page of Project Settings.


#include "VHLiveLinkFaceAnimation.h"


#include "AssetRegistry/AssetRegistryModule.h"
#include "Animation/PoseAsset.h"
#include "HAL/PlatformFileManager.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"
#include "Engine.h"

#include "Helper/CharacterPluginLogging.h"

bool UVHLiveLinkAnimation::InternalLoadFile(FString LocalFileName)
{
	FString Filename = FPaths::Combine(FPaths::ProjectContentDir(),
		LocalFileName);
	IPlatformFile& File = FPlatformFileManager::Get().GetPlatformFile();

	if (!File.FileExists(*Filename))
	{
		VH_ERROR("Cannot find LiveLinkFace tracking file %s in your content dir!\n"
			, *LocalFileName);
		return false;
	}

	FString FileContent;
	FFileHelper::LoadFileToString(FileContent, *Filename);

	TArray<FString> Lines;
	FileContent.ParseIntoArrayLines(Lines, true);

	if (Mapping == nullptr)
	{
		VH_ERROR("[UVHLiveLinkAnimation::InternalLoadFile] Mapping is not specified!\n");
		return false;
	}

	TArray<FName> BlendshapeNames = Mapping->GetCurveFNames();
	TArray<FString> PoseNames;
	TArray<TArray<float>> BlendshapeValues;
	// BlendshapeValues holds [PoseId][BlendshapeId]
	// where poses are the poses by LiveLink like eyeBlinkLeft and blendshapes are those of the mesh
	for (FName PoseName : Mapping->GetPoseFNames())
	{
		PoseNames.Add(PoseName.ToString());
		BlendshapeValues.Add(Mapping->GetCurveValues(Mapping->GetPoseIndexByName(PoseName)));
	}


	bool bHeaderRead = false;
	int TimecodeIndex = 0;
	float StartTimeCode = -1.0f;
	int HeadRotationIndex = 0;
	int LeftEyeRotationIndex = 0;
	int RightEyeRotationIndex = 0;

	AnimationData.BlendshapeNames.Empty();
	AnimationData.TimeSteps.Empty();

	for (FName Name : BlendshapeNames)
	{
		AnimationData.BlendshapeNames.Add(Name.ToString());
	}

	struct PoseIndex
	{
		int IndexInCSVFile;
		int IndexInValueMapping;
	};
	TArray<PoseIndex> PoseMappingIndices;

	TArray<FString> Entries;
	for (FString Line : Lines)
	{
		if (!bHeaderRead)
		{
			// read in first line (the header), so we know where to look for the data
			Line.ParseIntoArray(Entries, TEXT(","));
			for (int Index = 0; Index < Entries.Num(); ++Index)
			{
				FString HeaderEntry = Entries[Index];
				//HeaderEntry.ReplaceInline(TEXT(" "), TEXT(""));
				if (HeaderEntry == TEXT("Timecode"))
				{
					TimecodeIndex = Index;
				}
				else if (HeaderEntry == TEXT("BlendShapeCount"))
				{
					//ignore for now
				}
				else
				{
					//we are reading a blendshape
					if (!bUseEyeRotations && HeaderEntry.Contains("EyeLook"))
					{
						VH_LOG("[UVHLiveLinkAnimation::InternalLoadFile] Ignore %s since bUseEyeRotations==false", *HeaderEntry);
					}
					else if (PoseNames.Contains(*HeaderEntry))
					{
						//we have a mapping for it
						PoseMappingIndices.Add({ Index, PoseNames.Find(*HeaderEntry) });
					}
					else if (HeaderEntry.Contains("Head") || HeaderEntry.Contains("LeftEye") || HeaderEntry.Contains("RightEye"))
					{
						//VH_DEBUG("Found a header which is tracked for rotation: %s\n", *HeaderEntry);
						//check whether it is one of our needed rotations
						if (HeaderEntry.Contains("HeadYaw"))
						{
							HeadRotationIndex = Index;
						}
						else if (HeaderEntry.Contains("LeftEyeYaw"))
						{
							LeftEyeRotationIndex = Index;
						}
						else if (HeaderEntry.Contains("RightEyeYaw"))
						{
							RightEyeRotationIndex = Index;
						}
					}
					else if (HeaderEntry.Contains("MouthClose"))
					{
						// ignore it, as of now the common metahuman ARKit mapping (Content/MetaHumans/Common/Common/MoCap/mh_arkit_mapping_pose)
						// comes without ampaaing for MouthClose, so apparently it is not that needed or not easy to map
						// more info on MouthClose at: https://developer.apple.com/documentation/arkit/arfaceanchor/blendshapelocation/2928266-mouthclose
						VH_LOG("[UVHLiveLinkAnimation::InternalLoadFile] MouthClose was not mapped in the official metahuman ARKit mapping so far, maybe it is now?")
					}
					else
					{
						VH_WARN("[UVHLiveLinkAnimation::InternalLoadFile] Unmapped tracking data pose: %s\n", *HeaderEntry);
					}
				}
			}
			bHeaderRead = true;
		}
		else
		{
			FFrameData FrameData;
			Line.ParseIntoArray(Entries, TEXT(","));

			float TimeCode = ConvertTimecodeToSeconds(Entries[TimecodeIndex]);
			if (StartTimeCode == -1.0f)
			{
				StartTimeCode = TimeCode;
			}

			FrameData.Timestamp = TimeCode - StartTimeCode;

			// factor etc described here:
			// https://docs.unrealengine.com/en-US/AnimatingObjects/SkeletalMeshAnimation/FacialRecordingiPhone/#headrotationblueprint-animgraph
			FrameData.HeadRotation = 180.0f / PI * FRotator(FCString::Atof(*Entries[HeadRotationIndex + 0]),
				FCString::Atof(*Entries[HeadRotationIndex + 2]),
				FCString::Atof(*Entries[HeadRotationIndex + 1]));

			FrameData.LeftEyeRotation = 180.0f / PI * FRotator(FCString::Atof(*Entries[LeftEyeRotationIndex + 0]),
				FCString::Atof(*Entries[LeftEyeRotationIndex + 2]),
				-FCString::Atof(*Entries[LeftEyeRotationIndex + 1]));

			FrameData.RightEyeRotation = 180.0f / PI * FRotator(FCString::Atof(*Entries[RightEyeRotationIndex + 0]),
				FCString::Atof(*Entries[RightEyeRotationIndex + 2]),
				-FCString::Atof(*Entries[RightEyeRotationIndex + 1]));


			for (int i = 0; i < BlendshapeNames.Num(); ++i) {
				float BlendshapeActivation = 0.0f;
				for (int j = 0; j < PoseMappingIndices.Num(); ++j) {
					PoseIndex IndexMapping = PoseMappingIndices[j];
					BlendshapeActivation += FCString::Atof(*Entries[IndexMapping.IndexInCSVFile]) * BlendshapeValues[IndexMapping.IndexInValueMapping][i];
				}
				FrameData.BlendshapeActivations.Add(BlendshapeActivation);
			}
			AnimationData.TimeSteps.Add(FrameData);
		}
	}
	return true;
}

float UVHLiveLinkAnimation::ConvertTimecodeToSeconds(FString Timecode)
{
	//it's in the format hh:mm:ss:1/60ofasecondasfloat
	TArray<FString> Entries;
	Timecode.ParseIntoArray(Entries, TEXT(":"));
	int hours = FCString::Atoi(*Entries[0]);
	int minutes = FCString::Atoi(*Entries[1]);
	int seconds = FCString::Atoi(*Entries[2]);
	float sextieth = FCString::Atof(*Entries[3]);

	return 3600 * hours + 60 * minutes + seconds + sextieth / 60;
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "VHListenerBackchannels.h"

#include "VirtualHuman.h"
#include "VHAnimInstance.h"
#include "Helper/CharacterPluginLogging.h"


// Sets default values for this component's properties
UVHListenerBackchannels::UVHListenerBackchannels()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UVHListenerBackchannels::BeginPlay()
{
	Super::BeginPlay();
	AVirtualHuman* Owner = Cast<AVirtualHuman>(GetOwner());
	if (Owner != nullptr) {
		AnimInstance = Owner->GetBodyAnimInstance();
	}
	
	if (!AnimInstance) {
		VH_ERROR("[UVHListenerBackchannels::BeginPlay] No AnimInstance at BeginPlay\n");
	}
	RandomStream = FRandomStream(TextKeyUtil::HashString(*GetOwner()->GetName()));
}

// Called every frame
void UVHListenerBackchannels::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(bNodding)
	{
		NoddingTime += DeltaTime;
		if(NoddingTime>ActualNodLength)
		{
			bNodding = false;
		}
		else
		{
			//formula from work by Klara Tyroller extended by some randomness and possible prolongment
			float HeadPitch = - cos((NoddingTime * 2 * PI) / ActualNodDuration) * ActualNodAmplitude + ActualNodAmplitude;
			//decrease over time, e.g., if more than one nod is played
			HeadPitch *= (ActualNodLength - NoddingTime) / ActualNodLength; 
			AnimInstance->SkelControl_Head.Roll = HeadPitch; //is actually the pitch
		}

	}
}

void UVHListenerBackchannels::StartNod()
{
	if(bNodding)
	{
		return;
	}

	ActualNodAmplitude = RandomStream.FRandRange(0.5f * NodAmplitude, 1.5f * NodAmplitude);
	ActualNodDuration = RandomStream.FRandRange(0.5f * NodDuration, 1.5f * NodDuration);
	if(ActualNodDuration < NodDuration)
	{
		//play one to three nods
		ActualNodLength = RandomStream.FRandRange(1.5f, 3.0f) * ActualNodDuration;
	}
	else
	{
		ActualNodLength = ActualNodDuration;
	}

	bNodding = true;
	NoddingTime = 0.0f;
}


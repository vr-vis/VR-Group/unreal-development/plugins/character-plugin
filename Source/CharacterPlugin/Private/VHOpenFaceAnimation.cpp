// Fill out your copyright notice in the Description page of Project Settings.


#include "VHOpenFaceAnimation.h"

#include "HAL/PlatformFileManager.h"
#include "Misc/Paths.h"
#include "Misc/FileHelper.h"
#include "Engine.h"

#include "Helper/CharacterPluginLogging.h"

bool UVHOpenFaceAnimation::InternalLoadFile(FString OpenFaceFileName)
{
	if (Mapping == nullptr) {
		VH_ERROR("[UVHOpenFaceAnimation::InternalLoadFile] Mapping is not specified!\n");
		return false;
	}

	TArray<FName> BlendshapeNames = Mapping->GetCurveFNames();
	TArray<TArray<float>> BlendshapeValues; // Values[Pose][BlendshapeName]
	TArray<FString> PoseNames;
	for (FName PoseName : Mapping->GetPoseFNames()) {
		BlendshapeValues.Add(Mapping->GetCurveValues(Mapping->GetPoseIndexByName(PoseName)));
		PoseNames.Add(PoseName.ToString());
	}

	FString Filename = FPaths::Combine(FPaths::ProjectContentDir(),
		OpenFaceFileName);
	IPlatformFile& File = FPlatformFileManager::Get().GetPlatformFile();

	if (!File.FileExists(*Filename))
	{
		VH_ERROR("[UVHOpenFaceAnimation::InternalLoadFile] Cannot find OpenFace tracking file %s in your content dir!\n", *OpenFaceFileName);
		return false;
	}


	FString FileContent;
	FFileHelper::LoadFileToString(FileContent, *Filename);

	TArray<FString> Lines;
	FileContent.ParseIntoArrayLines(Lines, true);

	bool bHeaderRead = false;
	int TimestampIndex = 0;
	int HeadRotationIndex = 0;

	AnimationData.BlendshapeNames.Empty();
	AnimationData.TimeSteps.Empty();
	for (FName Name : BlendshapeNames)
	{
		AnimationData.BlendshapeNames.Add(Name.ToString());
	}

	struct FIndexBlendshapeMapping
	{
		int Index;
	};

	TArray<FIndexBlendshapeMapping> AUIndices;
	AUIndices.Reserve(17);

	TArray<FString> Entries;
	for (FString Line : Lines)
	{
		if (!bHeaderRead)
		{
			// read in first line (the header), so we know where to look for the data
			Line.ParseIntoArray(Entries, TEXT(","));
			for (int Index = 0; Index < Entries.Num(); ++Index)
			{
				FString HeaderEntry = Entries[Index];
				HeaderEntry.ReplaceInline(TEXT(" "), TEXT(""));
				if (HeaderEntry == TEXT("timestamp"))
				{
					TimestampIndex = Index;
				}
				else if (HeaderEntry == TEXT("pose_Rx"))
				{
					HeadRotationIndex = Index;
				}
				else if (HeaderEntry.StartsWith(TEXT("AU")) && HeaderEntry.EndsWith(
					TEXT("_r")))
				{

					AUIndices.Add({ Index });
					if (!PoseNames.Contains(HeaderEntry)) {
						VH_ERROR("[UVHOpenFaceAnimation::InternalLoadFile] Pose Asset does not have the required poses!\n", *OpenFaceFileName);
						return false;
					}
				}
			}
			if (TimestampIndex == 0 || HeadRotationIndex == 0 || AUIndices.Num() == 0
				)
			{
				VH_ERROR("UVHOpenFaceAnimation Cannot parse %s, since not all needed elements were found in the header! The header first line in the file is %s\n", *OpenFaceFileName, *Line);
				return false;
			}
			bHeaderRead = true;
		}
		else
		{
			FFrameData Date;
			Line.ParseIntoArray(Entries, TEXT(","));

			if (Entries.Num() < TimestampIndex || Entries.Num() < HeadRotationIndex +
				2)
			{
				VH_ERROR("UVHOpenFaceAnimation Cannot parse %s, since the are not sufficient data elements in line %s\n", *OpenFaceFileName, *Line);
				return false;
			}

			Date.Timestamp = FCString::Atof(*Entries[TimestampIndex]);

			//  OpenFace rotation is in radians around X,Y,Z axes with the convention R = Rx * Ry * Rz, left-handed positive sign
			// right: x, up: y, forward: z ==> Yaw: Y, pitch: X, roll: Z
			float XRot = FCString::Atof(*Entries[HeadRotationIndex + 0]) / PI *
				360.0f;
			float YRot = FCString::Atof(*Entries[HeadRotationIndex + 1]) / PI *
				360.0f;
			float ZRot = FCString::Atof(*Entries[HeadRotationIndex + 2]) / PI *
				360.0f;
			//it seems that rotations are too stronger, so we scale them done
			float RotationScaling = 0.5f;
			Date.HeadRotation = (FRotator(-1 * RotationScaling * ZRot, 0.0f, 0.0f).Quaternion()
				* FRotator(0.0f, YRot, 0.0f).Quaternion()
				* FRotator(0.0f, 0.0f, RotationScaling * XRot).Quaternion()).Rotator();
			
			for (int i = 0; i < BlendshapeNames.Num(); i++) {
				float Value = 0.f;
				for (int j = 0; j < AUIndices.Num(); j++) {
					Value += FCString::Atof(*Entries[AUIndices[j].Index]) * AUActivationScaling * BlendshapeValues[j][i];
				}
				Date.BlendshapeActivations.Add(Value);
			}

			AnimationData.TimeSteps.Add(Date);
		}
	}
	return true;

}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Crowds/VelocityPostProcessorComponent.h"

#include "Helper/CharacterPluginLogging.h"

UVelocityPostProcessorComponent::UVelocityPostProcessorComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

/**
*	We integrate a 'Movement Post Processor' into the existing UE's navigation system.
*	To this end, the 'Movement Post Processor' receives the desired velocity (AS VECTOR) 
*	to the next waypoint from the 'Path Following Component' and overrides the velocity
*	before it is sent to the actual 'VHMovement Component'.
**/
void UVelocityPostProcessorComponent::PostProcessVelocity(float LastDeltaTime, FVector& Velocity)
{
	Velocity = FVector::ZeroVector;
	VH_WARN("Velocity Post Processor not implemented as this is the base class.")
}

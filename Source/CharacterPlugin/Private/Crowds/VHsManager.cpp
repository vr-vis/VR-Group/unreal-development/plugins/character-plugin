// Fill out your copyright notice in the Description page of Project Settings.


#include "Crowds/VHsManager.h"
#include "Kismet/GameplayStatics.h"
#include "Helper/CharacterPluginLogging.h"
#include "Engine/World.h"

AVHsManager::AVHsManager()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AVHsManager::BeginPlay()
{
	Super::BeginPlay();

	
	/* Do NOT store all agents for the VHsManager as there might be several ones
		TArray<AActor*> Actors;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVirtualHuman::StaticClass(), Actors);

		for (AActor* Actor : Actors)
		{
			AllVHs.Push(Cast<AVirtualHuman>(Actor));
			SetCrowdAlgorithmFor(AllVHs.Last());
		}
	*/
}

AVirtualHuman* AVHsManager::SpawnVH(FVector Position, bool IgnoreCollisions)
{
	AVirtualHuman* Agent;

	if (IgnoreCollisions)
	{
		FActorSpawnParameters ActorSpawnParameters;
		ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Agent = GetWorld()->SpawnActor<AVirtualHuman>(AgentClass, Position, FRotator::ZeroRotator, ActorSpawnParameters);
	}
	else
	{
		Agent = GetWorld()->SpawnActor<AVirtualHuman>(AgentClass, Position, FRotator::ZeroRotator);
	}

	// Won't spawn if not enough space
	if (Agent != nullptr)
	{
		Agent->SpawnDefaultController();
		SetCrowdAlgorithmFor(Agent);
		Add(Agent);
	}

	return Agent;
}

void AVHsManager::ClearAllVHs()
{
	for (AVirtualHuman* Agent : AllVHs)
	{
		if (Agent != nullptr)
		{
			Agent->Destroy();
		}
	}
	AllVHs.Empty();
}

void AVHsManager::Add(AVirtualHuman* Agent)
{
	if (Agent == nullptr)
	{
		VH_ERROR("[AVHsManager::Add] Attempted to register nullptr Agent")
		return;
	}
	AllVHs.Add(Agent);
}

void AVHsManager::Remove(AVirtualHuman* Agent)
{
	AllVHs.Remove(Agent);
}

bool AVHsManager::HasAgent(AVirtualHuman * Agent)
{
	if (Agent == nullptr)
	{
		VH_ERROR("[AVHsManager::HasAgent] Attempted to find nullptr Agent")
		return false;
	}
	return (AllVHs.Find(Agent) != INDEX_NONE);		
}

void AVHsManager::SetCrowdAlgorithm(TSubclassOf<UVelocityPostProcessorComponent> NewCrowdAlgorithm)
{
	CrowdAlgorithm = NewCrowdAlgorithm;
	for(AVirtualHuman* VH : AllVHs)
	{
		SetCrowdAlgorithmFor(VH);
	}
}

TArray<AVirtualHuman*> AVHsManager::GetNeighbours(AVirtualHuman* VH, const float MaxDistance)
{
	FVector VHLocation = VH->GetActorLocation();
	float MaxDistanceSq = MaxDistance*MaxDistance;
	return AllVHs.FilterByPredicate(
			[VH, VHLocation, MaxDistanceSq](AActor* Agent)
	{
		return (VH != Agent) && (Agent != nullptr) && (FVector::DistSquared(
			VHLocation, Agent->GetActorLocation()) < MaxDistanceSq);
	});

}

TArray<AVirtualHuman*> AVHsManager::GetAllVHs() const
{
	return AllVHs;
}

void AVHsManager::SetCrowdAlgorithmFor(AVirtualHuman* VH)
{
	if(CrowdAlgorithm == nullptr)
	{
		return;
	}

	TArray<UVelocityPostProcessorComponent*> OldComponents;
	VH->GetComponents(OldComponents);
	for(UVelocityPostProcessorComponent* OldComponent : OldComponents){
		OldComponent->DestroyComponent();
	}

	UVelocityPostProcessorComponent* SpawnedComponent = NewObject<UVelocityPostProcessorComponent>(VH, CrowdAlgorithm);
	SpawnedComponent->RegisterComponent();
}

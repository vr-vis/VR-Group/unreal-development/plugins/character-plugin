// Fill out your copyright notice in the Description page of Project Settings.


#include "Crowds/Spawner/CircleSpawner.h"
#include "Crowds/VHsManager.h"
#include "VHMovement.h"
#include "Helper/CharacterPluginLogging.h"
#include "Kismet/GameplayStatics.h"


void ACircleSpawner::BeginPlay() {

}


void ACircleSpawner::Spawn() {
	Super::Spawn();

	// get our associated VHsManager
	if (!VHsManager)
	{
		VH_ERROR("VHsManager of %s is nullptr", *GetName());
		return;
	}
	AVHsManager* Manager = Cast<AVHsManager>(VHsManager);

	for (int i = 0; i < GroupSize; i++) {
		//VH_DEBUG("Actor location is: %s", *GetActorLocation().ToString())
		FVector Position = GetActorLocation() + FVector(FVector2D(0, SpawnRadius).GetRotated(i * 360 / GroupSize), 0);
		//VH_DEBUG("Spawn position is is: %s", *Position.ToString())

		AVirtualHuman* VH = Manager->SpawnVH(Position, true);
		if(!VH)
		{
			VH_ERROR("[ACircleSpawner::Spawn()] VH is nullptr");
			continue;
		}

		//VH_DEBUG("Create VH with name %s", *VH->GetName())
		//TArray<UActorComponent*> Components;
		//VH->GetComponents(Components);
		//VH_DEBUG("VH has %d components", Components.Num())
		//for (UActorComponent* c : Components)
		//{
		//	VH_DEBUG("VH has %s component", *c->GetName())
		//}

		UVHMovement* VHMovement = Cast<UVHMovement>(VH->GetComponentByClass(UVHMovement::StaticClass()));
		VHMovement->WaypointGroup = WaypointGroup;
		VHMovement->StoreAllWaypoints();
		VHMovement->MoveToWaypoint();
	}
}

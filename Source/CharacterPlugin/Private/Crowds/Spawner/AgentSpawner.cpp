// Fill out your copyright notice in the Description page of Project Settings.


#include "Crowds/Spawner/AgentSpawner.h"
#include "Engine/World.h"


void AAgentSpawner::BeginPlay()
{
	Super::BeginPlay();
}

void AAgentSpawner::Spawn()
{
	GetWorld()->GetTimerManager().ClearTimer(SpawnTimerHandle);

	if (RespawnRate > 0) {
		GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &AAgentSpawner::Spawn, RespawnRate, false);
	}
}

void AAgentSpawner::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	// Alternatively you can clear ALL timers that belong to this (Actor) instance.
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}
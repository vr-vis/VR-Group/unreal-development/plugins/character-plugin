// Fill out your copyright notice in the Description page of Project Settings.
// FIXME: Add copyright notice from original implementation

#include "Crowds/Predictive.h"

#include "VirtualHuman.h"
#include "VHMovement.h"
#include "Crowds/VHsManager.h"
#include "Helper/CharacterPluginLogging.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

/**
*	Implementation based on the following paper:
*   Karamouzas et al.: 
*   A Predictive Collision Avoidance Model for Pedestrian Simulation. 2009, 
*   10.1007/978-3-642-10347-6_4
*
*	Reference Implementation used:
*	Menge Framework
*   https://github.com/MengeCrowdSim/Menge/tree/master/src/Plugins/AgtKaramouzas
*
**/
void UPredictive::PostProcessVelocity(float LastDeltaTime, FVector& Velocity)
{
	/************************************************************************/
	/* GET THE REQUIRED AGENT & MANAGER										*/
	/************************************************************************/

	AVirtualHuman* Agent = Cast<AVirtualHuman>(GetOwner());
	//VH_DEBUG("%s delta time: %f", *Agent->GetName(), LastDeltaTime)
	//VH_DEBUG("%s start velocity im cm/s, %f, %f, %f", *Agent->GetName(), Velocity[0], Velocity[1], Velocity[2])
	
	if (Agent == nullptr)
	{
		Velocity = FVector::ZeroVector;
		return;
	}

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AVHsManager::StaticClass(), Actors);
	/*
	if(Actors.Num()!=1){
		return;
	}
	AVHsManager* Manager = Cast<AVHsManager>(Actors[0]);
	if (Manager == nullptr)
	{
		Velocity = FVector::ZeroVector;
		return;
	}
	*/
	TArray<AVirtualHuman*> Neighbours;
	for (AActor *a : Actors)
	{
		AVHsManager* Manager = Cast<AVHsManager>(a);
		if (Manager != nullptr)
		{
			TArray<AVirtualHuman*> tmp = Manager->GetAllVHs();
			for (int i = 0; i < tmp.Num(); ++i)
			{
				Neighbours.Add(tmp[i]);
			}
		}
	}
	if (Neighbours.Num() == 0)
	{
		// there are no neighbors, so we do not change the velocity
		return;
	}


	/************************************************************************/
	/* SETUP START VALUES													*/
	/************************************************************************/

	Velocity = Velocity.GetClampedToMaxSize2D(WALK_SPEED);
	//VH_DEBUG("%s clamped velocity, %f, %f, %f", *Agent->GetName(), Velocity[0], Velocity[1], Velocity[2])
	// speed adaption to m/s instead of UE cm/s
	Velocity = Velocity / 100.0f;

	const float PREDICTIVE_EPSILON = 0.01f; // this eps from Ioannis
	const float FOV = COS_FOV_ANGLE;

	FVector CurrentLocation = Agent->GetActorLocation() / 100.0f;
	FVector CurrentVelocity = Agent->GetVelocity() / 100.0f;
	//VH_DEBUG("%s current velocity im m/s: %f, %f, %f", *Agent->GetName(), CurrentVelocity[0], CurrentVelocity[1], CurrentVelocity[2])
	
	// goal force attracting the agents to it's goal
	FVector2D force((Velocity - CurrentVelocity) / REACTION_TIME);
	
	// safe distance agent prefers to keep from buildings etc.
	const float SAFE_DIST = WALL_DISTANCE + RADIUS;
	
	FVector2D desiredVel = FVector2D(CurrentVelocity) + force * LastDeltaTime;
	float desSpeed = (desiredVel).Size();
		
	/************************************************************************/
	/* WEIGHT ALL NEIGHBORS													*/
	/************************************************************************/
	force.Set(0.f, 0.f);
	bool colliding = false;
	int collidingCount = COLLIDING_COUNT;

	TArray<TPair<float, const AActor*>> collidingSet;
	for (auto Neighbour : Neighbours)
	{
		float circRadius = PERSONAL_SPACE + RADIUS;
		FVector2D relVel = desiredVel - FVector2D(Neighbour->GetVelocity() / 100.0f);
		FVector2D relPos = FVector2D((Neighbour->GetActorLocation() / 100.0f) - CurrentLocation);

		if ((relPos).SizeSquared() < circRadius * circRadius)
		{
			/// collision!
			if (!colliding)
			{
				colliding = true;
				collidingSet.Empty();
			}
			
			collidingSet.Add(TPair<float, const AActor*>(.0f, Neighbour));
			
			if (collidingSet.Num() > collidingCount)
			{
				++collidingCount;
			}
			continue;
		}

		FVector2D relDir = (relPos).GetSafeNormal();
		float tc = RayCircleTTC(relVel, relPos, circRadius);
		if (tc < ANTICIPATION_TIME && !colliding)
		{
			collidingSet.Add(TPair<float, const AActor*>(tc, Neighbour));
		}
	}

	collidingSet.Sort([](const TPair<float, const AActor*>& A, const TPair<float, const AActor*>& B)
	{
		return A.Key < B.Key;
	});

	
	int count = 0;
	for (auto CollidingNeighbourPair : collidingSet)
	{
		const AActor* CollidingNeighbour = CollidingNeighbourPair.Value;
		float tc = CollidingNeighbourPair.Key;
		// future positions
		FVector2D myPos = FVector2D(CurrentLocation) + desiredVel * tc;
		FVector2D hisPos = FVector2D(
			(CollidingNeighbour->GetActorLocation() / 100.0) + (CollidingNeighbour->GetVelocity() * tc / 100.0));
		FVector2D forceDir = myPos - hisPos;
		float fDist = (forceDir).Size();
		// ensure to avoid NANs :)
		if (fDist != 0)
		{
			forceDir /= fDist;
		}
		float collisionDist = fDist - RADIUS - RADIUS;
		float D = FMath::Max(desSpeed * tc + (collisionDist < 0 ? 0 : collisionDist), PREDICTIVE_EPSILON);

		// determine magnitude
		float mag = 0.f;
		if (D < D_MIN)
		{
			mag = AGENT_FORCE * D_MIN / D;
		}
		else if (D < D_MID)
		{
			mag = AGENT_FORCE;
		}
		else if (D < D_MAX)
		{
			mag = AGENT_FORCE * (D_MAX - D) / (D_MAX - D_MID);
		}
		else
		{
			continue; // magnitude is zero
		}
		float weight = pow(colliding ? 1.f : 0.8f, count++);
		force += forceDir * (mag * weight);
	}

	/************************************************************************/
	/* NOISE TO AVOID DEADLOCKS AND INTRODUCE VARIATIONS					*/
	/************************************************************************/
	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	CrowdsPredictiveRandomStream = FRandomStream(TextKeyUtil::HashString(*GetOwner()->GetName()));
	float angle = CrowdsPredictiveRandomStream.FRandRange(0.0f, 2.0f * PI);
	float dist = CrowdsPredictiveRandomStream.FRandRange(0.0f, 0.001f);
	force += dist * FVector2D(cos(angle), sin(angle));
	
	// Cap the force to maxAcceleration
	if ((force).Size() > MAX_ACCELERATION)
	{
		force = (force).GetSafeNormal() * MAX_ACCELERATION;
	}
	//VH_DEBUG("%s force after noise and clamping: %f, %f", *Agent->GetName(), force[0], force[1])
	//VH_DEBUG("%s desired vel im m/s: %f, %f", *Agent->GetName(), desiredVel[0], desiredVel[1])

	/************************************************************************/
	/* UPDATE THE AGENT'S VELOCITY											*/
	/************************************************************************/
		Velocity = FVector((desiredVel + force * LastDeltaTime) * 100.f, Velocity.Z * 100.f); // assumes unit mass
	// VH_DEBUG("%s final velocity in m/s:, %f, %f, %f", *Agent->GetName(), Velocity[0], Velocity[1], Velocity[2])
}

/** 
*	Determines the time to collision of a ray from the origin with a circle (center, radius)
*	Return val: "infinity" if no collision, else timt to the collision
**/
float UPredictive::RayCircleTTC(const FVector2D& Dir, const FVector2D& Center, float Radius)
{
	float a = (Dir).SizeSquared();
	float b = -2.f * (Dir | Center);
	float c = (Center).SizeSquared() - (Radius * Radius);
	float discr = b * b - 4.f * a * c;
	
	if (discr < 0.f)
	{
		return INFINITY;
	}
	
	const float sqrtDiscr = sqrtf(discr);
	float t0 = (-b - sqrtDiscr) / (2.f * a);
	float t1 = (-b + sqrtDiscr) / (2.f * a);
	
	// If the points of collision have different signs, it means I'm already colliding
	if ((t0 < 0.f && t1 > 0.f) || (t1 < 0.f && t0 > 0.f)) 
		return 0.f;
	
	if (t0 < t1 && t0 > 0.f)
		return t0;
	else if (t1 > 0.f)
		return t1;
	else
		return INFINITY;
}

// after SAFE_DIST intoduction
//const float SAFE_DIST2 = SAFE_DIST * SAFE_DIST;
//auto Obstacles = Agent->GetObstacles(); //empty array in the current implementation if PawnStorm
	/*for (auto Obstacle : Obstacles)
	{
		// TODO: Interaction with obstacles is, currently, defined strictly
		//	by COLLISIONS.  Only if I'm going to collide with an obstacle is
		//	a force applied.  This may be too naive.
		//	I'll have to investigate this.

		FVector2D nearPt; // set by distanceSqToPoint
		float sqDist; // set by distanceSqToPoint
		if (Obstacle.DistanceSqToPoint(FVector2D(CurrentLocation) * 100.0f, nearPt, sqDist) == LAST) continue;
		nearPt /= 100.0f;
		sqDist /= 10000.0f;

		if (SAFE_DIST2 > sqDist)
		{
			// A repulsive force is actually possible
			float dist = sqrtf(sqDist);
			float num = SAFE_DIST - dist;
			float distMradius = (dist - Radius) < PREDICTIVE_EPSILON ? PREDICTIVE_EPSILON : dist - Radius;
			float denom = powf(distMradius, WALL_STEEPNESS);
			FVector2D dir = (FVector2D(CurrentLocation) - nearPt).GetSafeNormal();
			float mag = num / denom;
			force += dir * mag;

			if (Simulation->DebugDraw)
			{
				DrawDebugLine(Agent->GetWorld(), CurrentLocation * 100, (FVector(nearPt, CurrentLocation.Z)) * 100,
							  FColor::Yellow, false, Comp->LastDeltaTime);
			}
			else if (Simulation->DebugDraw)
			{
				DrawDebugLine(Agent->GetWorld(), CurrentLocation * 100, (FVector(nearPt, CurrentLocation.Z)) * 100,
							  FColor::Black, false, Comp->LastDeltaTime);
			}
		}
	}*/

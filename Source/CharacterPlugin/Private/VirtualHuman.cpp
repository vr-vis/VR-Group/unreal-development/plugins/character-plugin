// Fill out your copyright notice in the Description page of Project Settings.


#include "VirtualHuman.h"


#include "VHAnimInstance.h"
#include "VirtualHumanAIController.h"
#include "Engine/Engine.h"
#include "Helper/CharacterPluginLogging.h"


// Sets default values
AVirtualHuman::AVirtualHuman()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AIControllerClass = AVirtualHumanAIController::StaticClass();

	ConstructorHelpers::FClassFinder<UVHAnimInstance> ai_class_mh(TEXT("/CharacterPlugin/Meta_AnimBP"));
	MetaHumanBodyAnimInstanceClass = ai_class_mh.Class;
	ConstructorHelpers::FClassFinder<UVHAnimInstance> ai_class_mh2(TEXT("/CharacterPlugin/Meta_Face_AnimBP"));
	MetaHumanFaceAnimInstanceClass = ai_class_mh2.Class;
	ConstructorHelpers::FClassFinder<UVHAnimInstance> ai_class_cc3(TEXT("/CharacterPlugin/Henry_AnimBP"));
	CC3AnimInstanceClass = ai_class_cc3.Class;

	BodyAnimInstanceToUse = nullptr;
	FaceAnimInstanceToUse = nullptr;
}

// Called when the game starts or when spawned
void AVirtualHuman::BeginPlay()
{
	Super::BeginPlay();
	SetUpBodyType();

	bFirstTick = true;

	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	IdleRandomStream = FRandomStream(TextKeyUtil::HashString(GetName()));
}

// Called every frame
void AVirtualHuman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// documentation only says that GetSlotMontageLocalWeight is 0.0 if nothing is playing, but not whether it is exactly 1.0 if something is playing
	// therefore this more complicated formula is used
	// Furthermore 0.001f is used and not 0.0f since this value is used for blending and once the animation played in the default slot
	// is blended with 0.0f GetSlotMontageLocalWeight() always returns 0.0f since the animation played is not actually used
	// so we would have a circular dependency problem.
	if (UVHAnimInstance* const BodyAnimInstance = GetBodyAnimInstance(); BodyAnimInstance != nullptr)
	{
		BodyAnimInstance->IsDefaultSlotPlayingAnimation =
			BodyAnimInstance->GetSlotMontageLocalWeight("DefaultSlot") == 0.0f ? 0.001f : 1.0f;
		BodyAnimInstance->IsCustomIdleSlotPlayingAnimation =
			BodyAnimInstance->GetSlotMontageLocalWeight("CustomIdleSlot") == 0.0f ? 0.001f : 1.0f;
	}

	if (bFirstTick && CustomIdleAnimations.Num() > 0)
	{
		PlayRandomIdleAnimation();
		bFirstTick = false;
	}
}

// Called to bind functionality to input
void AVirtualHuman::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

USkeletalMeshComponent* AVirtualHuman::GetBodyMesh()
{
	if (BodyType == EBodyType::CC3)
	{
		return GetMesh();
	}
	else if (BodyType == EBodyType::MetaHuman)
	{
		UObject* body_obj = GetDefaultSubobjectByName(TEXT("Body"));
		if (body_obj != nullptr)
		{
			return Cast<USkeletalMeshComponent>(body_obj);
		}
	}

	return nullptr;
}

USkeletalMeshComponent* AVirtualHuman::GetFaceMesh()
{
	if (BodyType == EBodyType::CC3)
	{
		return GetMesh();
	}
	else if (BodyType == EBodyType::MetaHuman)
	{
		UObject* face_obj = GetDefaultSubobjectByName(TEXT("Face"));
		if (face_obj != nullptr)
		{
			return Cast<USkeletalMeshComponent>(face_obj);
		}
	}

	return nullptr;
}

UVHAnimInstance* AVirtualHuman::GetBodyAnimInstance()
{
	if (BodyType == EBodyType::CC3)
	{
		return Cast<UVHAnimInstance>(GetMesh()->GetAnimInstance());
	}
	else if (BodyType == EBodyType::MetaHuman)
	{
		if (const UObject* BodyObj = GetDefaultSubobjectByName(TEXT("Body")); BodyObj != nullptr)
		{
			const USkeletalMeshComponent* Body = Cast<USkeletalMeshComponent>(BodyObj);
			return Cast<UVHAnimInstance>(Body->GetAnimInstance());
		}
	}

	return nullptr;
}

UVHAnimInstance* AVirtualHuman::GetFaceAnimInstance()
{
	if (BodyType == EBodyType::CC3)
	{
		return Cast<UVHAnimInstance>(GetMesh()->GetAnimInstance());
	}
	else if (BodyType == EBodyType::MetaHuman)
	{
		UObject* face_obj = GetDefaultSubobjectByName(TEXT("Face"));
		if (face_obj != nullptr)
		{
			USkeletalMeshComponent* face = Cast<USkeletalMeshComponent>(face_obj);
			return Cast<UVHAnimInstance>(face->GetAnimInstance());
		}
	}

	return nullptr;
}

const BoneNames& AVirtualHuman::GetBoneNames()
{
	if (BoneNames.boneroot == NAME_None)
	{
		SetUpBodyType();
	}
	return BoneNames;
}

EBodyType AVirtualHuman::GetBodyType()
{
	return BodyType;
}

EVHGender AVirtualHuman::GetGender()
{
	return Gender;
}

bool AVirtualHuman::SupportsMorphs()
{
	return BodyType != EBodyType::MetaHuman;
}

bool AVirtualHuman::ForcePlayNewIdleAnimation()
{
	if (CustomIdleAnimations.IsEmpty()) return false;

	NextIdleTimerHandle.Invalidate();
	PlayRandomIdleAnimation();

	return true;
}

void AVirtualHuman::SetUpBodyType()
{
	if (BodyType == EBodyType::CC3)
	{
		if (BodyAnimInstanceToUse != nullptr)
		{
			GetBodyMesh()->SetAnimInstanceClass(BodyAnimInstanceToUse);
		}
		else
		{
			GetBodyMesh()->SetAnimInstanceClass(CC3AnimInstanceClass);
		}
		//AnimationInstance = Cast<UVHAnimInstance>(GetMesh()->GetAnimInstance());

		//This is the CC naming convention as it is for the current Henry model
		BoneNames.boneroot = TEXT("root");
		BoneNames.spine01 = TEXT("spine_01");
		BoneNames.spine02 = TEXT("spine_02");
		BoneNames.spine03 = TEXT("spine_03");
		BoneNames.spine04 = TEXT("");
		BoneNames.spine05 = TEXT("");

		BoneNames.neck = TEXT("neck_01");
		BoneNames.neck02 = TEXT("");
		BoneNames.head = TEXT("head");
		BoneNames.eye_r = TEXT("cc_base_r_eye");
		BoneNames.eye_l = TEXT("cc_base_l_eye");
		BoneNames.jaw = TEXT("cc_base_jawroot");
		BoneNames.teeth_lower = TEXT("cc_base_jawroot");
		BoneNames.mouth_lower = TEXT("cc_base_jawroot");

		BoneNames.clavicle_l = TEXT("clavicle_l");
		BoneNames.clavicle_r = TEXT("clavicle_r");
		BoneNames.upperarm_l = TEXT("upperarm_l");
		BoneNames.upperarm_r = TEXT("upperarm_r");
		BoneNames.lowerarm_l = TEXT("lowerarm_l");
		BoneNames.lowerarm_r = TEXT("lowerarm_r");
		BoneNames.hand_l = TEXT("hand_l");
		BoneNames.hand_r = TEXT("hand_r");

		BoneNames.pelvis = TEXT("pelvis");
		BoneNames.calf_l = TEXT("calf_l");
		BoneNames.calf_r = TEXT("calf_r");
		BoneNames.thigh_l = TEXT("thigh_l");
		BoneNames.thigh_r = TEXT("thigh_r");
		BoneNames.foot_r = TEXT("foot_r");
		BoneNames.foot_l = TEXT("foot_l");
		BoneNames.foot_socket_r = TEXT("foot_rSocket");
		BoneNames.foot_socket_l = TEXT("foot_lSocket");
	}
	else if (BodyType == EBodyType::MetaHuman)
	{
		UObject* root_obj = GetDefaultSubobjectByName(TEXT("Root"));
		USkeletalMeshComponent* body = GetBodyMesh();
		USkeletalMeshComponent* face = GetFaceMesh();
		FStreamableManager AssetLoader;
		FSoftObjectPath AssetRef(
			FPaths::ConvertRelativePathToFull(
				"/CharacterPlugin/Characters/MetaHuman/metahuman_base_skel.metahuman_base_skel"));
		UObject* body_skeleton_obj = AssetLoader.LoadSynchronous(AssetRef);
		FStreamableManager AssetLoader2;
		FSoftObjectPath AssetRef2(FPaths::ConvertRelativePathToFull(
			"/CharacterPlugin/Characters/MetaHuman/Face_Archetype53_Skeleton.Face_Archetype53_Skeleton"));
		UObject* face_skeleton_obj = AssetLoader2.LoadSynchronous(AssetRef2);

		if (root_obj != nullptr && body != nullptr && body_skeleton_obj != nullptr && face != nullptr &&
			face_skeleton_obj != nullptr)
		{
			USceneComponent* root = Cast<USceneComponent>(root_obj);
			USkeleton* body_skel = Cast<USkeleton>(body_skeleton_obj);
			USkeleton* face_skel = Cast<USkeleton>(face_skeleton_obj);

			root->SetRelativeLocationAndRotation(FVector(0, 0, -87), FRotator(0, -90, 0));

			body->GetSkeletalMeshAsset()->SetSkeleton(body_skel);
			if (BodyAnimInstanceToUse != nullptr)
			{
				body->SetAnimInstanceClass(BodyAnimInstanceToUse);
			}
			else
			{
				body->SetAnimInstanceClass(MetaHumanBodyAnimInstanceClass);
			}

			face->GetSkeletalMeshAsset()->SetSkeleton(face_skel);
			if (FaceAnimInstanceToUse != nullptr)
			{
				face->SetAnimInstanceClass(FaceAnimInstanceToUse);
			}
			else
			{
				face->SetAnimInstanceClass(MetaHumanFaceAnimInstanceClass);
			}

			//AnimationInstance = Cast<UVHAnimInstance>(body->GetAnimInstance());

			BoneNames.boneroot = TEXT("root");
			BoneNames.spine01 = TEXT("spine_01");
			BoneNames.spine02 = TEXT("spine_02");
			BoneNames.spine03 = TEXT("spine_03");
			BoneNames.spine04 = TEXT("spine_04");
			BoneNames.spine05 = TEXT("spine_05");

			BoneNames.neck = TEXT("neck_01");
			BoneNames.neck02 = TEXT("neck_02");
			BoneNames.head = TEXT("head");
			BoneNames.eye_r = TEXT("FACIAL_R_Eye");
			BoneNames.eye_l = TEXT("FACIAL_L_Eye");
			BoneNames.jaw = TEXT("FACIAL_C_Jaw");
			BoneNames.teeth_lower = TEXT("FACIAL_C_TeethLower");
			BoneNames.mouth_lower = TEXT("FACIAL_C_MouthLower");

			BoneNames.clavicle_l = TEXT("clavicle_l");
			BoneNames.clavicle_r = TEXT("clavicle_r");
			BoneNames.upperarm_l = TEXT("upperarm_l");
			BoneNames.upperarm_r = TEXT("upperarm_r");
			BoneNames.lowerarm_l = TEXT("lowerarm_l");
			BoneNames.lowerarm_r = TEXT("lowerarm_r");
			BoneNames.hand_l = TEXT("hand_l");
			BoneNames.hand_r = TEXT("hand_r");

			BoneNames.pelvis = TEXT("pelvis");
			BoneNames.calf_l = TEXT("calf_l");
			BoneNames.calf_r = TEXT("calf_r");
			BoneNames.thigh_l = TEXT("thigh_l");
			BoneNames.thigh_r = TEXT("thigh_r");
			BoneNames.foot_r = TEXT("foot_r");
			BoneNames.foot_l = TEXT("foot_l");
			BoneNames.foot_socket_r = TEXT("ball_r");
			BoneNames.foot_socket_l = TEXT("ball_l");
		}
		else
		{
			VH_WARN(
				"The structure in the skeletal mesh components does not fit a typical meta human structure! Some features may not work.\n");
		}
	}
}

void AVirtualHuman::PlayRandomIdleAnimation()
{
	if (CustomIdleAnimations.Num() == 0)
	{
		return;
	}

	// play random idle animation
	const int Random = IdleRandomStream.RandRange(0, CustomIdleAnimations.Num() - 1);
	if (UAnimSequence* IdleAnim = CustomIdleAnimations[Random])
	{
		const float BlendTime = 0.25f;
		const UAnimMontage* DynamicMontage = GetBodyAnimInstance()->PlaySlotAnimationAsDynamicMontage(
			IdleAnim, "CustomIdleSlot", BlendTime, BlendTime);
		// start timer for next random pick (make it a bit shorter to allow for blending, but only by a defined minimal amount for the animations)
		GetWorld()->GetTimerManager().SetTimer(NextIdleTimerHandle, this, &AVirtualHuman::PlayRandomIdleAnimation,
		                                       DynamicMontage->GetPlayLength() - fmin(
			                                       2.0 * BlendTime, (DynamicMontage->GetPlayLength()) * MinBlendTime),
		                                       false);
	}
	else
	{
		VH_ERROR("[AVirtualHuman::PlayRandomIdleAnimation]: IdleAnimation is not valid");
	}
}

#if WITH_EDITOR
void AVirtualHuman::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FProperty* prop = PropertyChangedEvent.Property;
	if (prop != nullptr && prop->GetNameCPP().Compare(TEXT("BodyType")) == 0)
	{
		SetUpBodyType();
	}
}
#endif

void AVirtualHuman::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	SetUpBodyType();
}

// Fill out your copyright notice in the Description page of Project Settings.

#include "VHFacialExpressions.h"
#include "VHAnimInstance.h"
#include "Helper/CharacterPluginLogging.h"
#include "Math/UnrealMathUtility.h"
#include "VirtualHuman.h"

// Sets default values for this component's properties
UVHFacialExpressions::UVHFacialExpressions()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	LastSelectedEmotion = SelectedEmotion;

	FACSExpressionsLibrary = {
		{
			Happiness, {
				{6, 0.5, AU_Both},
				{12, 0.7, AU_Both}
			}
		},
		{
			Sadness,{
				{1, 0.5, AU_Both},
				{4, 0.3, AU_Both},
				{15, 0.7, AU_Both}
			}
		},
		{
			Surprise,{
				{1, 0.5, AU_Both},
				{2, 0.3, AU_Both},
				{26, 0.4, AU_Both}
			}
		},
		{
			Fear,{
				{1, 0.5, AU_Both},
				{2, 0.3, AU_Both},
				{4, 0.2, AU_Both},
				{7, 0.4, AU_Both},
				{26, 0.55, AU_Both}
			}
		},
		{
			Anger,{
				{4, 0.7, AU_Both},
				{7, 0.4, AU_Both},
				{23, 0.7, AU_Both}
			}
		},
		{
			Disgust,{
				{9, 0.6, AU_Both},
				{15, 0.3, AU_Both},
				{16, 0.5, AU_Both}
			}
		},
		{
			Contempt,{
				{12, 0.5, AU_Both},
				{14, 0.7, AU_Left}
			}
		},
		{
			SlightSmile,{
				{12, 0.3, AU_Both}
			}
		},
		{
			Custom,{}
		}
	};
}


// Called when the game starts
void UVHFacialExpressions::BeginPlay()
{
	Super::BeginPlay();
	owner = Cast<AVirtualHuman>(GetOwner());
	if (owner != nullptr) {
		AnimInstance = owner->GetFaceAnimInstance();
	}
	else {
		AnimInstance = Cast<UVHAnimInstance>(Cast<USkeletalMeshComponent>(GetOwner()->GetComponentByClass(USkeletalMeshComponent::StaticClass()))->GetAnimInstance());
	}

	if (!AnimInstance) {
		VH_WARN("No AnimInstance at BeginPlay\n");
	}
	if (Mapping == nullptr) {
		VH_WARN("No Mapping at BeginPlay\n");
	}

	SetupAUMapping();
}

void UVHFacialExpressions::SetupAUMapping()
{

  //Load Poses from PoseAsset into FAUPoses
  if (Mapping != nullptr) {
	  
	  TArray<FName> BlendshapeNames = Mapping->GetCurveFNames();
	  FDefaultValueHelper dvh;

	  for (FName PoseName : Mapping->GetPoseFNames()) {
		  
		  FString Name = PoseName.ToString();
		  FACSSidedness side = FACSSidedness::AU_Both;
		  if (Name.EndsWith(TEXT("_r"))) {
			  Name.RemoveFromEnd("_r");
			  side = FACSSidedness::AU_Right;
		  }
		  else if (Name.EndsWith(TEXT("_l"))) {
			  Name.RemoveFromEnd("_l");
			  side = FACSSidedness::AU_Left;
		  }

		  int AUNumber;
		  bool success = dvh.ParseInt(Name, AUNumber);
		  if (success) {
			  
			  TArray<float> PoseValues = Mapping->GetCurveValues(Mapping->GetPoseIndexByName(PoseName));

			  FAUPose* added = AUPoses.Find(AUNumber);
			  bool found = (added != nullptr);
			  if (!found) {
				  added = &AUPoses.Add(AUNumber, FAUPose());
			  }
			  else if (found && side == FACSSidedness::AU_Both) {
				  //only one pose for poses, which are not sided
				  continue;
			  }

			  added->ActionUnitNumber = AUNumber;
			  if (side == FACSSidedness::AU_Both) {
				  added->bSided = false;
				  added->Blendshapes = { {} };
			  }
			  else {
				  added->bSided = true;
				  if (added->Blendshapes.Num() == 0) {
					  added->Blendshapes = { {}, {} };
				  }
			  }

			  for (int i = 0; i < BlendshapeNames.Num(); i++) {
				  if (std::abs(PoseValues[i]) > 0.05f) {

					  FAUBlendshape bs;
					  bs.ActionUnitNumber = AUNumber;
					  bs.BlendShapeName = BlendshapeNames[i].ToString();
					  bs.BlendShapeValue = PoseValues[i];

					  if (side == FACSSidedness::AU_Both || side == FACSSidedness::AU_Left) {
						  added->Blendshapes[0].Add(bs);
					  }
					  else {
						  added->Blendshapes[1].Add(bs);
					  }

					  if (!CurrentAnimationValues.Contains(FName(*bs.BlendShapeName))) {
						  CurrentAnimationValues.Add(FName(*bs.BlendShapeName), 0.f);
					  }

				  }
			  }

		  }
	  }
  }
}


// Called every frame
void UVHFacialExpressions::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!AnimInstance) {
		if (owner != nullptr) {
			AnimInstance = owner->GetFaceAnimInstance();
		}
		else {
			AnimInstance = Cast<UVHAnimInstance>(Cast<USkeletalMeshComponent>(GetOwner()->GetComponentByClass(USkeletalMeshComponent::StaticClass()))->GetAnimInstance());
		}
	}
	else if (Mapping != nullptr)
	{

		if (LastSelectedEmotion != SelectedEmotion) {
			CurrentEmotion = LastSelectedEmotion;
			LastSelectedEmotion = SelectedEmotion;
		}

		if (CurrentEmotion != SelectedEmotion && SelectedEmotion != Custom)
		{
			EvaluatedNames.Empty();
			if (SelectedEmotion == Neutral)
			{
				if (ResetFACS(DeltaTime))
				{
					CurrentEmotion = SelectedEmotion;
				}
			}
			else
			{
				if (UpdateFACS(SelectedEmotion, DeltaTime))
				{
					CurrentEmotion = SelectedEmotion;
				}
			}
		}
		else if (SelectedEmotion == Custom)
		{
			EvaluatedNames.Empty();
			CurrentEmotion = SelectedEmotion;
			UpdateFACS(SelectedEmotion, DeltaTime);
		}
	}
}

/*
 *	Updates relevant AU values per selected Emotion. Resets the inactive AUs.
 *	returns true when interpolation has been finished and desired value has been set set.
 *	returns false otherwise.
 */
bool UVHFacialExpressions::UpdateFACS(Emotions Emotion, float DeltaTime)
{
	bool isInterpolationFinished = true;
	TArray<int> ActiveAU;
	for (FACSValue Element : FACSExpressionsLibrary[Emotion])
	{
		if (!SetFACSActionUnit(Element.AUNumber, Element.Value, DeltaTime, Element.Side))
		{
			isInterpolationFinished = false;
		}
		ActiveAU.Add(Element.AUNumber);
	}
	for (TTuple<Emotions, TArray<FACSValue>> EmotionEntry : FACSExpressionsLibrary)
	{
		if (EmotionEntry.Key != Emotion)
		{
			for (auto Element : FACSExpressionsLibrary[EmotionEntry.Key])
			{
				if (!ActiveAU.Contains(Element.AUNumber))
				{
					if (!SetFACSActionUnit(Element.AUNumber, 0, DeltaTime, Element.Side))
					{
						isInterpolationFinished = false;
					}
				}

			}
		}
	}
	return isInterpolationFinished;
}

const TMap<FName, float>& UVHFacialExpressions::GetCurrentAnimationValues() const
{
	return CurrentAnimationValues;
}

/*
 *	Resets all AU values.
 */
bool UVHFacialExpressions::ResetFACS(float DeltaTime)
{
	bool isInterpolationFinished = true;
	for (TTuple<Emotions, TArray<FACSValue>> EmotionEntry : FACSExpressionsLibrary)
	{
		TArray<int> ProcessedAU;
		for (FACSValue Element : EmotionEntry.Value)
		{
			if (!ProcessedAU.Contains(Element.AUNumber))
			{
				if (!SetFACSActionUnit(Element.AUNumber, 0, DeltaTime, Element.Side))
				{
					isInterpolationFinished = false;
				}
				ProcessedAU.Add(Element.AUNumber);
			}
		}
	}
	return  isInterpolationFinished;
}


/*
 *	Adds a custom defined array of FACS values to the FACSExpressionsLibrary
 *	Interpolates to them smoothly continuously during the runtime
 */
void UVHFacialExpressions::VHSetFACSValues(TArray<FACSValue> FACSValues)
{
	FACSExpressionsLibrary.Add(Custom,FACSValues);
}


/*
 *	Assigns a value to a given Blend Shape without interpolation.
 *	Name should be passed as a FString.
 */
void UVHFacialExpressions::VHSetMorphTarget(FString Name, float Value)
{
	if(AnimInstance && !EvaluatedNames.Contains(Name))
	{
		EvaluatedNames.Add(Name);
		CurrentAnimationValues.Emplace(FName(*Name), Value);
		if (owner != nullptr && owner->SupportsMorphs()) {
			// in this case set it directly otherwise the data (CurrentAnimationValues) will be pulled from the animation graph
			AnimInstance->SetMorphTarget(FName(*Name), Value);
		}
	}
}

/*
 *	Assigns a new value for a given Blend Shape with interpolation.
 *	Requires delta time for interpolation.
 *	returns true when interpolation has been finished and desired value has been set set.
 *	returns false otherwise.
 */
bool UVHFacialExpressions::VHSetMorphTarget(FString Name, float NewValue, float deltaTime)
{
	if(AnimInstance)
	{
		if (EvaluatedNames.Contains(Name)) {
			return true;
		}

		EvaluatedNames.Add(Name);
		USkeletalMeshComponent* Component = AnimInstance->GetOwningComponent();
		float Value = *CurrentAnimationValues.Find(FName(*Name));

		if(Value!=NewValue)
		{
			Value = FMath::FInterpConstantTo(Value, NewValue, deltaTime, InterpSpeed);
			CurrentAnimationValues.Emplace(FName(*Name), Value);
			if (owner != nullptr && owner->SupportsMorphs()) {
				// in this case set it directly otherwise the data (CurrentAnimationValues) will be pulled from the animation graph
				AnimInstance->SetMorphTarget(FName(*Name), Value);
			}
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
}

/*
 *	Sets the given value to a given AU and AU Sidedness (AU_Both, AU_Left, AU_Right).
 *	Requires delta time for interpolation.
 *	Returns false if:
 *		AU does not exist
 *		AnimInstance is not attached
 *		Interpolation is not finished
 *	Returns true otherwise, i.e. interpolation has finished and new value has been set.
 */
bool UVHFacialExpressions::SetFACSActionUnit(int AUNumber, float Value, float deltaTime, FACSSidedness Side /*=AU_Both*/)
{
	if (!AUPoses.Contains(AUNumber))
	{
		VH_WARN("AU %d is not mapped in PoseAsset Mapping.\n", AUNumber)
			return false;
	}

	if (!AnimInstance)
	{
		VH_WARN("No AnimInstance present in SetFACSActionUnit, cannot activate AU %d\n", AUNumber);
		return false;
	}

	bool InterpolationFinished = true;

	FAUPose* AUPose = AUPoses.Find(AUNumber);

	TArray<int> SideIndices;
	if (Side == FACSSidedness::AU_Both) {
		if (AUPose->bSided) {
			SideIndices = { 0 , 1 };
		}
		else {
			SideIndices = { 0 };
		}
	}
	else if (Side == FACSSidedness::AU_Left) {
		SideIndices = { 0 };
	}
	else {
		if (AUPose->bSided) {
			SideIndices = { 1 };
		}
		else {
			SideIndices = { 0 };
		}
	}



	for (int i = 0; i < SideIndices.Num(); i++) {

		const TArray<FAUBlendshape>& Blendshapes = AUPose->Blendshapes[SideIndices[i]];

		for (int bsIndex = 0; bsIndex < Blendshapes.Num(); bsIndex++) {

			const FAUBlendshape& Blendshape = Blendshapes[bsIndex];

			bool finished = VHSetMorphTarget(Blendshape.BlendShapeName, Value * Blendshape.BlendShapeValue, deltaTime);

			if (!finished) {
				InterpolationFinished = false;
			}

		}
	}

	return InterpolationFinished;
}

bool UVHFacialExpressions::SetFACSActionUnit(int AUNumber, float Value, FACSSidedness Side /*=AU_Both*/)
{
	if (SelectedEmotion != Custom)
	{
		SelectedEmotion = Custom;
	}
	if (!AUPoses.Contains(AUNumber))
	{
		VH_WARN("AU %d is not mapped in PoseAsset Mapping.\n", AUNumber)
        return false;
	}
	if (!AnimInstance)
	{
		VH_WARN("No AnimInstance present in SetFACSActionUnit, cannot activate AU %d\n", AUNumber);
		return false;
	}

	FAUPose* AUPose = AUPoses.Find(AUNumber);

	TArray<int> SideIndices;
	if (Side == FACSSidedness::AU_Both) {
		if (AUPose->bSided) {
			SideIndices = { 0 , 1 };
		}
		else {
			SideIndices = { 0 };
		}
	}
	else if (Side == FACSSidedness::AU_Left) {
		SideIndices = { 0 };
	}
	else {
		if (AUPose->bSided) {
			SideIndices = { 1 };
		}
		else {
			SideIndices = { 0 };
		}
	}

	for (int i = 0; i < SideIndices.Num(); i++) {

		const TArray<FAUBlendshape>& Blendshapes = AUPose->Blendshapes[SideIndices[i]];

		for (int bsIndex = 0; bsIndex < Blendshapes.Num(); bsIndex++) {

			const FAUBlendshape& Blendshape = Blendshapes[bsIndex];

			FACSExpressionsLibrary.Add(Custom, { {AUNumber, Value, Side} });
			VHSetMorphTarget(Blendshape.BlendShapeName, Value * Blendshape.BlendShapeValue);

		}
	}

	return true;
}
	









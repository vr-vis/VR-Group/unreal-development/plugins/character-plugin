// Fill out your copyright notice in the Description page of Project Settings.


#include "VHSpeech.h"

#include "EngineUtils.h"
#include "Helper/CharacterPluginLogging.h"

#include "VHAnimInstance.h"
#include "VHLiveLinkFaceAnimation.h"
#include "AssetRegistry/AssetRegistryModule.h"

// Sets default values for this component's properties
UVHSpeech::UVHSpeech()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}

// Called when the game starts
void UVHSpeech::BeginPlay()
{
	Super::BeginPlay();
	if (!Init()) return;
	// ...

	// constantly seed the random stream so we don't have cluster syncing problems
	// but seed it with the name of the VH, so random behavior is different for every VH
	SpeechRandomStream = FRandomStream(TextKeyUtil::HashString(*GetOwner()->GetName()));

	// load speaking animations from folder
	SpeakingAnimations = LoadDefaultAnimations();
}


// Called every frame
void UVHSpeech::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// workaround for missing OnAudioFinished() delegate
	if (bIsSpeaking)
	{
		// if audio is playing we CAN finish
		if (AudioComp->GetAudiofileSignalSource()->GetPlayActionEnum() == EPlayAction::Play)
		{
			bAudioCanFinish = true;
			
		}// if audio is not playing anymore, but was playing before
		else if (bAudioCanFinish)
		{
			OnAudioFinished();
			bAudioCanFinish = false;
		}
	}

	// ...
}


void UVHSpeech::PlayDialogueArray(TArray<FDialogueUtterance> Dialogues, float MinDelay, float MaxDelay)
{
	//VH_DEBUG("PLAY DIALOGUE")
	DialogueUtterances = Dialogues;
	DialogueIt = 0;
	MinD = MinDelay;
	MaxD = MaxDelay;
	// call NextDialogue() after initial delay
	StartNextDialogueTimer();

}
void UVHSpeech::PlayDialogue(FDialogueUtterance Dialogue, float Delay)
{
 
	TArray<FDialogueUtterance> Dialogues;
	Dialogues.Add(Dialogue);
	DialogueUtterances = Dialogues;
	DialogueIt = 0;
	MinD = Delay;
	MaxD = Delay;
	StartNextDialogueTimer();

}

void UVHSpeech::StartNextDialogueTimer()
{
	float Delay = SpeechRandomStream.FRandRange(MinD, MaxD);
	if(Delay >0.0f){
		GetWorld()->GetTimerManager().SetTimer(SpeakerTimerHandle, this, &UVHSpeech::NextDialogue, Delay, false);
	}
	else
	{
		NextDialogue();
	}
}

void UVHSpeech::NextDialogue()
{
	if (DialogueIt >= DialogueUtterances.Num())
	{
		OnDialogueCompleted();
		return;
	}
	//VH_DEBUG("NEXT DIALOGUE")
	FDialogueUtterance Dialogue = DialogueUtterances[DialogueIt];
	bIsSpeaking = true;

	//first load both face and audio:
	AudioComp->GetAudiofileSignalSource()->SetAudiofile(Dialogue.Audiofile);
	if(!Dialogue.FaceAnimFile.IsEmpty()){
		FaceAnimComp->LoadAnimationFile(Dialogue.FaceAnimFile);
	}
	
	//then play it (to prevent lags in between!)
	// use play from time, to prevent paused audio from resuming
	AudioComp->PlayFromTime(0.0f);
	if(!Dialogue.FaceAnimFile.IsEmpty()){
		FaceAnimComp->PlayFromTime(0.0f);
	}

	if (bUseSpeechBodyAnimation)
	{
		if (Dialogue.BodyAnim)
		{
			SpeakingMontage = VH->GetBodyAnimInstance()->PlaySlotAnimationAsDynamicMontage(Dialogue.BodyAnim, bPlaySpeechAnimationOnWholeBody?"DefaultSlot":"UpperBodySlot", AnimationBlendTime, AnimationBlendTime);
		}
		else {
			PlayRandomSpeakingAnimation();
		}
	}
}

void UVHSpeech::PlayRandomSpeakingAnimation()
{

	if (!bIsSpeaking)
	{
		return;
	}

	// play animation montage with body gestures for speaking
	int R = SpeechRandomStream.RandRange(0, SpeakingAnimations.Num() - 1);
	UAnimSequence* SpeakingAnim = SpeakingAnimations[R];
	if (SpeakingAnim)
	{
		SpeakingMontage = VH->GetBodyAnimInstance()->PlaySlotAnimationAsDynamicMontage(SpeakingAnim, bPlaySpeechAnimationOnWholeBody ? "DefaultSlot" : "UpperBodySlot", AnimationBlendTime, AnimationBlendTime);
		// start timer and check if the speech has finished after the montage is over, if not, start a new montage
		StartSpeakingMontageTimer(SpeakingMontage->GetPlayLength());

	}
	else
	{
		VH_ERROR("[VHDialogue]: SpeakingAnim is not valid");
	}

}

void UVHSpeech::StartSpeakingMontageTimer(float MontageLength)
{
	GetWorld()->GetTimerManager().SetTimer(SpeakerTimerHandle, this, &UVHSpeech::PlayRandomSpeakingAnimation, MontageLength - AnimationBlendTime, false);
}

void UVHSpeech::OnAudioFinished()
{
	//VH_DEBUG("OnAudioFinished");
	StopMontages();
	bIsSpeaking = false;
	DialogueIt++;
	StartNextDialogueTimer();
}

void UVHSpeech::OnDialogueCompleted()
{
	// broadcast delegate
	DialogueCompletedDelegate.Broadcast();

}

void UVHSpeech::PreloadUtterance(FDialogueUtterance Utterance)
{
	if (!bHasBeenInitialized)
	{
		Init();
	}
	AudioComp->GetAudiofileSignalSource()->PreLoadAudiofile(Utterance.Audiofile);
	if(!Utterance.FaceAnimFile.IsEmpty())
		FaceAnimComp->LoadAnimationFile(Utterance.FaceAnimFile);
}

float UVHSpeech::GetCurrentUtteranceLength() const
{
	// actually getting the length of the audiofile would be nice, but seems hard to do with VA Interface as of now
	// so we use the length of the face animation, make sure it's cut accordingly!
	return FaceAnimComp->GetDuration();
}

UVAAudiofileSourceComponent* UVHSpeech::GetAudioComponent() 
{
	return AudioComp;
}

void UVHSpeech::SetAudioComponent(UVAAudiofileSourceComponent* AudioSourceComponent)
{
	AudioComp = AudioSourceComponent;
}

UVHFaceAnimation* UVHSpeech::GetFaceAnimComponent() const
{
	return FaceAnimComp;
}

void UVHSpeech::StopDialogue()
{
	StopMontages();
	AudioComp->Stop();
	FaceAnimComp->Stop();
}

void UVHSpeech::PauseDialogue()
{
	StopMontages();
	AudioComp->Pause();
	FaceAnimComp->Stop();
}

TArray<UAnimSequence*> UVHSpeech::LoadDefaultAnimations()
{
	TArray<UAnimSequence*> Animations;
	// load animations for hand gestures
	FString Path = "";
	if (VH->GetBodyType() == EBodyType::CC3)
	{
		Path = "/CharacterPlugin/Animations/Talking/CC3";
	}
	else if (VH->GetBodyType() == EBodyType::MetaHuman)
	{
		Path = "/CharacterPlugin/Animations/Talking/MH";
	}

	if (!FPaths::ValidatePath(Path))
	{
		VH_WARN("[VHSpeech::LoadAnimationMontages] Path: %s is not valid\n", *Path)
	}

	TArray<UObject*> Assets;
	EngineUtils::FindOrLoadAssetsByPath(Path, Assets, EngineUtils::ATL_Regular);

	for (UObject* Asset : Assets)
	{
		UAnimSequence* Anim = Cast<UAnimSequence>(Asset);
		if (Anim)
		{
			Animations.Add(Anim);
		}
	}

	if(Animations.Num()==0)
	{
		VH_WARN("[VHSpeech::LoadDefaultAnimations] Could not find a valid UAnimSequence at %s", *Path)
	}

	return Animations;
}

void UVHSpeech::StopMontages() const
{
	if (SpeakingMontage != nullptr && VH->GetBodyAnimInstance()->Montage_IsPlaying(SpeakingMontage))
	{
		VH->GetBodyAnimInstance()->Montage_Stop(AnimationBlendTime, SpeakingMontage);
	}
}

bool UVHSpeech::Init()
{
	VH = Cast<AVirtualHuman>(GetOwner());
	if (!VH)
	{
		VH_ERROR("[VHSpeech::Init] Virtual Human is not valid.\n")
		return false;
	}
	//Maybe this value has been set before, if not, revert to default implementation
	if (!AudioComp)
	{
		AudioComp = GetOwner()->FindComponentByClass<UVAAudiofileSourceComponent>();
	}
	//Here: Allow Components to be NULL, at one's own risk: REMEMBER TO INITIALIZE
	if (!AudioComp)
	{
		VH_LOG("[VHSpeech::Init] No AudioComp set or found. This can be ok, just make sure it is set before playing an utterance!\n")
	}
	FaceAnimComp = GetOwner()->FindComponentByClass<UVHFaceAnimation>();
	if (!FaceAnimComp)
	{
		VH_ERROR("[VHSpeech::Init] Cannot find a FaceAnimation component.\n")
		return false;
	}
	bHasBeenInitialized = true;
	return true;
}

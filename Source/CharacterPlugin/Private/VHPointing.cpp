// Fill out your copyright notice in the Description page of Project Settings.


#include "VHPointing.h"
#include "Helper/CharacterPluginLogging.h"
#include "VirtualHuman.h"

// Sets default values for this component's properties
UVHPointing::UVHPointing()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVHPointing::BeginPlay()
{
	Super::BeginPlay();
	
	
}


// Called every frame
void UVHPointing::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (VHAnimInstance == nullptr) {
		AVirtualHuman* owner = Cast<AVirtualHuman>(GetOwner());
		if (owner != nullptr) {
			VHAnimInstance = owner->GetBodyAnimInstance();
		}
		else {
			VHAnimInstance = Cast<UVHAnimInstance>(Cast<USkeletalMeshComponent>(GetOwner()->GetComponentByClass(USkeletalMeshComponent::StaticClass()))->GetAnimInstance());
		}
		if (!VHAnimInstance) {
			VH_ERROR("[UVHPointing::TickComponent] No VHAnimInstance component attached");
		}
	}
	else
	{
		VHAnimInstance->EffectorPosition = PointingTarget;
		if (bIsPointing)
		{
			FVector DirVHTarget = PointingTarget - GetOwner()->GetActorLocation();
			float AngleLeftRight = FVector::DotProduct(FVector::CrossProduct(GetOwner()->GetActorUpVector().GetSafeNormal(), DirVHTarget.GetSafeNormal()), GetOwner()->GetActorForwardVector().GetSafeNormal());
			float AngleFrontTarget = FMath::Acos(FVector::DotProduct(DirVHTarget.GetSafeNormal(), GetOwner()->GetActorForwardVector().GetSafeNormal()));
			if (AngleFrontTarget > (180-BlindAngle)*PI/180.0) {
				VHAnimInstance->PointingUseHand = EPointingHand::HandNone;
			}
			else 
			{
				if (AngleLeftRight > 0) {
					VHAnimInstance->PointingUseHand = EPointingHand::HandLeft;
				}
				else
				{
					VHAnimInstance->PointingUseHand = EPointingHand::HandRight;
				}
			}
		}
		else
		{
			VHAnimInstance->PointingUseHand = EPointingHand::HandNone;
		}
		
	}
	
}


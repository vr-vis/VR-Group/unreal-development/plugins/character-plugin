// Fill out your copyright notice in the Description page of Project Settings.


#include "Waypoint.h"
#include "GameFramework/Character.h"

AWaypoint::AWaypoint()
{
	// Defaults
	WalkingSpeed = 100.0f;
	WaypointGroup = 0;
	WaypointOrder = 0;

}


int AWaypoint::GetWaypointOrder()
{
	return WaypointOrder;
}

float AWaypoint::GetWalkingSpeed()
{
	return WalkingSpeed;
}

int AWaypoint::GetWaypointGroup()
{
	return WaypointGroup;
}

int AWaypoint::GetLastWaypointCommand()
{
	return OnReachLastWaypoint;
}






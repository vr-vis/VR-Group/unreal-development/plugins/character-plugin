// This Animgraph node file is a modification from the file "AnimNode_ModifyCurve.cpp" by Epic Games (in UE 4.26)


#include "AnimNodes/ModifyCurvesNode.h"
#include "AnimationRuntime.h"
#include "Animation/AnimInstanceProxy.h"

FModifyCurvesNode::FModifyCurvesNode()
{
	ApplyMode = EModifyCurveApplyModeLocal::Blend;
	Alpha = 1.f;
	FaceAnimInstance = nullptr;
	FacialExpressionsInstance = nullptr;
}

void FModifyCurvesNode::Initialize_AnyThread(const FAnimationInitializeContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Initialize_AnyThread)
	Super::Initialize_AnyThread(Context);
	SourcePose.Initialize(Context);
}

void FModifyCurvesNode::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(CacheBones_AnyThread)
	Super::CacheBones_AnyThread(Context);
	SourcePose.CacheBones(Context);
}

void FModifyCurvesNode::Evaluate_AnyThread(FPoseContext& Output)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)
	FPoseContext SourceData(Output);
	SourcePose.Evaluate(SourceData);

	Output = SourceData;

	if (FacialExpressionsInstance != nullptr) {

		const TMap<FName, float> AnimData = FacialExpressionsInstance->GetCurrentAnimationValues();

		//	Morph target and Material parameter curves
		USkeleton* Skeleton = Output.AnimInstanceProxy->GetSkeleton();

		/*TArray<FName> CurveNames;
		Skeleton->GetCurveMetaDataNames(CurveNames);
		UE_LOG(LogTemp, Warning, TEXT("Existing CurveMapping Names are:"));
		for (FName Name : CurveNames) {
			UE_LOG(LogTemp, Warning, TEXT("%s"), *Name.ToString());
		}*/

		for (auto& Elem : AnimData)
		{

			FName CurveName = Elem.Key;
			FCurveMetaData* CurveMetaData = Skeleton->GetCurveMetaData(CurveName);

			//UE_LOG(LogTemp, Warning, TEXT("Set %s to %f"), *CurveName.ToString(), Elem.Value);

			if (CurveMetaData)
			{
				float CurveValue = Elem.Value;

				float CurrentValue = Output.Curve.Get(CurveName);

				// Use ApplyMode enum to decide how to apply
				if (ApplyMode == EModifyCurveApplyModeLocal::Add)
				{
					Output.Curve.Set(CurveName, CurrentValue + CurveValue);
				}
				else if (ApplyMode == EModifyCurveApplyModeLocal::Scale)
				{
					Output.Curve.Set(CurveName, CurrentValue * CurveValue);
				}
				else if (ApplyMode == EModifyCurveApplyModeLocal::RemapCurve)
				{
					const float RemapScale = 1.f / FMath::Max(1.f - CurveValue, 0.01f);
					const float RemappedValue = FMath::Min(FMath::Max(CurrentValue - CurveValue, 0.f) * RemapScale, 1.f);
					Output.Curve.Set(CurveName, RemappedValue);
				}
				else // Blend
				{
					float UseAlpha = FMath::Clamp(Alpha, 0.f, 1.f);
					Output.Curve.Set(CurveName, FMath::Lerp(CurrentValue, CurveValue, UseAlpha));
				}
			}
		}

	}

	if (FaceAnimInstance != nullptr) {

		const FAnimationData AnimData = FaceAnimInstance->GetAnimData();

		if (AnimData.BlendshapeNames.Num() > AnimData.CurrentFrameValues.Num()) {
			return;
		}

		//	Morph target and Material parameter curves
		USkeleton* Skeleton = Output.AnimInstanceProxy->GetSkeleton();

		for (int32 ModIdx = 0; ModIdx < AnimData.BlendshapeNames.Num(); ModIdx++)
		{
			FName CurveName = *AnimData.BlendshapeNames[ModIdx];
			if (Skeleton->GetCurveMetaData(CurveName))
			{
				float CurveValue = AnimData.CurrentFrameValues[ModIdx];
				float CurrentValue = Output.Curve.Get(CurveName);

				// Use ApplyMode enum to decide how to apply
				if (ApplyMode == EModifyCurveApplyModeLocal::Add)
				{
					Output.Curve.Set(CurveName, CurrentValue + CurveValue);
				}
				else if (ApplyMode == EModifyCurveApplyModeLocal::Scale)
				{
					Output.Curve.Set(CurveName, CurrentValue * CurveValue);
				}
				else if (ApplyMode == EModifyCurveApplyModeLocal::RemapCurve)
				{
					const float RemapScale = 1.f / FMath::Max(1.f - CurveValue, 0.01f);
					const float RemappedValue = FMath::Min(FMath::Max(CurrentValue - CurveValue, 0.f) * RemapScale, 1.f);
					Output.Curve.Set(CurveName, RemappedValue);
				}
				else // Blend
				{
					float UseAlpha = FMath::Clamp(Alpha, 0.f, 1.f);
					Output.Curve.Set(CurveName, FMath::Lerp(CurrentValue, CurveValue, UseAlpha));
				}
			}
		}

	}

}

void FModifyCurvesNode::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Update_AnyThread)
	// Run update on input pose nodes
	SourcePose.Update(Context);

	// Evaluate any BP logic plugged into this node
	GetEvaluateGraphExposedInputs().Execute(Context);
}
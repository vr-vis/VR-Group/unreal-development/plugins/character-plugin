// Copyright Epic Games, Inc. All Rights Reserved.

#include "AnimNodes/AnimNode_VHGazing.h"
#include "Animation/AnimInstanceProxy.h"
#include "AnimationRuntime.h"




void FAnimNode_VHGazing::Initialize_AnyThread(const FAnimationInitializeContext& Context) {
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Initialize_AnyThread)
	FAnimNode_Base::Initialize_AnyThread(Context);

	InputPose.Initialize(Context);
}

void FAnimNode_VHGazing::CacheBones_AnyThread(const FAnimationCacheBonesContext& Context)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(CacheBones_AnyThread)
	FAnimNode_Base::CacheBones_AnyThread(Context);

	InputPose.CacheBones(Context);

	const FBoneContainer& RequiredBones = Context.AnimInstanceProxy->GetRequiredBones();
	auto PrivateInitialize = [&](FGazingBoneData& BoneData)
	{
		if (BoneData.TargetBone.BoneName == NAME_None)
		{
			UE_LOG(LogTemp, Error, TEXT("[FAnimNode_VHGazing] You need to specify a targetbone, which should be oriented towards the gaze target"))
		}

		if (BoneData.InfluenceBones.Num() != BoneData.InfluenceStrength.Num())
		{
			UE_LOG(LogTemp, Warning, TEXT("[FAnimNode_VHGazing] A different number of InfluenceBones (#=%d) is given then strength values for those (#=%d), this will not work correctly!"),
				BoneData.InfluenceBones.Num(), BoneData.InfluenceStrength.Num())
		}

		BoneData.CurrentRotation = FRotator::ZeroRotator;
		if (BoneData.TargetBone.BoneName != NAME_None) {
			BoneData.TargetBone.Initialize(RequiredBones);
			BoneData.DefaultTargetBoneTransform = FAnimationRuntime::GetComponentSpaceTransformRefPose(*RefSkel, BoneData.TargetBone.BoneIndex);

		}
		for (auto& Bone : BoneData.InfluenceBones)
		{
			if (Bone.BoneName != NAME_None)
				Bone.Initialize(RequiredBones);
		}

	};

	RefSkel = &RequiredBones.GetReferenceSkeleton();

	PrivateInitialize(LeftEyeBoneData);
	PrivateInitialize(RightEyeBoneData);
	PrivateInitialize(HeadBoneData);
	PrivateInitialize(TorsoBoneData);
}

void FAnimNode_VHGazing::Update_AnyThread(const FAnimationUpdateContext& Context)
{
	InputPose.Update(Context);
	//Context knows, e.g., DeltaTime
	DeltaTime = Context.GetDeltaTime();

	GetEvaluateGraphExposedInputs().Execute(Context);
}


void FAnimNode_VHGazing::Evaluate_AnyThread(FPoseContext& Output)
{
	DECLARE_SCOPE_HIERARCHICAL_COUNTER_ANIMNODE(Evaluate_AnyThread)
	InputPose.Evaluate(Output);
	
	// dublicate the local pose to a component space one, since the computations are done in component space
	FComponentSpacePoseContext CSPoseContext(Output.AnimInstanceProxy);
	CSPoseContext.Pose.InitPose(CopyTemp(Output.Pose));
	CSPoseContext.Curve = CopyTemp(Output.Curve);
	CSPoseContext.CustomAttributes = CopyTemp(Output.CustomAttributes);

	UpdateRotations(Output, CSPoseContext);

}

void FAnimNode_VHGazing::UpdateRotations(FPoseContext& LocalPoseContext, FComponentSpacePoseContext& CSPoseContext) {

	// Update rotations one after another
	// We use data of the last frame to see whether, e.g., the eyes were able to reach their target 
	// or whether, e.g., the head as to help out by also rotating further towards the target


	// *************************
	//   Compute & Apply Torso
	// *************************

	if (VHGazingData.GazeMode == EGazeMode::EyesHeadTorso)
	{
		// Full rotation required to be inline with the target
		FRotator TargetRotationTorso = GetRotationToTarget(TorsoBoneData, LocalPoseContext, CSPoseContext);
		TargetRotationTorso = VHGazingData.TorsoData.Alignment * TargetRotationTorso;

		if (VHGazingData.bSupportClampedRotationByOtherParts) {
			TargetRotationTorso += HeadBoneData.MissingRotationDueToClamp;
		}

		float MinDistanceToClampTorso_Unused;
		TargetRotationTorso = ClampRotation(TargetRotationTorso, VHGazingData.TorsoData, MinDistanceToClampTorso_Unused);

		UpdateCurrentRotation(TorsoBoneData, VHGazingData.TorsoData, TargetRotationTorso);
		ApplyRotation(TorsoBoneData, TorsoBoneData.CurrentRotation, LocalPoseContext, CSPoseContext);
	}


	//now recompute and apply for head and eyes again with updated data (most code is identical!)

	// ****************
	//   Apply Head
	// ****************

	if (VHGazingData.GazeMode != EGazeMode::EyesOnly)
	{
		// Full rotation required to be inline with the target
		FRotator FullTargetRotationHead = GetRotationToTarget(HeadBoneData, LocalPoseContext, CSPoseContext);

		//maybe only use a part of that (as specified by HeadAlignment)
		FRotator TargetRotationHead = VHGazingData.HeadData.Alignment * FullTargetRotationHead;

		//remove the way that the torso already made
		TargetRotationHead -= TorsoBoneData.CurrentRotation;

		if (VHGazingData.bSupportClampedRotationByOtherParts) {
			//add the rotation that the eyes weren't able to make due to them being clamped at the gaze limits 
			TargetRotationHead += 0.5f * (RightEyeBoneData.MissingRotationDueToClamp + LeftEyeBoneData.MissingRotationDueToClamp);
		}

		if (AdditionalHeadRotation != FRotator::ZeroRotator)
		{
			TargetRotationHead += AdditionalHeadRotation;
		}

		//clamp to maximal rotation
		FRotator PreClampedHead = TargetRotationHead;
		float DistanceToClampHead;
		TargetRotationHead = ClampRotation(TargetRotationHead, VHGazingData.HeadData, DistanceToClampHead);
		FRotator MissingHead = PreClampedHead - TargetRotationHead;

		//store what part of the rotation the torso potentially has to take over in the next frame
		if ((DistanceToClampHead > 1.f && MissingHead.IsNearlyZero()) || !MissingHead.IsNearlyZero()) {
			// however, take the mean of this and last frame to avoid jittering
			HeadBoneData.MissingRotationDueToClamp = 0.5f * (HeadBoneData.MissingRotationDueToClamp + MissingHead);
		}
		//else we probably only reached the target because the troso helped, so don't set the missing part to 0 to avoid jitter


		UpdateCurrentRotation(HeadBoneData, VHGazingData.HeadData, TargetRotationHead);
		ApplyRotation(HeadBoneData, HeadBoneData.CurrentRotation, LocalPoseContext, CSPoseContext);
	}


	// ****************
	//   Apply Eyes
	// ****************

	// full rotation required to be inline with the target
	const FRotator FullTargetRotationRightEye = GetRotationToTarget(RightEyeBoneData, LocalPoseContext, CSPoseContext, true);
	const FRotator FullTargetRotationLeftEye = GetRotationToTarget(LeftEyeBoneData, LocalPoseContext, CSPoseContext, true);

	//remove the way that the torso and head already made
	FRotator TargetRotationRightEye = FullTargetRotationRightEye - (HeadBoneData.CurrentRotation + TorsoBoneData.CurrentRotation);
	FRotator TargetRotationLeftEye = FullTargetRotationLeftEye - (HeadBoneData.CurrentRotation + TorsoBoneData.CurrentRotation);

	//add (micro) saccade offset
	TargetRotationRightEye += VHGazingData.AdditionalSaccadeRotation;
	TargetRotationLeftEye += VHGazingData.AdditionalSaccadeRotation;

	if (AdditionalHeadRotation != FRotator::ZeroRotator)
	{
		TargetRotationRightEye -= AdditionalHeadRotation;
		TargetRotationLeftEye -= AdditionalHeadRotation;
	}

	// clamp eye rotation to Ocular Motor Range (OMR), store value before to see whether lower body parts can help in reaching the goal
	const FRotator PreClampedRightEye = TargetRotationRightEye;
	const FRotator PreClampedLeftEye = TargetRotationLeftEye;
	float DistanceToClampLeftEye, DistanceToClampRightEye;
	TargetRotationRightEye = ClampRotation(TargetRotationRightEye, VHGazingData.RightEyeData, DistanceToClampRightEye);
	TargetRotationLeftEye = ClampRotation(TargetRotationLeftEye, VHGazingData.LeftEyeData, DistanceToClampLeftEye);

	FRotator MissingLeft = PreClampedLeftEye - TargetRotationLeftEye;
	FRotator MissingRight = PreClampedRightEye - TargetRotationRightEye;
	// store what head and torso need to take over for next frame. 
	// if this angele is small, we probably only reached the target because the head + torso helped, 
	// so don't set the missing part to 0 directly to avoid jitter
	if ((DistanceToClampRightEye > 1.f && MissingRight.IsNearlyZero()) || !MissingRight.IsNearlyZero()) {
		// however, take the mean of this and last frame to avoid jittering
		RightEyeBoneData.MissingRotationDueToClamp = 0.5 * (RightEyeBoneData.MissingRotationDueToClamp + MissingRight);
	}

	if ((DistanceToClampLeftEye > 1.f && MissingLeft.IsNearlyZero()) || !MissingLeft.IsNearlyZero()) {
		// however, take the mean of this and last frame to avoid jittering
		LeftEyeBoneData.MissingRotationDueToClamp = 0.5 * (LeftEyeBoneData.MissingRotationDueToClamp + MissingLeft);
	}

	//compute the dynamic update to the CurrentRotation
	UpdateCurrentRotation(RightEyeBoneData, VHGazingData.RightEyeData, TargetRotationRightEye);
	UpdateCurrentRotation(LeftEyeBoneData, VHGazingData.LeftEyeData, TargetRotationLeftEye);

	//apply this CurrentRotation
	ApplyRotation(RightEyeBoneData, RightEyeBoneData.CurrentRotation, LocalPoseContext, CSPoseContext);
	ApplyRotation(LeftEyeBoneData, LeftEyeBoneData.CurrentRotation, LocalPoseContext, CSPoseContext);
}

void FAnimNode_VHGazing::UpdateCurrentRotation(FGazingBoneData& GazingBoneData, const FGazingPartData& GazingPartData, const FRotator& TargetRotation) {

	if(!GazingBoneData.bUseBlendshapeOutput && GazingBoneData.InfluenceBones.Num()==0)
	{
		//nothing is applied here, e.g. for torso bone in metahuman face animBP
		GazingBoneData.CurrentRotation = FRotator::ZeroRotator;
		return;
	}

	//for convenience convert to FQuat:
	FQuat CurrRotQuat = GazingBoneData.CurrentRotation.Quaternion();
	FQuat TargetRotQuat = TargetRotation.Quaternion();
	const float Angle = FMath::RadiansToDegrees(CurrRotQuat.AngularDistance(TargetRotQuat));

	//float MinAngle = DeltaTime * GazingPartData.MaxVelocity;
	const float MinAngle = 5.0f; //the above would somewhat make more sense, during testing, however, this magic number performed much better...
	if (Angle < MinAngle && GazingBoneData.AmplitudeCurrMovement == 0.0f)
	{
		// any rotation below MinAngle, is directly applied to avoid jittering etc. with moving targets and especially parallel full-body animations 
		// so if this is only a small correction movement, apply it! 
		GazingBoneData.CurrentRotation = TargetRotation;
		return;
	}


	//we need to keep track how far this movement went to adapt speeds according to https://doi.org/10.1145/2724731
	if (Angle > GazingBoneData.AmplitudeCurrMovement) {
		GazingBoneData.AmplitudeCurrMovement = Angle;
		if (Angle > 10.0f && GazingBoneData.TimeDelay == 0.0f) { //this number (10degree) is just a guess
			//only do Delay for larger movements
			GazingBoneData.TimeDelay = GazingPartData.DelayConstant + GazingPartData.DelayLinear * Angle;
		}
	}

	if (GazingBoneData.TimeDelay > 0.0f) {
		GazingBoneData.TimeDelay -= DeltaTime;
		return; // since we are delaying right now
	}

	float Velocity = 0.0f;
	float FractionMovement = FMath::Clamp(1.0f - Angle / GazingBoneData.AmplitudeCurrMovement, 0.0f, 1.0f); //going from 0.0f at the start of the movement to 1.0f at the end
	if (FractionMovement < 0.5f)
	{
		// following https://doi.org/10.1145/2724731 the start velocity factor is 0.5 (for eye) or 0.25 (for head/trunk) MaxVelocity
		// velocity then increases linearly for half the saccade to MaxVelocity
		Velocity = GazingPartData.MaxVelocity * (GazingPartData.StartVelocityFactor + 2.0f * FractionMovement * (1.0f - GazingPartData.StartVelocityFactor));
		//UE_LOG(LogTemp, Warning, TEXT("Phase 1 Angle: %f of Amplitude: %f    Speed: %f deltaTime: %f alpha before: %f alpha after: %f"), Angle, GazingBoneData.AmplitudeCurrMovement, Velocity, DeltaTime, Angle/ GazingBoneData.AmplitudeCurrMovement, (Angle - Velocity * DeltaTime) / GazingBoneData.AmplitudeCurrMovement)
	}
	else
	{
		// for the second half speed has a ease in / ease out look, which was approximated in https://doi.org/10.1145/2724731
		// by (8r^3 - 18r^2 + 12r - 1.5)*v_max for 0.5<=r<1.0 (but only for eye and slightly different for head/torso since it needs to go down
		// we  tried to aproximate it by quadratic ease in/out
		Velocity = FMath::InterpEaseInOut(GazingPartData.MaxVelocity, GazingPartData.StartVelocityFactor * GazingPartData.MaxVelocity, 2.0f * (FractionMovement - 0.5f), 2.0f);
		//UE_LOG(LogTemp, Warning, TEXT("Phase 2 Angle: %f of Amplitude: %f    Speed: %f deltaTime: %f alpha before: %f alpha after: %f"), Angle, GazingBoneData.AmplitudeCurrMovement, Velocity, DeltaTime, Angle / GazingBoneData.AmplitudeCurrMovement, (Angle - Velocity * DeltaTime) / GazingBoneData.AmplitudeCurrMovement)
	}

	if (Velocity * DeltaTime < Angle)
	{
		GazingBoneData.CurrentRotation = FQuat::Slerp(CurrRotQuat, TargetRotQuat, Velocity * DeltaTime / Angle).Rotator();
	}
	else
	{
		GazingBoneData.CurrentRotation = TargetRotation;
		//reset amplitude so we are ready for the next movement
		GazingBoneData.AmplitudeCurrMovement = 0.0f;
		GazingBoneData.TimeDelay = 0.0f;
	}
}

void FAnimNode_VHGazing::ApplyRotation(FGazingBoneData& GazingBoneData, FRotator Rotation, FPoseContext& PoseContext, FComponentSpacePoseContext& CSPoseContext)
{

	//actually apply the current rotation
	//check whether we do this via blendshapes or bones:
	if(GazingBoneData.bUseBlendshapeOutput)
	{
		//do it via blendshapes:
		USkeleton* Skeleton = PoseContext.AnimInstanceProxy->GetSkeleton();
		auto ApplyBlendshapeRotation = [&](float Angle, float FullAngle, FName PositiveBlendshape, FName NegativeBlendshape)
		{
			float PositiveActivation = FMath::Clamp(Angle / FullAngle, 0.0f, 1.0f);
			float NegativeActivation = FMath::Clamp(-1.0f * Angle / FullAngle, 0.0f, 1.0f);

			PoseContext.Curve.Set(PositiveBlendshape, PositiveActivation);
			PoseContext.Curve.Set(NegativeBlendshape, NegativeActivation);

			//UE_LOG(LogTemp, Warning, TEXT("Angle %f   Pos: %f, Neg: %f"), Angle, PositiveActivation, NegativeActivation);
		};
		ApplyBlendshapeRotation(Rotation.Yaw, GazingBoneData.YawAngleAtFull, GazingBoneData.BlendshapeNameRight, GazingBoneData.BlendshapeNameLeft);
		ApplyBlendshapeRotation(Rotation.Pitch, GazingBoneData.PitchAngleAtFull, GazingBoneData.BlendshapeNameUp, GazingBoneData.BlendshapeNameDown);
	}
	else {
		//do it via bones:
		const FBoneContainer& BoneContainer = PoseContext.Pose.GetBoneContainer();

		Rotation = TransformToAgentForward(Rotation);

		for (int i = 0; i < GazingBoneData.InfluenceBones.Num() && i < GazingBoneData.InfluenceStrength.Num(); i++)
		{
			FCompactPoseBoneIndex BoneIndex = GazingBoneData.InfluenceBones[i].GetCompactPoseIndex(BoneContainer);

			FCompactPoseBoneIndex CSBoneIndexTarget = GazingBoneData.InfluenceBones[i].GetCompactPoseIndex(CSPoseContext.Pose.GetPose().GetBoneContainer());
			const FTransform ComponentSpaceTransform = CSPoseContext.Pose.GetComponentSpaceTransform(CSBoneIndexTarget);

			FRotator PartialRotation = GazingBoneData.InfluenceStrength[i] * Rotation;
			PoseContext.Pose[BoneIndex].SetRotation(PoseContext.Pose[BoneIndex].GetRotation() * ComponentSpaceTransform.GetRotation().Inverse() * PartialRotation.Quaternion() * ComponentSpaceTransform.GetRotation());
			PoseContext.Pose[BoneIndex].NormalizeRotation();
		}
	}
}


FRotator FAnimNode_VHGazing::GetRotationToTarget(FGazingBoneData& GazingBoneData, FPoseContext& LocalPoseContext, FComponentSpacePoseContext& CSPoseContext, bool bEyes /*= false*/)
{

	if (!VHGazingData.bIsGazing || GazingBoneData.TargetBone.BoneName == NAME_None)
	{
		return FRotator::ZeroRotator;
	}

	const FBoneContainer& BoneContainer = LocalPoseContext.Pose.GetBoneContainer();
	FCompactPoseBoneIndex BoneIndexTarget = GazingBoneData.TargetBone.GetCompactPoseIndex(BoneContainer);

	FCompactPoseBoneIndex CSBoneIndexTarget = GazingBoneData.TargetBone.GetCompactPoseIndex(CSPoseContext.Pose.GetPose().GetBoneContainer());
	const FTransform TargetBoneTransformCS = CSPoseContext.Pose.GetComponentSpaceTransform(CSBoneIndexTarget);

	FAnimInstanceProxy* AnimProxy = LocalPoseContext.AnimInstanceProxy;
	check(AnimProxy);

	FVector TargetLocation = VHGazingData.GazeTargetLocation;
	if (bEyes && VHGazingData.EyesOnlyGazeTarget.IsSet()) {
		TargetLocation = VHGazingData.EyesOnlyGazeTarget.GetValue();
	}

	//do everything in component space (so also transform the gaze target into it
	FVector GazeTargeCS = AnimProxy->GetComponentTransform().Inverse().TransformPosition(TargetLocation);
	const FVector BoneLocation = TargetBoneTransformCS.GetLocation();
	FQuat BoneRotation = TargetBoneTransformCS.GetRotation();
	BoneRotation = BoneRotation * GazingBoneData.DefaultTargetBoneTransform.GetRotation().Inverse();

	const FVector CurrentForward = BoneRotation.RotateVector(AgentDefaultForward);
	FVector TargetForward = (GazeTargeCS - BoneLocation).GetSafeNormal();

	const FRotator GazeRotation = GetYawPitchRotationBetween(CurrentForward, TargetForward);
	return GazeRotation;

}

FRotator FAnimNode_VHGazing::GetYawPitchRotationBetween(FVector From, FVector To)
{
	FVector2D FromSpherical = From.UnitCartesianToSpherical();
	FVector2D ToSpherical = To.UnitCartesianToSpherical();
	//Output spherical Theta[0] will be in the range [0, PI], and output Phi[1] will be in the range [-PI, PI].

	float Yaw   = FMath::RadiansToDegrees(ToSpherical[1] - FromSpherical[1]);
	float Pitch = FMath::RadiansToDegrees(ToSpherical[0] - FromSpherical[0]) * -1; //pitch is defined other way around

	Yaw   = FRotator::NormalizeAxis(Yaw);
	Pitch = FRotator::NormalizeAxis(Pitch);

	return FRotator(Pitch, Yaw, 0.0f);
}

FRotator FAnimNode_VHGazing::TransformToAgentForward(FRotator Rotation) const
{
	if (AgentDefaultForward != FVector(0, 1, 0)) {
		UE_LOG(LogTemp, Warning, TEXT("This method currently works only for agents facing in positive Y direction!"))
	}

	return FRotator(Rotation.Roll, Rotation.Yaw, -Rotation.Pitch);
}

FRotator FAnimNode_VHGazing::ClampRotation(FRotator Rotation, const FGazingPartData& GazingPartData, float& MinDistanceToClamp) const {
	FRotator OutRotation = Rotation;

	OutRotation.Yaw = FMath::Clamp(OutRotation.Yaw, -GazingPartData.MaxYaw, GazingPartData.MaxYaw);
	OutRotation.Pitch = FMath::Clamp(OutRotation.Pitch, -GazingPartData.MaxPitch, GazingPartData.MaxPitch);

	//compute min distance to nearest border
	MinDistanceToClamp = FMath::Max(2 * GazingPartData.MaxYaw, 2 * GazingPartData.MaxPitch); //set it to potential maximum
	MinDistanceToClamp = FMath::Min(OutRotation.Yaw - (-GazingPartData.MaxYaw), MinDistanceToClamp);
	MinDistanceToClamp = FMath::Min(GazingPartData.MaxYaw - OutRotation.Yaw, MinDistanceToClamp);
	MinDistanceToClamp = FMath::Min(OutRotation.Pitch - (-GazingPartData.MaxPitch), MinDistanceToClamp);
	MinDistanceToClamp = FMath::Min(GazingPartData.MaxPitch - OutRotation.Pitch, MinDistanceToClamp);

	return OutRotation;
}


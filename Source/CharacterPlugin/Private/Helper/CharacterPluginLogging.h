// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "IUniversalLogging.h"

class CharacterPluginLogging {
public:
  static void InitializeLoggingStreams() {
    //we put the logs in the same file as the defult log stream ""
    ILogStream *LogStream = UniLog.NewLogStream("VHLog", "Saved/Logs",
                                                "UniversalLogging.log");
    LogStream->SetLogToDefaultLog(true);

    ILogStream *WarnStream = UniLog.NewLogStream(
        "VHWarn", "Saved/Logs", "UniversalLogging.log");
    WarnStream->SetLogToDefaultLog(true);
    WarnStream->SetLogOnScreenOnMaster(true);
    WarnStream->SetLogOnScreenOnSlaves(true);
    WarnStream->SetOnScreenColor(FColor::Yellow);
    WarnStream->SetOnScreenBackgroundColor(FColor::Black);
    //WarnStream->SetPrefix("WARNING: "); //we do the prefix in the macro

    ILogStream *ErrorStream = UniLog.NewLogStream(
        "VHError", "Saved/Logs", "UniversalLogging.log");
    ErrorStream->SetLogToDefaultLog(true);
    ErrorStream->SetLogOnScreenOnMaster(true);
    ErrorStream->SetLogOnScreenOnSlaves(true);
    ErrorStream->SetOnScreenColor(FColor::Red);
    ErrorStream->SetOnScreenBackgroundColor(FColor::Black);
    //ErrorStream->SetPrefix("ERROR: ");

		ILogStream *DebugStream = UniLog.NewLogStream(
			"VHDebug", "Saved/Logs", "UniversalLogging.log");
		DebugStream->SetLogToDefaultLog(true);
		DebugStream->SetLogOnScreenOnMaster(true);
		DebugStream->SetLogOnScreenOnSlaves(true);
		DebugStream->SetOnScreenColor(FColor::Green);
  }
};

// this is the blueprint for all logging macros
#define VH_LOGGING_MACRO(LOG_NAME, LOG_TAG, MESSAGE, ...) {FString TempString = FString::Printf(TEXT(MESSAGE) , ##__VA_ARGS__); FString VHName =""; if(GetOwner()!=nullptr)VHName=GetOwner()->GetName(); UniLog.LogF(LOG_NAME, TEXT(LOG_TAG), *VHName, *TempString);}

//these can only be used in ACtorComponents, or other classes the implement GetOwner()
#define VH_LOG(MESSAGE, ...) VH_LOGGING_MACRO("VHLog", "[CharacterPlugin] %s: %s", MESSAGE , ##__VA_ARGS__)
#define VH_WARN(MESSAGE, ...) VH_LOGGING_MACRO("VHWarn", "[CharacterPlugin:WARNING] %s: %s", MESSAGE , ##__VA_ARGS__)
#define VH_ERROR(MESSAGE, ...) VH_LOGGING_MACRO("VHError", "[CharacterPlugin:ERROR] %s: %s", MESSAGE , ##__VA_ARGS__)

//you can use this logging stream for debug logging, it is printed in green on the screen
#define VH_DEBUG(MESSAGE, ...) VH_LOGGING_MACRO("VHDebug", "[CharacterPlugin] %s: %s", MESSAGE , ##__VA_ARGS__)

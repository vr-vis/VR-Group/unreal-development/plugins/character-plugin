// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"



#include "AITypes.h"
#include "VirtualHuman.h"
#include "Waypoint.h"
#include "Components/ActorComponent.h"
#include "Components/SplineComponent.h"

#include "VHMovement.generated.h"



class AVirtualHumanAIController;
UENUM()
enum EWaypointOrdering
{
	Manual		UMETA(DisplayName = "Manual"),
	Random		UMETA(DisplayName = "Random"),
	Closest		UMETA(DisplayName = "Closest"),
};

UENUM()
enum EGroupFormations
{
	Abreast		UMETA(DisplayName = "Abreast"),
	InLine			UMETA(DisplayName = "InLine"),
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHARACTERPLUGIN_API UVHMovement : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVHMovement();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void MoveToWaypoint(FVector TargetLocation, float Speed);
	void MoveToWaypoint();
	void ChildMoveToWaypoint();

	AWaypoint* GetCurrentWaypoint()
	{
		return ACurrentWaypoint;
	}

	bool IsLoop()
	{
		return bLoop;
	}

	int GetWaypointGroup() 
	{
		return WaypointGroup;
	}

	bool IsChild()
	{
		return bChild;
	}

	bool ShouldChildMove() {
		return bChildMovement;
	}

	bool IsLineMovement() 
	{
		return bLineMovement;
	}

	// should be called automatically when the groupID is changed
	bool StoreAllWaypoints();

	UCharacterMovementComponent* GetCharacterMovementComponent() const;

	/*
	 * Specify to which WaypointGroup this VA belongs to.
	 * VAs will walk to waypoints, that are in the same WaypointGroup
	 */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = WaypointMovement)
		int WaypointGroup = 0;

	UPROPERTY(BlueprintReadWrite, Category = IKFoot)
		float IKInterpSpeed;

	UPROPERTY(BlueprintReadOnly, Category = IKFoot)
		float IKOffsetRigthFoot;

	UPROPERTY(BlueprintReadOnly, Category = IKFoot)
		float IKOffsetLeftFoot;

	UPROPERTY(BlueprintReadWrite, Category = IKFoot)
		float IKMaxOffset;

	UPROPERTY(BlueprintReadOnly, Category = IKFoot)
		float IKHipOffset;

	UPROPERTY(BlueprintReadWrite, Category = IKFoot)
		float HipDisplacementFactor;

	UPROPERTY(BlueprintReadWrite, Category = IKFoot)
		FName RightFootSocket;

	UPROPERTY(BlueprintReadWrite, Category = IKFoot)
		FName LeftFootSocket;
	UPROPERTY(BlueprintReadWrite, Category = IKFoot)
		float IKFootRotationAngle;

	FAIRequestID CurrentRequestID;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	TArray<AWaypoint*> ShuffleArray(TArray<AWaypoint*> myArray);
	float OffsetFromWaypoint;
	TArray<AWaypoint*> Waypoints;
	TMap<AWaypoint*, TArray<FVector>> ChildPoints;

	const float MAX_WALKING_SPEED = 350.0f;

	bool bChild;
	bool bChildMovement;
	bool bLineMovement;

	APawn* AVirtualHumanPawn;
	AVirtualHuman* parentVH;
	AVirtualHuman* AVirtualHumanPtr;
	AVirtualHumanAIController* VirtualHumanAIController;

	AWaypoint* ACurrentWaypoint;

	UNavigationPath* NavigationPath;

	float IKTraceDistance;
	bool bWalkingUp;
	FVector HitLocation;
	UVHAnimInstance* AnimationInstance;

	FRandomStream MovementRandomStream;

	FHitResult IKFootTrace(FName Socket, float TraceDistance);

	void CreateChildPoints();
	TArray<FVector> CreateIntermediatePoints(AWaypoint* last, AWaypoint* current, AWaypoint* next);
	float CalculateChildSpeed(AWaypoint* nextWaypoint, TArray<FVector> childPoints);
	float CalculateChildSpeed(AWaypoint* nextWaypoint, FVector childPoint);

	int IntermediatePointIterator;

	UPROPERTY(meta = (AllowPrivateAccess = "true"))
		int CurrentWaypointIterator;

	// loop to the first waypoint after reaching the last
	UPROPERTY(EditAnywhere, Category = WaypointMovement)
		bool bLoop = false;

	/*
	 *	Specify in which order the Waypoints will be processed
	 */
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"), Category = WaypointMovement)
		TEnumAsByte<EWaypointOrdering> WaypointOrder;

	//Key: Virtual Human that should be added as child
	//Value: distance from parent. Positive values correspond to left
	UPROPERTY(EditAnywhere, Category = GroupMovement)
		TMap<AVirtualHuman*, float> ChildVH;

	UPROPERTY(EditAnywhere, Category = GroupMovement)
		TEnumAsByte<EGroupFormations> GroupFormation;

	UPROPERTY(EditAnywhere, Category = GroupMovement)
		bool bDrawDebugSpheres;

	UFUNCTION(BlueprintCallable)
		void WaypointMovement();

};

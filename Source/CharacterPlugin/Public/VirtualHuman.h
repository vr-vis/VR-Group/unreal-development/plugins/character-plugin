// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Engine/StreamableManager.h"
#include "Components/SceneComponent.h"
#include "Components/SkeletalMeshComponent.h"

#include "VirtualHuman.generated.h"

UENUM()
enum class EBodyType : uint8
{
	CC3 = 0 UMETA(DisplayName = "Character Creator 3"),
	MetaHuman = 1 UMETA(DisplayName = "Meta Human")
};

UENUM()
enum class EVHGender : uint8
{
	Male			UMETA(DisplayName = "Male"),
	Female			UMETA(DisplayName = "Female"),
	Divers			UMETA(DisplayName = "Divers"),
};

struct BoneNames {

	FName boneroot;
	FName spine01;
	FName spine02;
	FName spine03;
	FName spine04;
	FName spine05;

	FName neck;
	FName neck02;
	FName head;
	FName eye_r;
	FName eye_l;
	FName jaw;
	FName teeth_lower;
	FName mouth_lower;

	FName clavicle_r;
	FName clavicle_l;
	FName upperarm_r;
	FName upperarm_l;
	FName lowerarm_r;
	FName lowerarm_l;
	FName hand_r;
	FName hand_l;
	
	FName pelvis;
	FName calf_r;
	FName calf_l;
	FName thigh_r;
	FName thigh_l;
	FName foot_r;
	FName foot_l;
	FName foot_socket_r;
	FName foot_socket_l;

};

class UVHAnimInstance;
UCLASS()
class CHARACTERPLUGIN_API AVirtualHuman : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AVirtualHuman();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
	virtual USkeletalMeshComponent* GetBodyMesh();
	UFUNCTION(BlueprintCallable)
	virtual USkeletalMeshComponent* GetFaceMesh();
	UFUNCTION(BlueprintCallable)
	virtual UVHAnimInstance* GetBodyAnimInstance();
	UFUNCTION(BlueprintCallable)
	virtual UVHAnimInstance* GetFaceAnimInstance();
	virtual const BoneNames& GetBoneNames();
	UFUNCTION(BlueprintCallable)
	virtual EBodyType GetBodyType();
	UFUNCTION(BlueprintCallable)
	virtual EVHGender GetGender();
	virtual bool SupportsMorphs();
	
	/**
	 * Forces a new IDLE animation to be played, which is randomly chosen from "CustomIdleAnimations".
	 * Note that the new animation might also be the one which is currently playing.
	 * This method is useful, e.g. if the "CustomIdleAnimations" change.
	 * @return true, if a animation was select (might be false, if "CustomIdleAnimations" is empty)
	 */
	UFUNCTION(BlueprintCallable)
	bool ForcePlayNewIdleAnimation();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	EBodyType BodyType;

	UPROPERTY(EditAnywhere)
	EVHGender Gender;

	UPROPERTY(EditAnywhere)
	TSubclassOf<UVHAnimInstance> BodyAnimInstanceToUse;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "BodyType==BodyType::MetaHuman"))
	TSubclassOf<UVHAnimInstance> FaceAnimInstanceToUse;

	//custom idle animations can be specified to be used (played in the DefaultSlot)
	//CAUTION: this currently does not work together with walking or pointing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VirtualHuman|Custom Idle Animations")
	TArray<UAnimSequence*> CustomIdleAnimations;
	//The minimal percentage of the animation that is allowed to be skipped to blend into another animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VirtualHuman|Custom Idle Animations")
	float MinBlendTime = 1.0;

private:

	BoneNames BoneNames;

	bool bFirstTick = true;

	TSubclassOf<UVHAnimInstance> MetaHumanBodyAnimInstanceClass;
	TSubclassOf<UVHAnimInstance> MetaHumanFaceAnimInstanceClass;
	TSubclassOf<UVHAnimInstance> CC3AnimInstanceClass;

	virtual void SetUpBodyType();

	void PlayRandomIdleAnimation();
	UPROPERTY()
	FTimerHandle NextIdleTimerHandle;
	FRandomStream IdleRandomStream;

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
	virtual void OnConstruction(const FTransform& Transform) override;
	
};
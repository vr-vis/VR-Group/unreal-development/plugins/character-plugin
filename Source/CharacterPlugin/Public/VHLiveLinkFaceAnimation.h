// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Helper/VHFaceAnimation.h"
#include "Animation/PoseAsset.h"
#include "VHAnimInstance.h"

#include "VHLiveLinkFaceAnimation.generated.h"

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CHARACTERPLUGIN_API UVHLiveLinkAnimation : public UVHFaceAnimation {
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPoseAsset* Mapping = nullptr;

private:

	virtual bool InternalLoadFile(FString FileName) override;

  float ConvertTimecodeToSeconds(FString Timecode);
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/Character.h"
#include "Waypoint.generated.h"

UENUM()
enum EOnWaypointReach
{
	Stay		UMETA(DisplayName = "Stationary Agent"),
	Destroy		UMETA(DisplayName = "Destroy Agent"),
};

/**
 *
 */
UCLASS()
class CHARACTERPLUGIN_API AWaypoint : public AStaticMeshActor
{
	GENERATED_BODY()
public:
	AWaypoint();
	int GetWaypointOrder();
	float GetWalkingSpeed();
	int GetWaypointGroup();
	int GetLastWaypointCommand();
	
private:
	
	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
		int WaypointOrder;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true", ClampMin = "0.0", ClampMax = "350.0", UIMin = "0.0", UIMax = "350.0"))
		float WalkingSpeed;
	
	/*
	 * Specify to which VA the waypoint belongs to.
	 * This waypoint belongs to all VAs, that are in the same WaypointGroup.
	 */
	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		int WaypointGroup;

	/*
	* What should happen with the agent at the last waypoint?
	* Value set uneffective if there are still waypoints in the queue.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
		TEnumAsByte<EOnWaypointReach> OnReachLastWaypoint = EOnWaypointReach::Destroy;


};

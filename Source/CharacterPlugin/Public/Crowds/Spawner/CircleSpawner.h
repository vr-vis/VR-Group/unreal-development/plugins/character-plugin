// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Crowds/Spawner/AgentSpawner.h"
#include "CircleSpawner.generated.h"

/**
 * Spawn agents in a circle formation
 */
UCLASS()
class CHARACTERPLUGIN_API ACircleSpawner : public AAgentSpawner
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpawnCircle)
		float SpawnRadius = 250.0f;

	void BeginPlay() override;

	void Spawn() override;

};

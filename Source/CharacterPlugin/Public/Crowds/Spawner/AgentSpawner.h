// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Crowds/VHsManager.h"
#include "CoreMinimal.h"
#include "Engine/TargetPoint.h"
#include "AgentSpawner.generated.h"


/**
 * Base class for an agent spawner
 */
UCLASS(Abstract)
class CHARACTERPLUGIN_API AAgentSpawner : public ATargetPoint
{
	GENERATED_BODY()

private:
	FTimerHandle SpawnTimerHandle;

public:
	void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	virtual void Spawn();

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/**Time deltas for respawning in seconds; 0 means just spawning once. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnGroup")
		float RespawnRate = 0.0f;

	/**How many agents should be spawned per trigger? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnGroup")
		int GroupSize = 10;

	/** Which waypoing group is associated with this spawner? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SpawnGroup")
		int WaypointGroup;

	/** With which manager is the spawner associated? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(UseComponentPicker), Category = "SpawnGroup")
		AVHsManager* VHsManager;

};

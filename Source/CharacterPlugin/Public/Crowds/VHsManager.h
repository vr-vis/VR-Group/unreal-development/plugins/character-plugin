// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VirtualHuman.h"
#include "Crowds/VelocityPostProcessorComponent.h"
#include "VHsManager.generated.h"

/**
*	This class manages a set of "manually" registered VHs.
*   If a spawner is associated with an instance of this class, 
*	all spawnd entites are automatically registered.
*	The entities of all VHsManagers of a scene are taken into
*	for crowd simulation!
**/
UCLASS()
class CHARACTERPLUGIN_API AVHsManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AVHsManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	AVirtualHuman* SpawnVH(FVector Position, bool IgnoreCollisions = false);

	void ClearAllVHs();

	void Add(AVirtualHuman * agent);

	void Remove(AVirtualHuman * agent);

	bool HasAgent(AVirtualHuman * Agent);

	UFUNCTION(BlueprintCallable)
	void SetCrowdAlgorithm(TSubclassOf<UVelocityPostProcessorComponent> NewCrowdAlgorithm);

	TArray<AVirtualHuman*> GetNeighbours(AVirtualHuman* VH, const float MaxDistance);

	TArray<AVirtualHuman*> GetAllVHs() const;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="VHManager")
	TSubclassOf<UVelocityPostProcessorComponent> CrowdAlgorithm;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="VHManager")
	TSubclassOf<AVirtualHuman> AgentClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "VHManager")
	bool DebugDraw = false;
	
private:

	void SetCrowdAlgorithmFor(AVirtualHuman* VH);

	TArray<AVirtualHuman*> AllVHs;

};

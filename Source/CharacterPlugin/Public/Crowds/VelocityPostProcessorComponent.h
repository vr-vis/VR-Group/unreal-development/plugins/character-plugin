// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Runtime/AIModule/Classes/AIController.h"

#include "VelocityPostProcessorComponent.generated.h"


/**
*	Base Class to adapt the velocity-vector per agent if required for a crowd simulation
**/
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CHARACTERPLUGIN_API UVelocityPostProcessorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVelocityPostProcessorComponent();

	virtual void PostProcessVelocity(float LastDeltaTime, FVector& Velocity);

	// 140cm/s is the average of comfortable gait speeds for women and men in their 20s to 50s
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VelocityPostProcessor")
		float WALK_SPEED = 140; // cm/s

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VelocityPostProcessor")
		float NEIGHBOUR_DIST_LIMIT_SQUARE = 1000 * 1000; // cm
};

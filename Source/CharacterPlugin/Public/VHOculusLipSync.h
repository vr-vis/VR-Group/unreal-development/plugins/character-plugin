// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/PoseAsset.h"
#include "Helper/VHFaceAnimation.h"
#include "VHOculusLipSync.generated.h"

// viseme file to read in for the lipsyncing, should be generated using the OculusLipSyncWAVParser project
// (https://devhub.vr.rwth-aachen.de/VR-Group/oculuslipsyncwavparser)
// also see: https://devhub.vr.rwth-aachen.de/VR-Group/unreal-development/character-plugin/-/wikis/Components/FaceAnimation

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHARACTERPLUGIN_API UVHOculusLipSync : public UVHFaceAnimation
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPoseAsset* Mapping = nullptr;

private:

	virtual bool InternalLoadFile(FString FileName) override;

	float Framerate = 60.0f;
};

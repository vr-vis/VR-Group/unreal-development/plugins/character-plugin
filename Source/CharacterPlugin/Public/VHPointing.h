// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VHAnimInstance.h"
#include "VHPointing.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHARACTERPLUGIN_API UVHPointing : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVHPointing();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UVHAnimInstance* VHAnimInstance;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsPointing;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector PointingTarget;
	// Angle of back area at which pointing stops in degrees.
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BlindAngle = 30.0;
};

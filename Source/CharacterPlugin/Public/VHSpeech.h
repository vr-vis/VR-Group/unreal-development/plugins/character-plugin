// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "VirtualHuman.h"
#include "VirtualHumanAIController.h"
#include "Components/ActorComponent.h"
#include "Helper/VHFaceAnimation.h"
#include "SoundSource/VAAudiofileSourceComponent.h"


#include "VHSpeech.generated.h"

USTRUCT(BlueprintType)
struct FDialogueUtterance
{
	GENERATED_BODY()

	// animation file for the face, this can be dependent on which UVHFaceAnimation-derived class is present on the agents
	// - a .csv file for VHLiveLinkFaceAnimation
	// - a .txt file for VHOculusLipSync
	// so this is d
	UPROPERTY(EditAnywhere)
	FString FaceAnimFile;
	// file path relative to the Content folder
	UPROPERTY(EditAnywhere)
	FString Audiofile;

	// to specify of which gender the agent should be, this dialogue is assigned to
	UPROPERTY(EditAnywhere)
	EVHGender Gender = EVHGender::Divers;

	//optional: the content/text of the utterance
	UPROPERTY(EditAnywhere)
	FString Text;

	//optional: an upper-body animation to be played, if not specified a random one is picked
	UPROPERTY(EditAnywhere)
	UAnimSequence* BodyAnim = nullptr;
};

UCLASS(ClassGroup = (Custom), HideCategories = (Variable, Tags, ComponentTick, Collision, Activation, Cooking, AssetUserData), meta = (BlueprintSpawnableComponent))
class CHARACTERPLUGIN_API UVHSpeech : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVHSpeech();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction) override;

	// Play multiple dialogues after one another
	// MinDelay: minimal delay between each dialogue
	// MaxDelay: maximal delay between each dialogue
	UFUNCTION(BlueprintCallable)
	void PlayDialogueArray(TArray<FDialogueUtterance> Dialogues, float MinDelay, float MaxDelay);

	// Play dialogue
	// Delay: initial delay before dialogue starts
	UFUNCTION(BlueprintCallable)
	void PlayDialogue(FDialogueUtterance Dialogues, float Delay);

	UFUNCTION(BlueprintCallable)
	void StopDialogue();

	UFUNCTION(BlueprintCallable)
	void PauseDialogue();

	//On Dialogue Complete delegate: is broadcasted when PlayDialogue (either for an array or a single FDialogueUtterance) is done
	DECLARE_MULTICAST_DELEGATE(OnDialogueCompletedDelegate);
	OnDialogueCompletedDelegate DialogueCompletedDelegate;

	UFUNCTION(BlueprintCallable)
	void PreloadUtterance(FDialogueUtterance Utterance);

	UFUNCTION(BlueprintCallable)
	float GetCurrentUtteranceLength() const;

	UFUNCTION(BlueprintCallable)
	UVAAudiofileSourceComponent* GetAudioComponent();

	UFUNCTION(BlueprintCallable)
	void SetAudioComponent(UVAAudiofileSourceComponent* AudioSourceComponent);


	UFUNCTION(BlueprintCallable)
	UVHFaceAnimation* GetFaceAnimComponent() const;

	// if this is set to true either the animation provided by the FDialogueUtterance is played
	// or if none is provided (nullptr) a random one is played. If this is false no animation is triggered.
	UPROPERTY(EditAnywhere)
	bool bUseSpeechBodyAnimation = true;

	// if bPlaySpeechAnimationOnWholeBody is true the DefaultSlot (whole body) is used to play the animation
	// otherwise (default) only the UpperBodySlot is used
	UPROPERTY(EditAnywhere)
	bool bPlaySpeechAnimationOnWholeBody = false;

	// the time used for blending in and out of the BodyAnim
	UPROPERTY(EditAnywhere)
	float AnimationBlendTime = 0.25f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;



private:
	UPROPERTY()
	AVirtualHuman* VH;
	UPROPERTY()
	UVAAudiofileSourceComponent* AudioComp;
	UPROPERTY()
	UVHFaceAnimation* FaceAnimComp;

	TArray<FDialogueUtterance> DialogueUtterances;
	float MinD;
	float MaxD;


	UPROPERTY()
	UAnimMontage* SpeakingMontage;

	TArray<UAnimSequence*> SpeakingAnimations;
	void PlayRandomSpeakingAnimation();
	FTimerHandle SpeakingMontageTimer;
	FTimerHandle SpeakerTimerHandle;
	void StartSpeakingMontageTimer(float MontageLength);

	FTimerHandle AudioFinishTimer;

	FRandomStream SpeechRandomStream;

	UFUNCTION()
	void OnAudioFinished();

	void OnDialogueCompleted();

	int DialogueIt;

	bool bIsSpeaking;
	bool bAudioCanFinish;
	bool bHasBeenInitialized = false;

	UFUNCTION()
	void NextDialogue();

	void StartNextDialogueTimer();

	UPROPERTY()
	FTimerHandle NextDialogHandle;

	bool Init();

	TArray<UAnimSequence*> LoadDefaultAnimations();

	void StopMontages() const;
};

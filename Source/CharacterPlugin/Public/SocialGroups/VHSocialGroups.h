// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VHAnimInstance.h"
#include "VHGazing.h"
#include "VirtualHuman.h"
#include "VirtualHumanAIController.h"
#include "Components/ActorComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "SoundSource/VAAudiofileSourceComponent.h"

#include "VHSocialGroups.generated.h"
class ASocialGroup;

UENUM()
enum EGazeStates
{
	GazeAtSpeaker		UMETA(DisplayName = "Gaze at Speaker"),
	GazeAtRandom		UMETA(DisplayName = "Random Gazing"),	
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CHARACTERPLUGIN_API UVHSocialGroups : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVHSocialGroups();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY()
		TEnumAsByte<EGazeStates> GazeState;

	UFUNCTION(BlueprintCallable)
		void LeaveGroup();

	UFUNCTION(BlueprintCallable)
		void JoinGroup(ASocialGroup* SG);
	
	UFUNCTION()
		void RandomGazing();

	UFUNCTION()
		void OnAudioFinished();

	void SetIsSpeaker(const bool bSpeaker)
	{
		bIsSpeaker = bSpeaker;
	}
	bool GetIsSpeaker() const
	{
		return bIsSpeaker;
	}

	ASocialGroup* GetSocialGroup()
	{
		return SocialGroup;
	}

	void SetSocialGroup(ASocialGroup* SG)
	{
		SocialGroup = SG;
	}

	UVHAnimInstance* GetAnimationInstance() const
	{
		return AnimationInstance;
	}
	AVirtualHumanAIController* GetAIController() const
	{
		return AIController;
	}

	bool InitializeParameters(ASocialGroup* SetSocialGroup);

	void VHMoveToLocation(FVector location, float speed);
	void SetGazingTimer();
	void EndGazing();
	
	bool bAudioCanFinish;
	bool bGazeToUser;
	
	void StartGazeToUserTimer();
	FTimerHandle GazeToUserTimer;
	void GazeToUser();
	void ClearGazeToUserTimer();
private:

	APawn* VHPawn;
	AVirtualHumanAIController* AIController;
	UCharacterMovementComponent* GetCharacterMovementComponent();

	TArray<FVector> PositionsOnCircle;
	UPROPERTY()
		AVirtualHuman* AVirtualHumanPtr;
	UPROPERTY()
		UVHAnimInstance* AnimationInstance;
	FTimerHandle GazingTimerHandle;
	UPROPERTY()
		ASocialGroup* SocialGroup;
	UPROPERTY()
		UVAAudiofileSourceComponent* AudioComp;
	UPROPERTY()
		UVHGazing* GazingComp;

	FRandomStream SocialGroupsRandomStream;

	FAIRequestID CurrentRequestID;

	FVector GetProximityForce();
	FVector GetCircleForce();
	bool bIsSpeaker;
	bool Init();

	float CurrentYaw;

};




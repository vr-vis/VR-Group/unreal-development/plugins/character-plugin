// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SocialGroups/SocialGroupState.h"
#include "UObject/NoExportTypes.h"
#include "SG_Gazing.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERPLUGIN_API USG_Gazing : public UObject, public ISocialGroupState
{
	GENERATED_BODY()

public:
	virtual void Tick(ASocialGroup* SG) override;
	virtual void ToInGroup(ASocialGroup* SG) override;
	virtual void ToIdle(ASocialGroup* SG) override;
	static void LookAtUserOnJoin(ASocialGroup* SG);

private:
	FRandomStream SGGazingRandomStream;
};

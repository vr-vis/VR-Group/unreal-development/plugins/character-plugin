// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SocialGroups/SocialGroupState.h"
#include "UObject/NoExportTypes.h"
#include "SG_Leaving.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERPLUGIN_API USG_Leaving : public UObject, public ISocialGroupState
{
	GENERATED_BODY()

public:
	virtual void ToIdle(ASocialGroup* SG) override;
	
};

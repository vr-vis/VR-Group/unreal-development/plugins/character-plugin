// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SocialGroups/SocialGroupState.h"
#include "UObject/NoExportTypes.h"
#include "SG_InGroup.generated.h"

/**
 * 
 */
UCLASS()
class CHARACTERPLUGIN_API USG_InGroup : public UObject, public ISocialGroupState
{
	GENERATED_BODY()
	
public:
	virtual void ToLeaving(ASocialGroup* SG) override;
};

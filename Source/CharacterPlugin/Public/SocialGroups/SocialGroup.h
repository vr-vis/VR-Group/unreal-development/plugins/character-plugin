// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <VirtualHumanAIController.h>
#include <Runtime/AIModule/Classes/Navigation/PathFollowingComponent.h>

#include "VHGazing.h"
#include "VHSocialGroups.h"
#include "VHSpeech.h"
#include "VirtualHuman.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Pawn/RWTHVRPawn.h"

#include "SocialGroup.generated.h"
class USG_InGroup;
class USG_Leaving;
class USG_Gazing;
class USG_Idle;
class ISocialGroupState;
class UVHSocialGroups;

USTRUCT(BlueprintType)
struct FSocialConstraints
{

	GENERATED_BODY()

	UPROPERTY()
		float MinimalDistance = 110.0f;

	UPROPERTY()
		float IntimateDistance = 45.0f;

	UPROPERTY()
		float PersonalDistance = 120.0f;

	UPROPERTY()
		float SocialDistance = 360.0f;

	UPROPERTY()
		float PublicDistance = 760.0f;

};

USTRUCT()
struct FAgentDialogueUtterance
{
	GENERATED_BODY()

	UPROPERTY()
		FDialogueUtterance Utterance;

	UPROPERTY()
		AVirtualHuman* VH;
};

UCLASS()
class CHARACTERPLUGIN_API ASocialGroup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASocialGroup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	UPROPERTY(EditAnywhere, Category = "SocialGroup", meta= (EditCondition = "!bSpawnAsGroup"))
		TArray<AVirtualHuman*> AgentsInGroup;

	// If true, GroupSize determines how many agents will spawn at start
	UPROPERTY(EditAnywhere, Category = "SocialGroup")
		bool bSpawnAsGroup;

	// Only editable if spawn VH on start is true
	UPROPERTY(EditAnywhere, Category = "SocialGroup", meta = (EditCondition = "bSpawnAsGroup"))
		int GroupSize;

	// Types of Virtual Human that should be spawned
	UPROPERTY(EditAnywhere, Category = "SocialGroup", meta = (EditCondition = "bSpawnAsGroup"))
		TArray<TSubclassOf<AVirtualHuman>> VHTypes;

	
	// Minimal Distance VAs keep to each other and the user
	UPROPERTY(EditAnywhere, Category = "SocialGroup")
		float ConversationDistance = 140.0f;
	// Distance at which the user is still recognizable
	UPROPERTY(EditAnywhere, Category = "SocialGroup")
		float GazeDistance = 220.0f;
	// Variance in which the user can move before a leave is detected
	UPROPERTY(EditAnywhere, Category = "SocialGroup")
		float StillStandingVariance = 100.0f;
	// Distance at which a join can be detected
	UPROPERTY(EditAnywhere, Category = "SocialGroup")
		float JoinDistance = 100.0f;

	// If the users orientation to the group is below this angle, he is classified as oriented towards the group
	UPROPERTY(EditAnywhere, Category = "SocialGroup")
		float ToGroupThreshold = 30.0f;


	// dialogue setup:
	
	
	UPROPERTY(EditAnywhere, Category = "SocialGroup|Dialogue")
		bool bStartDialogue;

	// contains data for speaking like sound file, face animation data and start of face animation
	UPROPERTY(EditAnywhere, Category = "SocialGroup|Dialogue")
		TArray<FDialogueUtterance> Dialogue;
	// on how many agents will the dialogue be distributed
	UPROPERTY(EditAnywhere, Category = "SocialGroup|Dialogue")
		int NumberOfAgentsInConversation;

	UPROPERTY(EditAnywhere, Category = "SocialGroup|Dialogue")
		bool bLoopDialogue;
	

	UFUNCTION(BlueprintCallable)
		void LeaveGroup(AVirtualHuman* VH);

	UFUNCTION(BlueprintCallable)
		void JoinGroup(AVirtualHuman* VH);

	UFUNCTION()
	AActor* GetSpeaker();
	
	UVHSocialGroups* GetSocialGroupsComponent(AVirtualHuman* VH) const;
	UVHGazing* GetGazingComponent(AVirtualHuman* VH) const;
	UVHSpeech* GetSpeechComponent(AVirtualHuman* VH) const;
	TArray<FVector> GetCirclePositions(int NumberOfAgents);
	
	FVector TrackedPawnLocation;

	UPROPERTY()
		APawn* AgentPawn;

	UPROPERTY()
		ARWTHVRPawn* VRPawn;

	void ChangeState(TScriptInterface<ISocialGroupState> NextState);

	// Dialogues
	int DialogueIterator;

	// Start dialogue after the initial delay.
	// Requires VHSpeech, VHFaceAnimation(like VHLiveLinkFaceAnimation) and VAAudiofileSourceComponent.
	UFUNCTION(BlueprintCallable)
		bool StartDialogue(float InitialDelay);

	UFUNCTION(BlueprintCallable)
		void StopDialogue();

	void NextDialogue();
	void PauseDialogue(float Duration);

	//preloads the dialogue. However, since audiofiles are loaded for all agents this can take some time
	UFUNCTION(BlueprintCallable)
		void PreloadDialogue();
	
	float GroupRadius;
	bool bPlayerIsInGroup;


	void SortByAngleToPlayer();
	void SortByDistanceToActor(FVector ActorLocation);
	bool bUser;
	float GetGroupRadius(int NumberOfAgents) const;
	void RestartDialogue(float InitialDelay);
	void StartRandomGazingTimer(float FirstDelay);
	void StartRandomGazing();
	
	// Animation Montages
	UPROPERTY()
		TArray<UAnimMontage*> WavingMontages;
	
	// States
	UPROPERTY()
		USG_Idle* Idle;
	UPROPERTY()
		USG_Gazing* Gazing;
	UPROPERTY()
		USG_Leaving* Leaving;
	UPROPERTY()
		USG_InGroup* InGroup;
	UPROPERTY()
		TArray<TScriptInterface<ISocialGroupState>> GroupStates;
	
private:

	TArray<AVirtualHuman*> SpawnInGroupFormation();

	FTimerHandle SpeakerTimerHandle;
	void SetSpeaker();
	void ClearSpeakerTimer();

	FRandomStream SocialGroupRandomStream;

	FTimerHandle RandomGazingTimer;

	UFUNCTION()
		void OnSphereBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	bool SetupDialogue();
	UPROPERTY()
		TArray<FAgentDialogueUtterance> AgentsInConversation;
	
	static TArray<AVirtualHuman*> ShuffleArray(TArray<AVirtualHuman*> Agents);

	bool bAgentIsJoining;
	UPROPERTY()
		AVirtualHuman* JoiningAgent;
	UPROPERTY()
		USphereComponent* CollisionSphere;
	UPROPERTY()
		TArray<AVirtualHuman*> CloseAgents;

	TArray<UAnimMontage*> LoadAnimationMontages() const;
	int TrackedAgentIt;
	bool IsUserOrientedToGroup(float Threshold);

	void DestroyAgent();

};


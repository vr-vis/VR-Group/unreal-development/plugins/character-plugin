// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SocialGroups/SocialGroup.h"
#include "UObject/Interface.h"
#include "SocialGroupState.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USocialGroupState : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class CHARACTERPLUGIN_API ISocialGroupState
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void Tick(ASocialGroup* SG);
	virtual void ToIdle(ASocialGroup* SG);
	virtual void ToGazing(ASocialGroup* SG);
	virtual void ToInGroup(ASocialGroup* SG);
	virtual void ToLeaving(ASocialGroup* SG);
};

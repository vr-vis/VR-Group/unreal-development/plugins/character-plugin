// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "AnimNodes/AnimNode_VHGazing.h"
#include "VHAnimInstance.generated.h"

//class VHFaceAnimation;

UENUM()
enum EPointingHand
{
    HandNone = 0 UMETA(DisplayName = "None"),
    HandLeft = 1 UMETA(DisplayName = "Left Hand"),
    HandRight = 2 UMETA(DisplayName = "Right Hand")
};

UCLASS()
class CHARACTERPLUGIN_API UVHAnimInstance : public UAnimInstance {
  GENERATED_UCLASS_BODY()

public:

	// All of these rotations are initially 0
	// they are all applied in component space (so relative to the actor's rotation)
	// our models by default point in positive-y, so for example roll would be nodding
	// and yaw would be shaking the head
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FRotator SkelControl_Head = FRotator(0.0f, 0.0f, 0.0f);
	
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator SkelControl_LeftEyeRot = FRotator(0.0f, 0.0f, 0.0f);

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator SkelControl_RightEyeRot = FRotator(0.0f, 0.0f, 0.0f);

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator SkelControl_JawRot = FRotator(0.0f, 0.0f, 0.0f);

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVHGazingData VHGazingData;


	// IK Foot System
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float IKOffsetRight;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float IKOffsetLeft;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float IKHipOffset;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float IKFootRotation;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
        TEnumAsByte<EPointingHand> PointingUseHand = EPointingHand::HandNone;

    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FVector EffectorPosition;

	// value for the blink blendshapes (0 open eyes, 1 eyes closed)
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float EyeBlink=0;

    // this value is 1.0f if DefaultSlot is playing an animation and 0.001f otherwise
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
      float IsDefaultSlotPlayingAnimation = 0.001f;

    // this value is 1.0f if CustomIdleSlot is playing an animation and 0.001f otherwise
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
      float IsCustomIdleSlotPlayingAnimation = 0.001f;

};

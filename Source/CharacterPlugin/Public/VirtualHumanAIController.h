// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Crowds/VHsManager.h"
#include "VirtualHumanAIController.generated.h"

/**
 *
 */
UCLASS()
class CHARACTERPLUGIN_API AVirtualHumanAIController : public AAIController
{
	GENERATED_BODY()

public:
	void DestroyAgent();

	void BeginPlay() override;

	void Tick(float DeltaSeconds) override;

	void PostProcessMovement(UPathFollowingComponent* Comp, FVector& Velocity);

	void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

private:

	TArray<AVHsManager*> GetAssociatedVHsManagers();

	float LastDeltaTime = 0.0f;
};
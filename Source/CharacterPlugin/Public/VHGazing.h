// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "VirtualHumanAIController.h"
#include "Components/ActorComponent.h"
#include "AnimNodes/AnimNode_VHGazing.h"

#include "VHGazing.generated.h"

UENUM()
enum class EBlinkState : uint8
{
	Open,
	Closing,
	Opening
};

UENUM()
enum class ESaccadeMode : uint8
{
	Listening,
	Speaking,
	Thinking,
	Fixating
};

USTRUCT()
struct FSaccadeData
{
	GENERATED_BODY()
	float MutualGazeMean;
	float MutualGazeSD;
	float AvertedGazeMean;
	float AvertedGazeSD;
	float PercentageMutual; //% of the time mutually gazing
	float PercentageMutualNumber; //% of the saccades being mutual (computed in SetupSaccadeData)
	//holds for 0, 45, 90, ... (0 means right) the probabilities in %, should add to 100%
	TArray<float> DirectionProbabilities;
};


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class CHARACTERPLUGIN_API UVHGazing : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UVHGazing();
	// Called when the game starts
	virtual void BeginPlay() override;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction) override;

	// Target: The Target that the agent should gaze towards
	UFUNCTION(BlueprintCallable)
	void GazeTo(FVector Target);

	// GazeTargetAgent: The Target that the agent should gaze towards
	// Offset: Offset from the actors location. 
	UFUNCTION(BlueprintCallable)
	void GazeToActor(AActor* GazeTargetActor, FVector Offset = FVector::ZeroVector);

	// GazeTargetComponent: The target that the agent should gaze towards
	// Offset: Offset from the component location. 
	UFUNCTION(BlueprintCallable)
	void GazeToComponent(USceneComponent* GazeTargetComponent, FVector Offset = FVector::ZeroVector);

	// Let the agent gaze at the user (the head component of the VirtualRealityPawn) 
	UFUNCTION(BlueprintCallable)
	void GazeToUser();

	// gaze in neutral direction, so straight forward, setting all rotations to 0
	UFUNCTION(BlueprintCallable)
	void StopGazing();

	// This methods let's only the eyes gaze towards a specific position while the head and torso are
	// potentiall oriented towards another gaze target specified with the methods above
	// Target: The Target that the agent should gaze towards
	UFUNCTION(BlueprintCallable)
	void GazeToEyesOnly(FVector Target);

	UFUNCTION(BlueprintCallable)
	void StopEyesOnlyGazing();


	// perform a blink when not currently blinking (only if bEyeBlink is true)
	UFUNCTION(BlueprintCallable)
	void ForceBlink();

	// sets the saccade mode, where Fixating does not produce any saccades
	// and Listening or Speaking follow the statistics in:
	// https://dl.acm.org/doi/10.1145/566570.566629
	// bStartWithMutualGaze whether after calling this directly a mutual (i.e. gaze at target)
	//								or an averted gaze begins
	UFUNCTION(BlueprintCallable)
	void SetSaccadeMode(ESaccadeMode NewSaccadeMode, bool bStartWithMutualGaze = true);

	UFUNCTION(BlueprintCallable)
	ESaccadeMode GetCurrentSaccadeMode() const;

	// Occular Motor Range to one side in degree
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeLimits")
	float MaxEyeYaw = 35.0f;
	// Occular Motor Range to top or bottom in degree
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeLimits")
	float MaxEyePitch = 10.0f;
	// Head Rotation Range to one side in degree
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeLimits")
	float MaxHeadYaw = 50.0f;
	// Head Pitch Range to one side in degree
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeLimits")
	float MaxHeadPitch = 30.0f;
	// Torso Rotation Range to one side in degree
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeLimits")
	float MaxTorsoYaw = 40.0f;

	// Maximal rotational velocity of the eye movement 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float InterpolationSpeedEyes = 300.0f; //degree/s (value specified in https://doi.org/10.1145/2724731 (150) felt to low)
	// This is a factor of InterpolationSpeed used at the beginning of the movement, see https://doi.org/10.1145/2724731
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float StartSpeedFactorEyes = 0.5f;

	// Maximal rotational velocity of the head movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float InterpolationSpeedHead = 150.0f; //degree/s (value specified in https://doi.org/10.1145/2724731 (50) felt to low)
	// This is a factor of InterpolationSpeed used at the beginning of the movement, see https://doi.org/10.1145/2724731
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float StartSpeedFactorHead = 0.25f;

	// Maximal rotational velocity of the torso movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float InterpolationSpeedTorso = 60.0f; //degree/s (value specified in https://doi.org/10.1145/2724731 (15) felt to low)
	// This is a factor of InterpolationSpeed used at the beginning of the movement, see https://doi.org/10.1145/2724731
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float StartSpeedFactorTorso = 0.25f;

	//torso rotation will start TorsoRotationDelayConstant + TorsoRotationDelayLinear*RotationAmplitude seconds after the eyes (idea from https://doi.org/10.1145/2724731)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float TorsoRotationDelayConstant = 0.0475f; //seconds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float TorsoRotationDelayLinear = 0.00025f; //seconds

	// HeadAlignment: Value between 0 and 1, to specify how much the head aligns towards the target
	// HeadAlignment = 0: the head rotates only the minimal amount required for the eyes to gaze towards the target.
	// HeadAlignment = 1: the head is fully aligned towards the target and the eyes gaze straight ahead.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float HeadAlignment = 0.9f;

	// TorsoAlignment: Value between 0 and 1, to specify how much the torso aligns towards the target.
	// same as with HeadAlignment
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	float TorsoAlignment = 0.1f;

	// whether, e.g., the head shoul rotate more in case the eyes cannot reach their target without
	// still remaining within the limits but potentially surpassing the Alignment factors given
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing|GazeDynamics")
	bool bSupportClampedRotationByOtherParts = true;

	//which parts of the body are involved in the gazing movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing")
	EGazeMode GazeMode = EGazeMode::EyesHeadTorso;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing")
	bool bEyeBlink = true;

	// whether to show microsaccades: see e.g. https://www.nature.com/articles/nrn1348 or https://doi.org/10.1167%2F8.14.15
	// these are very subtle movements of the eye, around 0.4 degree at 1-2Hz
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing")
	bool bDoMicrosaccades = true;

	// whether the VA should switch between looking in the user's eyes, as we do with real humans
	// if this is false the VA looks at the glasses' position which should be in the middle between the user's eyes (so approx. the bridge of the nose)
	// this only has an effect if GazeToUser was called
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VHGazing")
	bool bDoEyeSwitchingWhenLookingAtUser = true;


protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "VHGazing")
	ESaccadeMode SaccadeMode = ESaccadeMode::Fixating;

private:
	void UpdateTargetRotations();
	void Blink(float DeltaTime);

	//compute a new microsaccade, called by timer
	void MicroSaccade();
	void Saccade();
	void SwitchLookAtUsersEye();
	FRotator GazeAtUserEyeOffset();
	FRotator RandomSaccadeRotator(float Magnitude) const;
	FRotator RandomSaccadeRotator(float Magnitude, FSaccadeData SaccadeDate) const;
	float RandomGaussian(float Mean, float SD) const;
	float RandomSaccadeMagnitude() const;
	void SetupSaccadeData();


	AVirtualHuman* VirtualHumanPtr;


	bool bIsGazing = false;
	FVector TargetLocation;
	FVector LastTargetLocation;
	TOptional<FVector> EyesOnlyTargetLocation;
	AActor* TargetActor;
	USceneComponent* TargetComponent;
	bool bGazingAtUser = false;
	FVector TargetOffset; //only used for TargetActor or TargetComponent

	FRotator MicroSaccadeOffset;
	FRotator SaccadeOffset;

	FRandomStream GazingRandomStream;

	float BlinkPhaseTimeElapsed;
	float BlinkPhaseDuration;
	EBlinkState BlinkState;

	FTimerHandle NextMicroSaccadeHandle;
	FTimerHandle NextSaccadeHandle;
	FTimerHandle NextEyeSwitchHandle;
	bool bAvertedSaccade;
	TMap<ESaccadeMode, FSaccadeData> SaccadeData;

	//only needed when bDoEyeSwitchingWhenLookingAtUser
	bool bLookAtUsersRightEye = true;
};

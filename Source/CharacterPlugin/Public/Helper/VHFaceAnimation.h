// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VHAnimInstance.h"

#include "VHFaceAnimation.generated.h"


struct FFrameData
{
	float Timestamp;
	TArray<float> BlendshapeActivations;
	FRotator HeadRotation = FRotator(0, 0, 0);
	FRotator RightEyeRotation = FRotator(0, 0, 0);
	FRotator LeftEyeRotation = FRotator(0, 0, 0);
	FRotator JawRotation = FRotator(0, 0, 0);
};

struct FAnimationData
{
	//the BlendshapeNames belong to the elements of BlendshapeActivations
	TArray<FString> BlendshapeNames;
	TArray<FFrameData> TimeSteps;
	TArray<float> CurrentFrameValues;
};

UCLASS( )
class CHARACTERPLUGIN_API UVHFaceAnimation : public UActorComponent
{
  GENERATED_BODY()

public:
  // Sets default values for this component's properties
	UVHFaceAnimation();

  // Called when the game starts
  virtual void BeginPlay() override;

  // Called every frame
  virtual void TickComponent(float DeltaTime, ELevelTick TickType,
                             FActorComponentTickFunction* ThisTickFunction)
  override;

  UFUNCTION(BlueprintCallable)
  void Play();

	UFUNCTION(BlueprintCallable)
	void PlayFromTime(float StartTime);

  UFUNCTION(BlueprintCallable)
  void Stop();

  UFUNCTION(BlueprintCallable)
  bool IsPlaying();

  UFUNCTION(BlueprintCallable)
  float GetDuration() const; //in seconds

	// loads the animation file
	// can also be used to preload multiple files, if a file was already loaded a cached version is used on additional call at runtime
	// on BeginPlay the file specified at AnimationFilename (if any) is loaded
  UFUNCTION(BlueprintCallable)
  bool LoadAnimationFile(FString Filename);

  // this method can be used to calibrate the neutral face from any animation file (even another one then which should be played)
  // the data nearest to the time given will be substracted from any frame of the animation.
  // this will be overwritten if bUseFaceCalibrationFrame is true
  UFUNCTION(BlueprintCallable)
  bool Calibrate(FString Filename, float CalibrationTime);

  // saves the animation as AnimSequence so it can be adapted and potentially better be played back
  // AnimName can include a path, relative to the Content dir of your project
  // @deprecated This function is no longer functioning. To use it again it has to be rebuild without deprecated functions and variables.
  UFUNCTION(BlueprintCallable, meta = (DeprecatedFunction, DeprecationMessage = "Function has been deprecated, if needed reimplement it."))
  UAnimSequence* SaveAsAnimSequence(FString AnimName);


  UPROPERTY(BlueprintReadWrite, EditAnywhere)
  bool bUseHeadRotation = true;

  UPROPERTY(BlueprintReadWrite, EditAnywhere)
  bool bUseEyeRotations = true;

  UPROPERTY(EditAnywhere, BlueprintReadOnly)
  FString AnimationFilename;

	//delay the face animation by this amount of time (in seconds), for example, to compensate delay introduced by the audio processing 
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float ConstantDelay = 0.0f;


	// set to true then, whatever data is in the frame nearest to TimeForCalibration will be substracted from every frame
	// this can be used to calibrate to a neutral face
  UPROPERTY(BlueprintReadWrite, EditAnywhere)
  bool bUseFaceCalibrationFrame = false;

  UPROPERTY(BlueprintReadWrite, EditAnywhere)
  float TimeForCalibration = 0.0f;

	// whether the face should be reset to neutral after the animation is finished or the expression of the last frame is kept
  UPROPERTY(BlueprintReadWrite, EditAnywhere)
	  bool bResetFaceAfterFinish = true;

	//time in seconds for fading in and out of the animation
	// set to 0 if no smoothing is wanted
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float FadeInOutTime = 0.2f;

protected:

  

	virtual bool InternalLoadFile(FString FileName);

  void ApplyFrameData(int FrameNr);
  void ResetFace();

  //returns the index which is at the time or just below it.
  int SearchClosestFrameDataIndex();

  void Calibration(float Time);
  bool IsCalibrationPresent() const;

  float GetFacialExpressionMorphTargetValue(const FName& MorphName);


  FAnimationData AnimationData;

  FFrameData CalibrationFrame;

  TMap<FString, FAnimationData> CachedAnimationData;


  bool bCurrentlyPlaying = false;
  float CurrentPlayTime = 0.0f;
  float FadeFraction = 0.0f;
  int LastFrameNr = 0;

  UVHAnimInstance* AnimInstance;
  bool UseMorphs = true;

public:

  const FAnimationData& GetAnimData();

};

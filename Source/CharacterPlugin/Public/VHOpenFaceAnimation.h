// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VHFacialExpressions.h"
#include "VHAnimInstance.h"
#include "Helper/VHFaceAnimation.h"
#include "Animation/PoseAsset.h"
#include "VHOpenFaceAnimation.generated.h"

// How to recording/tracking an OpenFace tracking file to read in see:
// https://devhub.vr.rwth-aachen.de/VR-Group/unreal-development/character-plugin/-/wikis/Components/FaceAnimation

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHARACTERPLUGIN_API UVHOpenFaceAnimation : public UVHFaceAnimation {
  GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPoseAsset* Mapping = nullptr;

protected:

	virtual bool InternalLoadFile(FString FileName) override;

  UPROPERTY(EditAnywhere, BlueprintReadOnly)
  float AUActivationScaling = 0.2f;
  //OpenFace put activation out on a 5 point scale: https://github.com/TadasBaltrusaitis/OpenFace/wiki/Action-Units
};

// This Animgraph node file is a modification from the file "AnimNode_ModifyCurve.h" by Epic Games (in UE 4.26)

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNodeBase.h"

#include "Helper/VHFaceAnimation.h"
#include "VHFacialExpressions.h"

#include "ModifyCurvesNode.generated.h"

UENUM()
enum class EModifyCurveApplyModeLocal : uint8
{
	/** Add new value to input curve value */
	Add,

	/** Scale input value by new value */
	Scale,

	/** Blend input with new curve value, using Alpha setting on the node */
	Blend,

	/** Remaps the new curve value between the CurveValues entry and 1.0 (.5 in CurveValues makes 0.51 map to 0.02) */
	RemapCurve
};

USTRUCT(BlueprintType)
struct CHARACTERPLUGIN_API FModifyCurvesNode : public FAnimNode_Base
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, EditFixedSize, BlueprintReadWrite, Category = Links)
	FPoseLink SourcePose;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ModifyCurve, meta = (PinShownByDefault))
	UVHFaceAnimation* FaceAnimInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ModifyCurve, meta = (PinShownByDefault))
	UVHFacialExpressions* FacialExpressionsInstance;

	TArray<float> LastCurveValues;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ModifyCurve, meta = (PinShownByDefault))
	float Alpha;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ModifyCurve)
	EModifyCurveApplyModeLocal ApplyMode;

	FModifyCurvesNode();

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones_AnyThread(const FAnimationCacheBonesContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	// End of FAnimNode_Base interface
	
};

// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "Animation/AnimNodeBase.h"
#include "AnimNode_VHGazing.generated.h"

UENUM()
enum class EGazeMode : uint8
{
	EyesOnly,
	EyesHead,
	EyesHeadTorso
};

USTRUCT(BlueprintType)
struct FGazingPartData
{
	GENERATED_BODY()

	//to be provided by VHGazing:
	float MaxPitch = 0.0f;
	float MaxYaw = 0.0f;
	float MaxVelocity = 0.0f;
	float StartVelocityFactor = 0.0f;
	float DelayConstant = 0.0f;
	float DelayLinear = 0.0f;
	float Alignment = 0.0f;
};

USTRUCT(BlueprintType)
struct FVHGazingData
{
	GENERATED_BODY()

	bool bIsGazing;
	FVector GazeTargetLocation;
	TOptional<FVector> EyesOnlyGazeTarget;
	FRotator AdditionalSaccadeRotation;
	EGazeMode GazeMode;

	FGazingPartData RightEyeData;
	FGazingPartData LeftEyeData;
	FGazingPartData HeadData;
	FGazingPartData TorsoData;

	bool bSupportClampedRotationByOtherParts;
};

USTRUCT(BlueprintType)
struct FGazingBoneData
{
	GENERATED_BODY()

	//this is the bone to compute the current "look-at" direction from
	UPROPERTY(EditAnywhere)
	FBoneReference TargetBone;

	//those are the bones that are influenced when this is moved (hierachically rising from closer to root)
	UPROPERTY(EditAnywhere, meta = (EditCondition = "!bUseBlendshapeOutput"))
	TArray<FBoneReference> InfluenceBones;

	//the influence each of these bones takes (sum should be 1.0!), corresponding to the order of InfluenceBones
	UPROPERTY(EditAnywhere, meta = (EditCondition = "!bUseBlendshapeOutput"))
	TArray<float> InfluenceStrength;

	//sometimes not bones but morphtargets/blendshapes should be used, e.g., for metahuman eyes as output
	UPROPERTY(EditAnywhere)
	bool bUseBlendshapeOutput = false;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseBlendshapeOutput"))
	FName BlendshapeNameLeft = NAME_None;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseBlendshapeOutput"))
	FName BlendshapeNameRight = NAME_None;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseBlendshapeOutput"))
	FName BlendshapeNameUp = NAME_None;

	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseBlendshapeOutput"))
	FName BlendshapeNameDown = NAME_None;

	// at what yaw angle should the right blendshape be at 1.0 (it will not go further
	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseBlendshapeOutput"))
	float YawAngleAtFull = 25.0f;

	// at what pitch angle should the right blendshape be at 1.0 (it will not go further
	UPROPERTY(EditAnywhere, meta = (EditCondition = "bUseBlendshapeOutput"))
	float PitchAngleAtFull = 25.0f;

	//used internally:
	FTransform DefaultTargetBoneTransform;
	FRotator CurrentRotation;
	float TimeDelay;
	float AmplitudeCurrMovement;
	FRotator MissingRotationDueToClamp;
};

/**
 * This node computed the needed transformation for VHGazing and directly applies it
 * Code is inspired by the engine's FAnimNode_AimOffsetLookAt
 */
USTRUCT(BlueprintInternalUseOnly)
struct CHARACTERPLUGIN_API FAnimNode_VHGazing : public FAnimNode_Base
{
	GENERATED_USTRUCT_BODY()

protected:

	// Input link
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Links)
	FPoseLink InputPose;

	UPROPERTY(EditAnywhere, Category = SkeletalControl)
	FGazingBoneData RightEyeBoneData = FGazingBoneData();

	UPROPERTY(EditAnywhere, Category = SkeletalControl)
	FGazingBoneData LeftEyeBoneData = FGazingBoneData();

	UPROPERTY(EditAnywhere, Category = SkeletalControl)
	FGazingBoneData HeadBoneData = FGazingBoneData();

	UPROPERTY(EditAnywhere, Category = SkeletalControl)
	FGazingBoneData TorsoBoneData = FGazingBoneData();

	/** Data provided by the VHGazing component bundled into a struct */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VHGazing, meta = (PinShownByDefault))
	FVHGazingData VHGazingData = FVHGazingData();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VHGazing, meta = (PinHiddenByDefault))
	FVector AgentDefaultForward = FVector(0, 1, 0);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = VHGazing, meta = (PinShownByDefault))
	FRotator AdditionalHeadRotation = FRotator::ZeroRotator;


public:

	// FAnimNode_Base interface
	virtual void Initialize_AnyThread(const FAnimationInitializeContext& Context) override;
	virtual void CacheBones_AnyThread(const FAnimationCacheBonesContext& Context) override;
	virtual void Update_AnyThread(const FAnimationUpdateContext& Context) override;
	virtual void Evaluate_AnyThread(FPoseContext& Output) override; //only one of the two Evaluate methods should be implemented
	//virtual void EvaluateComponentSpace_AnyThread(FComponentSpacePoseContext& Output) override;
	// End of FAnimNode_Base interface

	//return yaw (first component) in [-180, 180] and pitch (second component) in [-90,90]
	//returns rotation between both vectors, requires normalized vectors!
	static FRotator GetYawPitchRotationBetween(FVector From, FVector To);

private:

	void UpdateRotations(FPoseContext& PoseContext, FComponentSpacePoseContext& CSPoseContext);

	void ApplyRotation(FGazingBoneData& GazingBoneData, FRotator Rotation, FPoseContext& PoseContext, FComponentSpacePoseContext& CSPoseContext);

	// works like FindLookAtRotation, but takes the agents view rotation into account,
	// Get missing rotation to target in component space as yaw and roll only
	FRotator GetRotationToTarget(FGazingBoneData& GazingBoneData, FPoseContext& LocalPoseContext, FComponentSpacePoseContext& CSPoseContext, bool bEyes = false);

	// updates the currentRotation in GazingBoneData using movement dynamics
	void UpdateCurrentRotation(FGazingBoneData& GazingBoneData, const FGazingPartData& GazingPartData, const FRotator& TargetRotation);

	// Transforms (Rotates) a Rotation in Component Space to fit the default rotation
	// So with DefautAgentForward = Y-Axis this basically only swaps roll and pitch
	// CAUTION: as of now it does not work for any other DefautAgentForward directions!
	FRotator TransformToAgentForward(FRotator Rotation) const;

	//Clamps the Rotation between the max jaw and pitch (both directions) given in GazingPartData
	// also reports the min distance to any of these clamps, so ~0 if the value was clamped
	FRotator ClampRotation(FRotator Rotation, const FGazingPartData& GazingPartData, float& MinDistanceToClamp) const;

	const FReferenceSkeleton* RefSkel = nullptr;

	float DeltaTime;
};

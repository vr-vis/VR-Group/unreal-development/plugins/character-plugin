// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VHListenerBackchannels.generated.h"


// class to show listener backchannel behavior: currently only nods are implemented
// for further implementations which were not merged yet, see thesis/MA_tyroller_backchannels branch

class UVHAnimInstance;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CHARACTERPLUGIN_API UVHListenerBackchannels : public UActorComponent
{
	GENERATED_BODY()

public:	
	UVHListenerBackchannels();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void StartNod();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	float NodDuration = 0.5; //in s

	UPROPERTY(EditAnywhere)
	float NodAmplitude = 3.0; //degrees
	

private:
	UVHAnimInstance* AnimInstance = nullptr;
	FRandomStream RandomStream;

	bool bNodding;
	float NoddingTime;
	float ActualNodDuration; //duration of one nod
	float ActualNodAmplitude;
	float ActualNodLength;//can be used to play a double nod etc.
};

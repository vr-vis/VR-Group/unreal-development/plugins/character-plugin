# Character Plugin

A currently **very preliminary** plugin to embedd virtual characters (virtual agents) into our Unreal projects. The project is based on Unreal version 4.22 and all features are demonstrated in separate maps in our test project called [Unreal Character Test](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/unreal-development/demos/unreal-character-test). To follow our group's CI/CD process, this test project also uses Nightly Builds.

If you are using the [VAServer](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/VAServerLauncher) for the audio, please ensure that you always have the newest version (due to sound files being stored there).

## Branches
| Branch                | Working Unreal Versions |
| --------------------- | ----------------------- |
| 5.3                   | 5.3, 5.4, 5.5           |
| deprecated/4.27       | 4.27                    |
| deprecated/4.22       | 4.22                    |

## Contributions
If you want to contribute new features to this project, please open a respective feature-branch and open a merge-request once the feature is finished (including: testing, documentation, and issue update). Please ensure to also open a feature-branch of the same name in the Character Test project to test and demonstrate the new feature with a character of your choice. Models can be found here: [growing collection of character models](https://git-ce.rwth-aachen.de/vr-vis/VR-Group/virtualcharactermodels).

## Contacts
For any questions, please do not hestitate to contact @jehret and @aboensch.
